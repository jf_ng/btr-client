"use strict";

var Util = require("./Util");

cc.Class({
    extends: cc.Component,

    editor: {
        // executeInEditMode: true
    },

    properties: {
        friendViewRect: null,
        friendView: null,
        friendContent: null,

        chatViewRect: null,
        chatView: null,
        chatContent: null,

        _actionOpen: false,

        _friend: null,
        _chat: null,
    },

    onLoad() {
        Util.playerData.user_data.new_chat_message = 0;
        Util.savePlayerData();
        

        this.friendView = this.node.getChildByName('list').getChildByName('view');
        this.friendContent = this.friendView.getChildByName('content');

        this.chatView = this.node.getChildByName('chat').getChildByName('view');
        this.chatContent = this.chatView.getChildByName('content');


        // **************************************************
        // scale list
        // **************************************************
        this.node.getChildByName('list').width *= cc.winSize.width / 960;
        this.node.getChildByName('list').height *= cc.winSize.height / 640;

        this.node.getChildByName('list').getChildByName('view').width *= cc.winSize.width / 960;
        this.node.getChildByName('list').getChildByName('view').getChildByName('content').width *= cc.winSize.width / 960;

        this.node.getChildByName('list').getChildByName('view').getChildByName('content').children[0].width *= cc.winSize.width / 960;
        this.node.getChildByName('list').getChildByName('view').getChildByName('content').children[0].getChildByName('name').width *= cc.winSize.width / 960;


        // **************************************************
        // scale chat
        // **************************************************
        this.node.getChildByName('chat').width *= cc.winSize.width / 960;
        this.node.getChildByName('chat').height *= cc.winSize.height / 640;

        this.node.getChildByName('chat').getChildByName('view').width *= cc.winSize.width / 960;
        this.node.getChildByName('chat').getChildByName('view').getChildByName('content').width *= cc.winSize.width / 960;


        // **************************************************
        // scale input
        // **************************************************
        this.node.getChildByName('input').width *= cc.winSize.width / 960;
        this.node.getChildByName('input').height *= cc.winSize.height / 640;

        this.node.getChildByName('input').getChildByName('editbox').width *= cc.winSize.width / 960;
    },

    start() {
        this.node.getChildByName('input').getChildByName('editbox').getComponent(cc.EditBox).enabled = false;


        this.showChat();
    },

    showChat() {
        var chat_friend = cc.sys.localStorage.getItem('chat_friend');
        cc.sys.localStorage.removeItem('chat_friend');
        var chat_exist = false;


        // delete existing
        var parent = this.node.getChildByName('list').getChildByName('view').getChildByName('content');
        var count = parent.children.length;
        for (var j = 1; j < count; j++) {
            parent.children[1].removeFromParent(false);
            // parent.children[1].destroy();
        }


        for (var i = 0; i < Util.playerChats.length; i++) {
            var friend = cc.instantiate(parent.children[0]);
            friend.parent = parent;

            if (Util.playerChats[i].friend_id == Util.playerData.uid) {
                var friend_name = Util.playerChats[i].user_name;
            }
            else {
                var friend_name = Util.playerChats[i].friend_name;
            }

            friend.getChildByName('name').getComponent(cc.Label).string = friend_name;

            var clickEventHandler = new cc.Component.EventHandler();
            clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
            clickEventHandler.component = "Chat";// This is the code file name
            clickEventHandler.handler = "showChatMessages";
            clickEventHandler.customEventData = Util.playerChats[i].id;

            var button = friend.getComponent(cc.Button);
            button.clickEvents = [];
            button.clickEvents.push(clickEventHandler);

            friend.active = true;


            if (chat_friend && chat_friend == Util.playerChats[i].friend_id) {
                this.showChatMessages(null, Util.playerChats[i].id);
                chat_exist = true;
            }
        }

        if (chat_friend && !chat_exist) {
            for (var i = 0; i < Util.playerData.friends.length; i++) {
                if (Util.playerData.friends[i].friend_id == chat_friend) {
                    var friend = cc.instantiate(parent.children[0]);
                    friend.parent = parent;

                    friend.getChildByName('name').getComponent(cc.Label).string = Util.playerData.friends[i].friend_name;

                    var clickEventHandler = new cc.Component.EventHandler();
                    clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
                    clickEventHandler.component = "Chat";// This is the code file name
                    clickEventHandler.handler = "showChatMessages";
                    clickEventHandler.customEventData = null;

                    var button = friend.getComponent(cc.Button);
                    button.clickEvents = [];
                    button.clickEvents.push(clickEventHandler);

                    friend.active = true;


                    this._friend = {
                        friend_id: Util.playerData.friends[i].friend_id,
                        friend_name: Util.playerData.friends[i].friend_name
                    }

                    this.showChatMessages(null, null);

                    break;
                }
            }
        }
    },

    showChatMessages(event, customEventData) {
        this._chat = customEventData;


        // delete existing
        var parent = this.node.getChildByName('chat').getChildByName('view').getChildByName('content');
        var count = parent.children.length;
        for (var j = 2; j < count; j++) {
            parent.children[2].removeFromParent(false);
            // parent.children[2].destroy();
        }

        for (var i = 0; i < Util.playerChats.length; i++) {
            if (Util.playerChats[i].id == customEventData) {
                if (Util.playerChats[i].friend_id == Util.playerData.uid) {
                    var friend_name = Util.playerChats[i].user_name;
                    var friend_id = Util.playerChats[i].user_id;
                }
                else {
                    var friend_name = Util.playerChats[i].friend_name;
                    var friend_id = Util.playerChats[i].friend_id;
                }

                this._friend = {
                    friend_id: friend_id,
                    friend_name: friend_name
                }

                break;
            }
        }


        this.node.getChildByName('name').getComponent(cc.Label).string = this._friend.friend_name;
        this.node.getChildByName('name').active = true;


        for (var j = 0; j < Util.playerChatMessages.length; j++) {
            if (Util.playerChatMessages[j].chat_id != customEventData) continue;

            if (Util.playerChatMessages[j].sender == Util.playerData.uid) {
                this.createMessage(true, Util.playerChatMessages[j].message);
            }
            else {
                this.createMessage(false, Util.playerChatMessages[j].message);
            }
        }

        this.scheduleOnce(function() {
            this.node.getChildByName('chat').getComponent(cc.ScrollView).scrollToBottom(0);
        }, 0.1);


        this.node.getChildByName('input').getChildByName('editbox').getComponent(cc.EditBox).enabled = true;
    },

    createMessage(self, message) {
        var parent = this.node.getChildByName('chat').getChildByName('view').getChildByName('content');

        if (self) {
            var node = cc.instantiate(parent.getChildByName('self'));
        }
        else {
            var node = cc.instantiate(parent.getChildByName('friend'));
        }

        node.parent = parent;
        node.getChildByName('text').getComponent(cc.Label).string = message;
        node.active = true;
    },

    home(event, customEventData) {
        // Util.showLoading();
        Util.loadScene('home');
    },

    showAction(event, customEventData) {
        if (!this._friend) return;

        if (!this._actionOpen) {
            this.node.getChildByName('action menu').active = true;
            this._actionOpen = true;
        }
        else {
            this.node.getChildByName('action menu').active = false;
            this._actionOpen = false;
        }
    },

    cleanup() {
         // delete existing chat
        var parent = this.node.getChildByName('chat').getChildByName('view').getChildByName('content');
        var count = parent.children.length;
        for (var j = 2; j < count; j++) {
            parent.children[2].removeFromParent(false);
            // parent.children[2].destroy();
        }

        // close action
        this.node.getChildByName('action menu').active = false;
        this._actionOpen = false;

        this.node.getChildByName('name').active = false;

        this.showChat();
    },

    actions(event, customEventData) {
        var self = this;

        switch (customEventData) {
            case 'delete':
                Util.createChoiceAlert(
                    'Are you sure you want to delete this chat?',
                    {
                        text: 'Yes',
                        cb: function() {
                            Util.showLoading();

                            var params = 'token=' + Util.playerData.token;
                            params += '&game_id=' + Util.playerData.game_id;
                            params += '&id=' + self._chat;

                            var xmlhttp = new XMLHttpRequest();
                            xmlhttp.open("POST", Util.getConfig().server_url + '/deleteChat', true);
                            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                            xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                            xmlhttp.onreadystatechange = function() {
                                if (xmlhttp.readyState === 4) {
                                    if (xmlhttp.status === 200) {
                                        var response = JSON.parse(xmlhttp.responseText);
                                        if (response.status === 'success') {
                                            // delete chat
                                            for (var i = 0; i < Util.playerChats.length; i++) {
                                                if (Util.playerChats[i].id == self._chat) {
                                                    Util.playerChats.splice(i, 1);
                                                    break;
                                                }
                                            }
                                            Util.saveChats();


                                            // delete messages
                                            for (var i = 0; i < Util.playerChatMessages.length; i++) {
                                                if (Util.playerChatMessages[i].chat_id == self._chat) {
                                                    Util.playerChatMessages.splice(i, 1);
                                                    i--;
                                                }
                                            }
                                            Util.saveChatMessages();


                                            self.cleanup();


                                            self.node.getChildByName('input').getChildByName('editbox').getComponent(cc.EditBox).enabled = false;


                                            Util.removeLoading();
                                        } else {
                                            Util.createAlert('Failed to delete chat', function() {
                                                Util.removeLoading();
                                            });
                                        }
                                    }
                                }
                            }
                            xmlhttp.onerror = function () {
                                Util.createAlert('Failed to delete chat', function() {
                                    Util.removeLoading();
                                });
                            };
                            xmlhttp.send(params);
                        }
                    },
                    {
                        text: 'No',
                        cb: function() {}
                    }
                );

                break;
            case 'block':
                Util.createChoiceAlert(
                    'Are you sure you want to block this chat?',
                    {
                        text: 'Yes',
                        cb: function() {
                            Util.showLoading();

                            var params = 'token=' + Util.playerData.token;
                            params += '&game_id=' + Util.playerData.game_id;
                            params += '&id=' + self._chat;

                            var xmlhttp = new XMLHttpRequest();
                            xmlhttp.open("POST", Util.getConfig().server_url + '/blockChat', true);
                            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                            xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                            xmlhttp.onreadystatechange = function() {
                                if (xmlhttp.readyState === 4) {
                                    if (xmlhttp.status === 200) {
                                        var response = JSON.parse(xmlhttp.responseText);
                                        if (response.status === 'success') {
                                            // delete chat
                                            for (var i = 0; i < Util.playerChats.length; i++) {
                                                if (Util.playerChats[i].id == self._chat) {
                                                    Util.playerChats.splice(i, 1);
                                                    break;
                                                }
                                            }
                                            Util.saveChats();


                                            // delete messages
                                            for (var i = 0; i < Util.playerChatMessages.length; i++) {
                                                if (Util.playerChatMessages[i].chat_id == self._chat) {
                                                    Util.playerChatMessages.splice(i, 1);
                                                    i--;
                                                }
                                            }
                                            Util.saveChatMessages();


                                            self.cleanup();


                                            self.node.getChildByName('input').getChildByName('editbox').getComponent(cc.EditBox).enabled = false;


                                            Util.removeLoading();
                                        } else {
                                            Util.createAlert('Failed to block chat', function() {
                                                Util.removeLoading();
                                            });
                                        }
                                    }
                                }
                            }
                            xmlhttp.onerror = function () {
                                Util.createAlert('Failed to block chat', function() {
                                    Util.removeLoading();
                                });
                            };
                            xmlhttp.send(params);
                        }
                    },
                    {
                        text: 'No',
                        cb: function() {}
                    }
                );

                break;
            case 'report':
                Util.createChoiceAlert(
                    'Are you sure you want to report and block this chat?',
                    {
                        text: 'Yes',
                        cb: function() {
                            Util.showLoading();

                            var params = 'token=' + Util.playerData.token;
                            params += '&game_id=' + Util.playerData.game_id;
                            params += '&id=' + self._chat;

                            var xmlhttp = new XMLHttpRequest();
                            xmlhttp.open("POST", Util.getConfig().server_url + '/reportChat', true);
                            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                            xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                            xmlhttp.onreadystatechange = function() {
                                if (xmlhttp.readyState === 4) {
                                    if (xmlhttp.status === 200) {
                                        var response = JSON.parse(xmlhttp.responseText);
                                        if (response.status === 'success') {
                                            // delete chat
                                            for (var i = 0; i < Util.playerChats.length; i++) {
                                                if (Util.playerChats[i].id == self._chat) {
                                                    Util.playerChats.splice(i, 1);
                                                    break;
                                                }
                                            }
                                            Util.saveChats();


                                            // delete messages
                                            for (var i = 0; i < Util.playerChatMessages.length; i++) {
                                                if (Util.playerChatMessages[i].chat_id == self._chat) {
                                                    Util.playerChatMessages.splice(i, 1);
                                                    i--;
                                                }
                                            }
                                            Util.saveChatMessages();


                                            self.cleanup();


                                            self.node.getChildByName('input').getChildByName('editbox').getComponent(cc.EditBox).enabled = false;


                                            Util.removeLoading();
                                        } else {
                                            Util.createAlert('Failed to report chat', function() {
                                                Util.removeLoading();
                                            });
                                        }
                                    }
                                }
                            }
                            xmlhttp.onerror = function () {
                                Util.createAlert('Failed to report chat', function() {
                                    Util.removeLoading();
                                });
                            };
                            xmlhttp.send(params);
                        }
                    },
                    {
                        text: 'No',
                        cb: function() {}
                    }
                );

                break;
        }
    },

    send(event, customEventData) {
        var message = this.node.getChildByName('input').getChildByName('editbox').getComponent(cc.EditBox).string.trim();
        if (!message) return;


        this.node.getChildByName('input').getChildByName('send').getComponent(cc.Button).interactable = false;


        var self = this;

        var doSend = function() {
            // **************************************************
            // add ui first
            // **************************************************
            self.createMessage(true, message);
            self.scheduleOnce(function() {
                self.node.getChildByName('chat').getComponent(cc.ScrollView).scrollToBottom(0);
            }, 0.1);

            var params = 'token=' + Util.playerData.token;
            params += '&game_id=' + Util.playerData.game_id;
            params += '&id=' + self._friend.friend_id;
            params += '&message=' + message;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", Util.getConfig().server_url + '/sendChat', true);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp.setRequestHeader("Cache-Control", "no-cache");
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState === 4) {
                    if (xmlhttp.status === 200) {
                        var response = JSON.parse(xmlhttp.responseText);
                        if (response.status === 'success') {
                            self.node.getChildByName('input').getChildByName('editbox').getComponent(cc.EditBox).string = '';


                            var chat_found = false;
                            for (var i = 0; i < Util.playerChats.length; i++) {
                                if (Util.playerChats[i].id == response.chat.id) {
                                    chat_found = true;
                                    break;
                                }
                            }
                            if (!chat_found) {
                                Util.playerChats.push(response.chat);
                                Util.saveChats();
                            }


                            Util.playerChatMessages.push(response.message);
                            Util.saveChatMessages();
                        } else {
                            Util.createAlert('Failed to send message');
                        }

                        self.node.getChildByName('input').getChildByName('send').getComponent(cc.Button).interactable = true;
                    }
                }
            }
            xmlhttp.onerror = function () {
                Util.createAlert('Failed to send message');
                self.node.getChildByName('input').getChildByName('send').getComponent(cc.Button).interactable = true;
            };
            xmlhttp.send(params);
        }


        var showRewarded = false, showRewardedDate = false;
        if (!Util.playerData.chat_rewarded_date || Util.playerData.chat_rewarded_date != new Date().toLocaleDateString()) {
            showRewarded = true;
            showRewardedDate = false;

            Util.playerData.chat_rewarded_count = 0;
        }
        else if (Util.playerData.chat_rewarded_count == 10) {
            showRewarded = true;
            showRewardedDate = true;
        }
        else if (Util.playerData.chat_rewarded_count > 10 && Util.playerData.chat_rewarded_count % 25 == 0) {
            showRewarded = true;
            showRewardedDate = true;
        }

        if (showRewarded) {
            Util.createChoiceAlert(
                ( (showRewardedDate) ? 'Watch a video ads to continue chatting?' : 'Watch a video ads to enable chat for today?' ),
                {
                    text: 'Yes',
                    cb: function() {
                        Util.showRewarded().then(data => {
                            if (data === 'wait') {
                                Util.createAlert('Loading ads. Please try again in a while.', function() {
                                    self.node.getChildByName('input').getChildByName('send').getComponent(cc.Button).interactable = true;
                                });
                            }
                            else if (data === 'success' && Util.rewardedSuccess) {
                                Util.playerData.chat_rewarded_date = new Date().toLocaleDateString();
                                Util.playerData.chat_rewarded_count++;
                                Util.savePlayerData();

                                doSend();
                            }
                            else {
                                Util.showInterstitial().then(data => {
                                    if (data === 'wait') {
                                        Util.createAlert('Loading ads. Please try again in a while.', function() {
                                            self.node.getChildByName('input').getChildByName('send').getComponent(cc.Button).interactable = true;
                                        });
                                    }
                                    else if (data === 'success') {
                                        Util.playerData.chat_rewarded_date = new Date().toLocaleDateString();
                                        Util.playerData.chat_rewarded_count++;
                                        Util.savePlayerData();

                                        doSend();
                                    }
                                    else if (data === 'fail') {
                                        Util.playerData.chat_rewarded_date = new Date().toLocaleDateString();
                                        Util.playerData.chat_rewarded_count++;
                                        Util.savePlayerData();

                                        doSend();
                                    }
                                });
                            }
                        });
                    }
                },
                {
                    text: 'No',
                    cb: function() {
                        self.node.getChildByName('input').getChildByName('send').getComponent(cc.Button).interactable = true;
                    }
                }
            );
        }
        else {
            Util.playerData.chat_rewarded_count++;
            doSend();
        }
    },

    update(dt) {
        this.friendViewRect = cc.rect(-this.friendView.width / 2, - this.friendContent.y - this.friendView.height / 2, this.friendView.width, this.friendView.height);

        for (let i = 0; i < this.friendContent.children.length; i++) {
            if (this.friendViewRect.intersects(this.friendContent.children[i].getBoundingBox())) {
                this.friendContent.children[i].opacity = 255;
            } else {
                this.friendContent.children[i].opacity = 0;
            }
        }


        this.chatViewRect = cc.rect(-this.chatView.width / 2, - this.chatContent.y - this.chatView.height / 2, this.chatView.width, this.chatView.height);

        for (let i = 0; i < this.chatContent.children.length; i++) {
            if (this.chatViewRect.intersects(this.chatContent.children[i].getBoundingBox())) {
                this.chatContent.children[i].opacity = 255;
            } else {
                this.chatContent.children[i].opacity = 0;
            }
        }
    },
});