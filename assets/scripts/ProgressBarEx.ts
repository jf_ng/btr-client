const {
    ccclass,
    property
} = cc._decorator;

@ccclass
export default class ProgressBarEx extends cc.Component {

    @property(cc.ProgressBar)
    bar: cc.ProgressBar = null;

    _from: number = 0;
    _to: number = 0;
    _duration: number = 0;
    _elapsed: number = 0;

    _percent: number = 0;

    _tween: cc.Tween = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        // enabled：是否每帧执行该组件的 update 方法，同时也用来控制渲染组件是否显示
        this.enabled = false;
    }

    isDone() {
        return this._elapsed >= this._duration;
    }

    progressTo(duration: number, percent: number, callback) {
        if (percent < 0 || percent > 1.0) {
            return;
        }

        // this.action(duration, percent);
        this.tween(duration, percent, callback);
    }

    tween(duration: number, percent: number, callback) {
        if (this._tween) {
            this._tween.stop();
        }

        this._tween = cc.tween(this.bar).to(duration, {
            progress: percent
        })
        .call(() => {
            this._tween = null;
            if (callback) callback();
        })
        .start();
    }

    action(duration: number, percent: number) {
        this._from = this.bar.progress;
        this._to = percent;
        this._elapsed = 0;
        this._duration = duration;

        this.enabled = true;
    }

    update(dt) {
        if (this.isDone()) {
            this.enabled = false;
            return;
        }

        this._elapsed += dt;

        let t = this._elapsed / (this._duration > 0.0000001192092896 ? this._duration : 0.0000001192092896);
        t = (1 > t ? t : 1);
        this.step(t > 0 ? t : 0);
    }

    step(dt) {
        let percent = this._from + (this._to - this._from) * dt;
        if (this._percent != percent) {
            // this._percent = cc.misc.clampf(progress, 0, 1);
            this._percent = cc.misc.clamp01(percent);
            this.bar.progress = this._percent;
        }
    }
}