"use strict";

cc.Class({
    extends: cc.Component,

    properties: {
        target: {
            default: null,
            type: cc.Node
        },
    },

    // use this for initialization
    onLoad() {
    },

    start() {
    },

    follow() {
        this.node.cleanup();
        
        var tileMapWidth = cc.find('Canvas').getComponent('Game').playGame.width;
        var tileMapHeight = cc.find('Canvas').getComponent('Game').playGame.height;
        
        var follow = cc.follow(this.target, cc.rect(0, 0, tileMapWidth, tileMapHeight));
        this.node.runAction(follow);
    },
});