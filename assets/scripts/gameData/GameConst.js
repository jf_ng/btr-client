"use strict";

var GameEnum = require("./GameEnum");

var GameConst = {
    GidToTileType: [
        GameEnum.TileType.tileNone,

        GameEnum.TileType.tileSteel, GameEnum.TileType.tileNone, GameEnum.TileType.tileBarrier1, GameEnum.TileType.tileBarrier2, GameEnum.TileType.tileGrass,
    ],

    Direction: ["up", "left", "down", "right"],
    DirectionRex: [/up/, /left/, /down/, /right/],

    RoomMax: 20,
    KingInterval: 180000, // ms @todo
    KingDuration: 60000, // ms @todo
    KingTickAnnounce: 2, // @todo

    Preload: {
        bullet: 10,
        shard: 10,
    },

    Shards: {
        type: ['bullet', 'speed'],
        rate: [1, 4],
        mod: 10,
    },

    InputCallbackSpeed: 0.2,

    TankStartingPosition: cc.v2(-2016, 1296),
    TankMoveSpeed: 0.8, // 0.8s
    TankBurst: {
        speed: 0.4,
        time: 4, // 4 cycles
    },

    BulletRange: cc.Enum({
        normal: 360, // 5 tiles
        laser: 504, // 7 tiles
    }),
    BulletSpeed: {
        normal: 1, // 1s
        laser: 1, // 1s
    },
};

module.exports = GameConst;