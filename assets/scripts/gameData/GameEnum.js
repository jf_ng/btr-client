"use strict";

var GameEnum = {
    TankFlag: cc.Enum({
        Player: 0,
        Enemy: 1
    }),

    Force: cc.Enum({
        Blue: 1,
        Red: 2
    }),

    TileType: cc.Enum({
        tileNone: 0,
        tileSteel: 1,
        tileBarrier1: 2,
        tileBarrier2: 3,
        tileGrass: 4,
        tileWood: 5,
        tileKing: 6,
    }),

    TankType: ['1-1', '1-2', '1-3', '2-1', '2-2', '2-3', '3-1', '3-2', '3-3'],

    BulletRange: cc.Enum({
        normal: 1,
        laser: 2,
    }),

    BulletType: cc.Enum({
        light: 1,
        medium: 2,
        heavy: 3,
        laser: 4
    }),

    Group: cc.Enum({
        Default: 'DEFAULT',
        Tank: 'TANK',
        Bullet: 'BULLET',
        HUD: 'HUD',
        Barrier: 'BARRIER',

        BarrierSteel: 'BARRIERSTEEL',
        Barrier1: 'BARRIER1',
        Barrier2: 'BARRIER2',
        BarrierGrass: 'BARRIERGRASS',
        BarrierWood: 'BARRIERWOOD',
        BarrierKing: 'BARRIERKING',
        
        Bullet1: 'BULLET1',
        Bullet2: 'BULLET2',
        Bullet3: 'BULLET3',
        Bullet4: 'BULLET4',
        
        Shard: 'SHARD',
        ShardBullet: 'SHARD_BULLET',
        ShardSpeed: 'SHARD_SPEED',
    }),

    // drop shards load
    Load: cc.Enum({
        small: 1,
        medium: 2,
        large: 3
    }),

    BurstState: cc.Enum({
        start: 1,
        stop: 2,
        stopping: 3
    }),
};

module.exports = GameEnum;