"use strict";

var GameEnum = require("../../gameData/GameEnum");
var GameConst = require("../../gameData/GameConst");
var NodePoolManager = require("../NodePoolManager");
var Util = require("../../Util");

cc.Class({
    extends: cc.Component,

    editor: {
        // executeInEditMode: true
    },

    properties: {
        player_id: null,
        player_name: null,

        isAuto: false,
        _dead: false,
        
        tankSpriteAtlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        tankSpriteFrames: {
            default: [],
            type: cc.SpriteFrame
        },

        force: {
            default: null,
            type: GameEnum.Force,
            // notify: function() {
            //     this.updateTank();
            // }
        },

        // // 1-1 1-2 etc
        tankType: null,

        /**************************************************/
        
        tankDirection: {
            default: GameConst.Direction[0],
            notify: function() {
                this.updateTankSpriteFrame();
            }
        },

        _tankNewDirection: {
            default: null
        },

        // position to be set before tween
        _tankStartPosition: {
            default: null
        },
        // position to be reached by tween
        _tankDestPosition: {
            default: null
        },

        // _tankMoving: false,

        _tankMoveSoundEffect: null,

        _tankMoveAction: {
            default: null,
            type: cc.action
        },

        _pauseTank: false,

        _burstState: null,
        _burstTime: 0,

        /**************************************************/

        // AI
        _toHide: false,
        _collided: false,
        _stuck: 0,

        _pathFinder: null,
        _pathBlocked: false,
        _pathToKing: null,
        _pathToPlayer: null,

        /**************************************************/

        // light medium heavy
        bulletType: {
            default: null,
        },

        bullet: null,
        _fireBullet: false,

        bulletPrefab: {
            default: null,
            type: cc.Prefab
        },

        bullet1Prefab: {
            default: null,
            type: cc.Prefab
        },

        bullet2Prefab: {
            default: null,
            type: cc.Prefab
        },

        bullet3Prefab: {
            default: null,
            type: cc.Prefab
        },


        laserPrefab: {
            default: null,
            type: cc.Prefab
        },

        laser: null,

        _fireLaser: false,

        /**************************************************/

        _label: null,

        _debug: false,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
    },

    onEnable() {
    },

    log(...data) {
        if (this._debug) Util.log(data);
    },

    cleanup() {
        this.hideNewDirection();

        this.stopMove();
        this._burstState = GameEnum.BurstState.stop;

        this.cleanupWeapon();
        this.node.cleanup();
    },

    onDisable() {
        this.cleanup();
    },

    // call by tankgroupmanager
    hide(bool) {
        this.log('hide', bool);
        this.log(this._pauseTank, this._toHide, this._collided, this._stuck, this._pathBlocked, this._tankMoveAction, this._pathToKing, this._pathToPlayer);

        if (bool) {
            this._pathBlocked = false;
            this._pathToKing = null;
            this._pathToPlayer = null;

            if (this.node.active) {
                this.node.getChildByName('born').active = false;
                this.node.getChildByName('explode').active = false;

                if (this._tankMoveAction) {
                    this._toHide = true;
                }
                else {
                    this.cleanup();
                    this.node.setPosition(this._tankDestPosition);
                    this.node.active = false;
                }
            }

            if (this._label) {
                this._label.active = true;

                var percentX = (this.node.position.x - cc.find('Canvas').getComponent('Game')._playerTank.position.x) / cc.winSize.width;
                var percentY = (this.node.position.y - cc.find('Canvas').getComponent('Game')._playerTank.position.y) / cc.winSize.height;

                if (Math.abs(percentX) >= Math.abs(percentY)) {
                    // right
                    if (this.node.position.x > cc.find('Canvas').getComponent('Game')._playerTank.position.x) {
                        this._label.getComponent(cc.Widget).isAlignRight = true;
                        this._label.getComponent(cc.Widget).right = 0;

                        this._label.getComponent(cc.Widget).isAlignLeft = false;
                        this._label.getComponent(cc.Widget).left = 0;

                        this._label.getComponent(cc.Widget).isAlignTop = false;
                        this._label.getComponent(cc.Widget).up = 0;

                        this._label.getComponent(cc.Widget).isAlignBottom = false;
                        this._label.getComponent(cc.Widget).down = 0;

                        this._label.getComponent(cc.Widget).updateAlignment();


                        this._label.y = this.node.position.y / (cc.find("Canvas").getComponent("TiledMapManager").mapResHeight - cc.find("Canvas").getComponent("TiledMapManager").tileSize.height) * cc.winSize.height;

                        this._label.getChildByName('texture').scaleX = 1;
                        this._label.angle = 0;
                    }
                    // left
                    else {
                        this._label.getComponent(cc.Widget).isAlignRight = false;
                        this._label.getComponent(cc.Widget).right = 0;

                        this._label.getComponent(cc.Widget).isAlignLeft = true;
                        this._label.getComponent(cc.Widget).left = 0;

                        this._label.getComponent(cc.Widget).isAlignTop = false;
                        this._label.getComponent(cc.Widget).up = 0;

                        this._label.getComponent(cc.Widget).isAlignBottom = false;
                        this._label.getComponent(cc.Widget).down = 0;

                        this._label.getComponent(cc.Widget).updateAlignment();


                        this._label.y = this.node.position.y / (cc.find("Canvas").getComponent("TiledMapManager").mapResHeight - cc.find("Canvas").getComponent("TiledMapManager").tileSize.height) * cc.winSize.height;

                        this._label.getChildByName('texture').scaleX = -1;
                        this._label.angle = 0;
                    }
                }
                else {
                    // up
                    if (this.node.position.y > cc.find('Canvas').getComponent('Game')._playerTank.position.y) {
                        this._label.getComponent(cc.Widget).isAlignTop = true;
                        this._label.getComponent(cc.Widget).up = 0.1; // 10%

                        this._label.getComponent(cc.Widget).isAlignBottom = false;
                        this._label.getComponent(cc.Widget).down = 0;

                        this._label.getComponent(cc.Widget).isAlignRight = false;
                        this._label.getComponent(cc.Widget).right = 0;

                        this._label.getComponent(cc.Widget).isAlignLeft = false;
                        this._label.getComponent(cc.Widget).left = 0;

                        this._label.getComponent(cc.Widget).updateAlignment();


                        this._label.x = this.node.position.x / (cc.find("Canvas").getComponent("TiledMapManager").mapResWidth - cc.find("Canvas").getComponent("TiledMapManager").tileSize.width) * cc.winSize.width;

                        this._label.getChildByName('texture').scaleX = 1;
                        this._label.angle = 90;
                    }
                    // down
                    else {
                        this._label.getComponent(cc.Widget).isAlignTop = false;
                        this._label.getComponent(cc.Widget).up = 0;

                        this._label.getComponent(cc.Widget).isAlignBottom = true;
                        this._label.getComponent(cc.Widget).down = 0.1; // 10%

                        this._label.getComponent(cc.Widget).isAlignRight = false;
                        this._label.getComponent(cc.Widget).right = 0;

                        this._label.getComponent(cc.Widget).isAlignLeft = false;
                        this._label.getComponent(cc.Widget).left = 0;

                        this._label.getComponent(cc.Widget).updateAlignment();


                        this._label.x = this.node.position.x / (cc.find("Canvas").getComponent("TiledMapManager").mapResWidth - cc.find("Canvas").getComponent("TiledMapManager").tileSize.width) * cc.winSize.width;

                        this._label.getChildByName('texture').scaleX = 1;
                        this._label.angle = -90;
                    }
                }
            } // end if (this._label)
        }
        else {
            this._toHide = false;

            this.node.active = true;

            if (this._label) {
                this._label.active = false;
            }
            
            if (this._pauseTank) {
                this._pauseTank = false;
            }

            if (this.isAuto) {
                this.AIMoveCallback().then(move => {});
            }
        }
    },

    // call by tankgroupmanager
    pause(bool) {
        this._pauseTank = bool;
    },


    /**************************************************
    *
    * initialization
    *
    **************************************************/
    initTank(id, name, force, photo, direction, group, auto, position, type, label) {
        this.node.player_id = id;
        this.node.player_name = name;
        this.node.player_photo = photo;

        this.node.group = group;
        this.isAuto = auto;

        this.node.setPosition(position);
        this._tankDestPosition = position;
        this._tankStartPosition = position;
        this._tankNewDirection = null;
        this._dead = false;
        this._label = label;

        this._moveActionSpeed = GameConst.TankMoveSpeed;
        this._burstState = GameEnum.BurstState.stop;
        this._burstTime = 0;

        if (force == 'Blue') {
            this.force = GameEnum.Force.Blue;
            this.node.force = GameEnum.Force.Blue;

            this.tankType = type + '-blue';
        }
        else {
            this.force = GameEnum.Force.Red;
            this.node.force = GameEnum.Force.Red;

            this.tankType = type + '-red';
        }

        this.updateTank();

        this.tankDirection = direction;

        // this.updateTank().then(data => {
        //     resolve();
        // });

        if (auto && !this._pathFinder) {
            this._pathFinder = cc.find("Canvas").getComponent("TiledMapManager").getPathFinder();
        }


        cc.find('Canvas').getComponent('TankGroupManager').addTank(this.force, this.node);
    },

    begin(respawn) {
        this.log('begin', respawn);

        if (this.isAuto) this.node.active = true;

        if (respawn) {
            this._dead = false;
            this.node.getChildByName('born').active = true;
            
            cc.tween(this.node.getChildByName('born'))
            .to(0, { scale: 0.8 })
            .to(0.15, { scale: 1.2 })
            .to(0.30, { scale: 0.6 })
            .to(0.45, { scale: 1.2 })
            .to(0.60, { scale: 0.6 })
            .to(0.75, { scale: 1.2 })
            .to(0.90, { scale: 0.6 })
            .call(() => {
                this.node.getChildByName('born').active = false;
            })
            .start();
        }
    },

    updateTank() {
        this.showName();

        var newSpriteFrame = null;

        switch (this.tankType) {
            case '1-1-blue':
                this.bulletType = GameEnum.BulletType.light;
                newSpriteFrame = this.tankSpriteFrames[0];
                break;
            case '1-2-blue':
                this.bulletType = GameEnum.BulletType.light;
                newSpriteFrame = this.tankSpriteFrames[1];
                break;
            case '1-3-blue':
                this.bulletType = GameEnum.BulletType.light;
                newSpriteFrame = this.tankSpriteFrames[2];
                break;
            case '1-1-red':
                this.bulletType = GameEnum.BulletType.light;
                newSpriteFrame = this.tankSpriteFrames[3];
                break;
            case '1-2-red':
                this.bulletType = GameEnum.BulletType.light;
                newSpriteFrame = this.tankSpriteFrames[4];
                break;
            case '1-3-red':
                this.bulletType = GameEnum.BulletType.light;
                newSpriteFrame = this.tankSpriteFrames[5];
                break;
            case '2-1-blue':
                this.bulletType = GameEnum.BulletType.medium;
                newSpriteFrame = this.tankSpriteFrames[6];
                break;
            case '2-2-blue':
                this.bulletType = GameEnum.BulletType.medium;
                newSpriteFrame = this.tankSpriteFrames[7];
                break;
            case '2-3-blue':
                this.bulletType = GameEnum.BulletType.medium;
                newSpriteFrame = this.tankSpriteFrames[8];
                break;
            case '2-1-red':
                this.bulletType = GameEnum.BulletType.medium;
                newSpriteFrame = this.tankSpriteFrames[9];
                break;
            case '2-2-red':
                this.bulletType = GameEnum.BulletType.medium;
                newSpriteFrame = this.tankSpriteFrames[10];
                break;
            case '2-3-red':
                this.bulletType = GameEnum.BulletType.medium;
                newSpriteFrame = this.tankSpriteFrames[11];
                break;
            case '3-1-blue':
                this.bulletType = GameEnum.BulletType.heavy;
                newSpriteFrame = this.tankSpriteFrames[12];
                break;
            case '3-2-blue':
                this.bulletType = GameEnum.BulletType.heavy;
                newSpriteFrame = this.tankSpriteFrames[13];
                break;
            case '3-3-blue':
                this.bulletType = GameEnum.BulletType.heavy;
                newSpriteFrame = this.tankSpriteFrames[14];
                break;
            case '3-1-red':
                this.bulletType = GameEnum.BulletType.heavy;
                newSpriteFrame = this.tankSpriteFrames[15];
                break;
            case '3-2-red':
                this.bulletType = GameEnum.BulletType.heavy;
                newSpriteFrame = this.tankSpriteFrames[16];
                break;
            case '3-3-red':
                this.bulletType = GameEnum.BulletType.heavy;
                newSpriteFrame = this.tankSpriteFrames[17];
                break;
        }

        this.node.getComponent(cc.Sprite).spriteFrame = newSpriteFrame;
        
        switch (this.bulletType) {
            case GameEnum.BulletType.light:
                this.bulletPrefab = this.bullet1Prefab;
                break;
            case GameEnum.BulletType.medium:
                this.bulletPrefab = this.bullet2Prefab;
                break;
            case GameEnum.BulletType.heavy:
                this.bulletPrefab = this.bullet3Prefab;
                break;
        }

        this.preloadWeapon();
    },

    updateTankSpriteFrame() {
        var tankSprite = this.node.getComponent(cc.Sprite);
        var oldSpriteFrameName = tankSprite.spriteFrame.name;
        var newSpriteFrameName = null;

        for (let i = 0; i < GameConst.DirectionRex.length; i++) {
            newSpriteFrameName = oldSpriteFrameName.replace(GameConst.DirectionRex[i], this.tankDirection);

            if (newSpriteFrameName !== oldSpriteFrameName) {
                break;
            }
        }

        if (this.tankSpriteAtlas.getSpriteFrame(newSpriteFrameName)) {
            tankSprite.spriteFrame = this.tankSpriteAtlas.getSpriteFrame(newSpriteFrameName);
        }
    },

    showName() {
        this.node.getChildByName('player name blue').active = false;
        this.node.getChildByName('player name red').active = false;

        if (Util.playerData.settings.names != 1 || this.isAuto) return;

        var found = false;
        for (var i = 0; i < Util.playerData.friends.length; i++) {
            if (Util.playerData.friends[i].friend_id == this.node.player_id) {
                found = true;
                break;
            }
        }

        if (found) {
            switch (this.force) {
                case GameEnum.Force.Blue:
                    this.node.getChildByName('player name blue').getComponent(cc.Label).string = this.node.player_name;
                    this.node.getChildByName('player name blue').active = true;
                    break;
                case GameEnum.Force.Red:
                    this.node.getChildByName('player name red').getComponent(cc.Label).string = this.node.player_name;
                    this.node.getChildByName('player name red').active = true;
                    break;
            }
        }
    },


    /**************************************************
    *
    * actions
    *
    **************************************************/
    // AI
    changeTankDirection(rates) {
        var direction;

        if (!rates) {
            switch (this.tankDirection) {
                case GameConst.Direction[0]:
                    rates = [2, 1, 1, 1];
                    break;
                case GameConst.Direction[1]:
                    rates = [1, 2, 1, 1];
                    break;
                case GameConst.Direction[2]:
                    rates = [1, 1, 2, 1];
                    break;
                case GameConst.Direction[3]:
                    rates = [1, 1, 1, 2];
                    break;
            }
        }

        try {
            direction = GameConst.Direction[Util.chance.weighted([0, 1, 2, 3], rates)];

            var collide = Util.hitBarrier(this.node.position, direction);
            if (collide === true) {
                switch (direction) {
                    case GameConst.Direction[0]:
                        rates[0] = 0;
                        break;
                    case GameConst.Direction[2]:
                        rates[2] = 0;
                        break;
                    case GameConst.Direction[1]:
                        rates[1] = 0;
                        break;
                    case GameConst.Direction[3]:
                        rates[3] = 0;
                        break;
                }
                this.changeTankDirection(rates);
            }
            else {
                this._tankNewDirection = direction;
            }
        } catch(err) {}
    },

    move() {
        if (this._tankMoveAction) return;

        if (this._pauseTank) {
            this.cleanup();
        }
        else {
            this.tween();
        }
    },

    tween() {
        this.hideNewDirection();

        if (this._fireLaser) {
            this.prepareBullet();

            this._tankMoveAction = cc.tween(this.laser)
            .by(GameConst.BulletSpeed.laser, {position: cc.v2(this.laser._deltaX, this.laser._deltaY)})
            .call(() => {
                this.putLaser();
            })
            .start();

            this._fireLaser = false;
            this._pathBlocked = false;
        }

        else if (this._fireBullet) {
            this.prepareBullet();
            
            this._tankMoveAction = cc.tween(this.bullet)
            .by(GameConst.BulletSpeed.normal, {position: cc.v2(this.bullet._deltaX, this.bullet._deltaY)})
            .call(() => {
                this.putBullet();
            })
            .start();

            this._fireBullet = false;
            this._pathBlocked = false;
        }

        // burst
        else if (this._burstState == GameEnum.BurstState.start) {
            if (!this._tankNewDirection) this._tankNewDirection = this.tankDirection;

            var collide = Util.hitBarrier(this._tankStartPosition, this._tankNewDirection);
            if (collide === true) {
                this._collided = true;
                this.stopBurst();

                if (this.node.player_id == Util.playerData.uid) cc.find('Canvas').getComponent('TankGroupManager').emitCollide();
            }
            else {
                this._pathBlocked = false;
                this._collided = false;
                this._moveStatus = 'burst';

                this._tankDestPosition = collide;
                this._tankStartPosition = collide;

                this._moveActionSpeed = GameConst.TankBurst.speed;

                if (this.tankDirection != this._tankNewDirection) this.tankDirection = this._tankNewDirection;

                // get dist per cycle
                var x = 0, y = 0;
                switch (this.tankDirection) {
                    case GameConst.Direction[0]:
                        y = cc.find("Canvas").getComponent("TiledMapManager").tileSize.height;
                        break;
                    case GameConst.Direction[2]:
                        y = -cc.find("Canvas").getComponent("TiledMapManager").tileSize.height;
                        break;
                    case GameConst.Direction[1]:
                        x = -cc.find("Canvas").getComponent("TiledMapManager").tileSize.width;
                        break;
                    case GameConst.Direction[3]:
                        x = cc.find("Canvas").getComponent("TiledMapManager").tileSize.width;
                        break;
                }

                if (this.node.player_id == Util.playerData.uid && this._tankMoveSoundEffect == null) {
                    this._tankMoveSoundEffect = cc.find('Canvas').getComponent("SoundManager").playEffectSound("speed", true);
                }

                this._tankMoveAction = cc.tween(this.node)
                .to(this._moveActionSpeed, { position: this._tankDestPosition })
                .call(() => {
                    var collide = Util.hitBarrier(this._tankDestPosition, this.tankDirection);
                    if (collide === true) {
                        this._collided = true;
                        this.stopBurst();

                        this._tankStartPosition = this._tankDestPosition;

                        if (this.node.player_id == Util.playerData.uid) cc.find('Canvas').getComponent('TankGroupManager').emitCollide();
                    }
                    else {
                        this._tankDestPosition = collide;
                    }
                })
                .by(this._moveActionSpeed, { position: cc.v2(x, y) })
                .call(() => {
                    var collide = Util.hitBarrier(this._tankDestPosition, this.tankDirection);
                    if (collide === true) {
                        this._collided = true;
                        this.stopBurst();

                        this._tankStartPosition = this._tankDestPosition;

                        if (this.node.player_id == Util.playerData.uid) cc.find('Canvas').getComponent('TankGroupManager').emitCollide();
                    }
                    else {
                        this._tankDestPosition = collide;
                    }
                })
                .by(this._moveActionSpeed, { position: cc.v2(x, y) })
                .call(() => {
                    var collide = Util.hitBarrier(this._tankDestPosition, this.tankDirection);
                    if (collide === true) {
                        this._collided = true;
                        this.stopBurst();

                        this._tankStartPosition = this._tankDestPosition;

                        if (this.node.player_id == Util.playerData.uid) cc.find('Canvas').getComponent('TankGroupManager').emitCollide();
                    }
                    else {
                        this._tankDestPosition = collide;
                    }
                })
                .by(this._moveActionSpeed, { position: cc.v2(x, y) })
                .call(() => {
                    this.stopBurst();

                    this._tankStartPosition = this._tankDestPosition;
                })
                .start();
            }
        }

        // normal
        else {
            if (!this._tankNewDirection) this._tankNewDirection = this.tankDirection;

            var collide = Util.hitBarrier(this._tankStartPosition, this._tankNewDirection);
            if (collide === true) {
                this._collided = true;
                this.stopMove();

                if (this.node.player_id == Util.playerData.uid) {
                    cc.find('Canvas').getComponent('TankGroupManager').emitCollide();

                    cc.find('Canvas').getComponent('TankGroupManager').toggleProcessPlayerInput(true);
                    cc.find('Canvas').getComponent('Game').enableSpeed();
                    cc.find('Canvas').getComponent('Game').enableFire();
                }
            }
            else {
                this._collided = false;
                this._moveStatus = 'move';

                this._tankDestPosition = collide;
                this._tankStartPosition = collide;

                if (this.tankDirection != this._tankNewDirection) this.tankDirection = this._tankNewDirection;

                this._moveActionSpeed = GameConst.TankMoveSpeed;

                this._tankMoveAction = cc.tween(this.node)
                .to(this._moveActionSpeed, { position: this._tankDestPosition })
                .call(() => {
                    this.stopMove();
                    
                    if (this.isAuto) {
                        if (!this._toHide && this.node.active) {
                            this.AIMoveCallback();
                        }
                    }
                    else this.move();
                })
                .start();

                // loop
                if (this.node.player_id == Util.playerData.uid) {
                    this.scheduleOnce(function() {
                        cc.find('Canvas').getComponent('TankGroupManager').toggleProcessPlayerInput(true);
                        cc.find('Canvas').getComponent('TankGroupManager').processInput();
                    }, this._moveActionSpeed - 0.2);
                }
            }
        }
    },

    stopMove() {
        if (this._tankMoveAction) {
            this._tankMoveAction.stop();
            this._tankMoveAction = null;
        }

        if (this._tankMoveSoundEffect != null) {
            cc.find('Canvas').getComponent("SoundManager").stopEffectSound(this._tankMoveSoundEffect);
            this._tankMoveSoundEffect = null;
        }

        this._moveStatus = 'stop';

        if (this._toHide) {
            this.node.setPosition(this._tankDestPosition);
            this.node.active = false;
            this._toHide = false;
        }
    },

    stopBurst() {
        this.stopMove();

        this._burstState = GameEnum.BurstState.stop;

        if (this.node.player_id == Util.playerData.uid) {
            cc.find('Canvas').getComponent('TankGroupManager').toggleProcessPlayerInput(true);
            cc.find('Canvas').getComponent('Game').enableSpeed();
            cc.find('Canvas').getComponent('Game').enableFire();
        }
    },

    inputCallback(direction, speed, bullet) {
        this._tankNewDirection = direction;

        if (bullet === GameEnum.BulletRange.normal) {
            this._fireBullet = true;
        }
        else if (bullet === GameEnum.BulletRange.laser) {
            this._fireLaser = true;
        }

        if (speed) this._burstState = GameEnum.BurstState.start;

        if (!this._tankMoveAction) {
            this.scheduleOnce(function() {
                this.move();
            }, 0.2);
        }
    },

    showNewDirection(direction) {
        // Direction: ["up", "left", "down", "right"],
        switch (direction) {
            case GameConst.Direction[0]:
                this.node.getChildByName('up').active = true;
                break;
            case GameConst.Direction[2]:
                this.node.getChildByName('down').active = true;
                break;
            case GameConst.Direction[1]:
                this.node.getChildByName('left').active = true;
                break;
            case GameConst.Direction[3]:
                this.node.getChildByName('right').active = true;
                break;
        }
    },

    hideNewDirection() {
        this.node.getChildByName('up').active = false;
        this.node.getChildByName('down').active = false;
        this.node.getChildByName('left').active = false;
        this.node.getChildByName('right').active = false;
    },

    /**************************************************
    *
    * fire if enemy in front
    *
    * move to king
    *
    * change direction if enemy within other direction range
    *
    * move to player
    *
    **************************************************/
    AIMoveCallback() {
        this.log('AIMoveCallback');

        var self = this;

        return new Promise(function(main_resolve, main_reject) {
            // for some fuck reason _tankMoveAction will not be clear
            // and tank stuck not doing anything
            if (self._tankMoveAction) {
                self.log('stuck');

                if (++self._stuck >= 2) {
                    self.log('unstuck');
                    self.cleanup();
                    self._stuck = 0;
                }

                main_resolve(false);
            }
            else {
                self._stuck = 0;

                if (self._collided) {
                    self.log('collide');

                    switch (self.tankDirection) {
                        case GameConst.Direction[0]:
                            var rates = [0, 1, 1, 1];
                            break;
                        case GameConst.Direction[1]:
                            var rates = [1, 0, 1, 1];
                            break;
                        case GameConst.Direction[2]:
                            var rates = [1, 1, 0, 1];
                            break;
                        case GameConst.Direction[3]:
                            var rates = [1, 1, 1, 0];
                            break;
                    }
                    self.changeTankDirection(rates);

                    self.move();

                    main_resolve(true);
                }
                else {
                    /**************************************************
                     * fire if enemy in front
                     **************************************************/
                    var enemies = cc.find('Canvas').getComponent('TankGroupManager').getForceTanks(((self.force == GameEnum.Force.Blue) ? GameEnum.Force.Red : GameEnum.Force.Blue));
                    var enemy, fire = false;

                    for (var i = 0; i < enemies.length; i++) {
                        if (!enemies[i].active) continue;

                        // kill player but not other human players
                        if (!enemies[i].getComponent('TankManager').isAuto && enemies[i].player_id != Util.playerData.uid) continue;

                        if (self.node.position.x == enemies[i].position.x && self.node.position.y == enemies[i].position.y) {
                            fire = true;
                            enemy = enemies[i];
                        }
                        else {
                            // Direction: ["up", "left", "down", "right"],
                            switch (self.tankDirection) {
                                case GameConst.Direction[0]:
                                    if (self.node.position.x == enemies[i].position.x && enemies[i].position.y > self.node.position.y && enemies[i].position.y - self.node.position.y <= GameConst.BulletRange.laser) {
                                        fire = true;
                                        enemy = enemies[i];
                                    }
                                    break;
                                case GameConst.Direction[2]:
                                    if (self.node.position.x == enemies[i].position.x && self.node.position.y > enemies[i].position.y && self.node.position.y - enemies[i].position.y <= GameConst.BulletRange.laser) {
                                        fire = true;
                                        enemy = enemies[i];
                                    }
                                    break;
                                case GameConst.Direction[1]:
                                    if (self.node.position.y == enemies[i].position.y && self.node.position.x > enemies[i].position.x && self.node.position.x - enemies[i].position.x <= GameConst.BulletRange.laser) {
                                        fire = true;
                                        enemy = enemies[i];
                                    }
                                    break;
                                case GameConst.Direction[3]:
                                    if (self.node.position.y == enemies[i].position.y && enemies[i].position.x > self.node.position.x && enemies[i].position.x - self.node.position.x <= GameConst.BulletRange.laser) {
                                        fire = true;
                                        enemy = enemies[i];
                                    }
                                    break;
                            }
                        }

                        if (fire) break;
                    }

                    self.log('fire', fire);
                    if (fire) self.log('shotHitTarget', self.shotHitTarget(enemy));

                    if (fire && self.shotHitTarget(enemy)) {
                        var bullet = Util.chance.weighted([GameEnum.BulletRange.normal, GameEnum.BulletRange.laser], [1, 1]);
                        if (bullet == GameEnum.BulletRange.normal) self._fireBullet = true;
                        else self._fireLaser = true;

                        self.move();
                    
                        main_resolve(true);
                    }
                    else {
                        /**************************************************
                         * move to king
                         **************************************************/
                        self.AIMoveToKing().then(move => {
                            if (move) {
                                self.log('AIMoveToKing');
                                main_resolve(true);
                            }
                            else {
                                /**************************************************
                                 * change direction if enemy within other direction range
                                 **************************************************/
                                // if (self.AIFaceEnemyWithinRange(enemies)) {
                                //     return;
                                // }

                                if (cc.find('Canvas').getComponent('Game')._king) {
                                    self.log('AIMoveToKing', 'free will');
                                    self.changeTankDirection();

                                    self.move();

                                    main_resolve(true);
                                }
                                else {
                                    /**************************************************
                                     * move to player
                                     **************************************************/
                                    self.AIMoveToPlayer().then(move => {
                                        if (move) {
                                            self.log('AIMoveToPlayer');
                                            main_resolve(true);
                                        }
                                        else {
                                            self.log('AIMoveToPlayer', 'free will');

                                            // AI same force as player
                                            self.changeTankDirection();

                                            self.move();

                                            main_resolve(true);
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });
    },

    shotHitTarget(target) {
        var position, collide = false;

        // Direction: ["up", "left", "down", "right"],
        switch (this.tankDirection) {
            case GameConst.Direction[0]:
                for (var k = 0; k < GameConst.BulletRange.laser / cc.find("Canvas").getComponent("TiledMapManager").tileSize.height; k++) {
                    position = {x: this.node.position.x, y: this.node.position.y + (k * cc.find("Canvas").getComponent("TiledMapManager").tileSize.height)};

                    collide = Util.hitBarrier(position, this.tankDirection);
                    if (collide === true) {
                        break;
                    }
                }

                break;
            case GameConst.Direction[1]:
                for (var k = 0; k < GameConst.BulletRange.laser / cc.find("Canvas").getComponent("TiledMapManager").tileSize.width; k++) {
                    position = {x: this.node.position.x - (k * cc.find("Canvas").getComponent("TiledMapManager").tileSize.width), y: this.node.position.y};

                    collide = Util.hitBarrier(position, this.tankDirection);
                    if (collide === true) {
                        break;
                    }
                }

                break;
            case GameConst.Direction[2]:
                for (var k = 0; k < GameConst.BulletRange.laser / cc.find("Canvas").getComponent("TiledMapManager").tileSize.height; k++) {
                    position = {x: this.node.position.x, y: this.node.position.y - (k * cc.find("Canvas").getComponent("TiledMapManager").tileSize.height)};

                    collide = Util.hitBarrier(position, this.tankDirection);
                    if (collide === true) {
                        break;
                    }
                }

                break;
            case GameConst.Direction[3]:
                for (var k = 0; k < GameConst.BulletRange.laser / cc.find("Canvas").getComponent("TiledMapManager").tileSize.width; k++) {
                    position = {x: this.node.position.x + (k * cc.find("Canvas").getComponent("TiledMapManager").tileSize.width), y: this.node.position.y};

                    collide = Util.hitBarrier(position, this.tankDirection);
                    if (collide === true) {
                        break;
                    }
                }

                break;
        }

        return (collide === true) ? false : true;
    },

    AIMoveToKing() {
        var self = this;

        return new Promise(function(main_resolve, main_reject) {
            new Promise(function(resolve, reject) {
                if (!cc.find('Canvas').getComponent('Game')._king) {
                    self._pathToKing = null;
                    resolve();
                }
                else {
                    // use existing path if exists, so wont keep calling path finder
                    if (!self._pathToKing) {
                        self.pathToKing(self.node.position).then(path => {
                            self._pathToKing = path;
                            resolve();
                        });
                    }
                    else {
                        resolve();
                    }
                }
            }).then(res => {
                self.log('AIMoveToKing', cc.find('Canvas').getComponent('Game')._king, self._pathToKing);

                /**************************************************
                 * in case path is null calling path finder
                 * >2 mean already outside wood area
                **************************************************/
                if (self._pathToKing && self._pathToKing.length > 2) {

                    /**************************************************
                     * check here again because findpath promise
                    **************************************************/
                    if (!cc.find('Canvas').getComponent('Game')._king) {
                        self._pathToKing = null;
                        main_resolve(false);
                    }
                    else if (self._tankMoveAction) {
                        main_resolve(false);
                    }
                    else {
                        var dest = cc.find("Canvas").getComponent("TiledMapManager").getTilePositionAt(self._pathToKing[0].x + 0.5, self._pathToKing[0].y + 0.5);

                        // up
                        if (dest.x == self.node.position.x && dest.y > self.node.position.y) {
                            self._tankNewDirection = 'up';

                            if (
                                self._pathToKing.length >= 4 &&
                                (self._pathToKing[1].x == self._pathToKing[0].x && self._pathToKing[1].y < self._pathToKing[0].y) &&
                                (self._pathToKing[2].x == self._pathToKing[1].x && self._pathToKing[2].y < self._pathToKing[1].y) &&
                                (self._pathToKing[3].x == self._pathToKing[2].x && self._pathToKing[3].y < self._pathToKing[2].y)
                            )
                            {
                                self._burstState = GameEnum.BurstState.start;
                            }
                        }
                        // down
                        else if (dest.x == self.node.position.x && dest.y < self.node.position.y) {
                            self._tankNewDirection = 'down';

                            if (
                                self._pathToKing.length >= 4 &&
                                (self._pathToKing[1].x == self._pathToKing[0].x && self._pathToKing[1].y > self._pathToKing[0].y) &&
                                (self._pathToKing[2].x == self._pathToKing[1].x && self._pathToKing[2].y > self._pathToKing[1].y) &&
                                (self._pathToKing[3].x == self._pathToKing[2].x && self._pathToKing[3].y > self._pathToKing[2].y)
                            )
                            {
                                self._burstState = GameEnum.BurstState.start;
                            }
                        }
                        // left
                        else if (dest.y == self.node.position.y && dest.x < self.node.position.x) {
                            self._tankNewDirection = 'left';

                            if (
                                self._pathToKing.length >= 4 &&
                                (self._pathToKing[1].y == self._pathToKing[0].y && self._pathToKing[1].x < self._pathToKing[0].x) &&
                                (self._pathToKing[2].y == self._pathToKing[1].y && self._pathToKing[2].x < self._pathToKing[1].x) &&
                                (self._pathToKing[3].y == self._pathToKing[2].y && self._pathToKing[3].x < self._pathToKing[2].x)
                            )
                            {
                                self._burstState = GameEnum.BurstState.start;
                            }
                        }
                        // right
                        else if (dest.y == self.node.position.y && dest.x > self.node.position.x) {
                            self._tankNewDirection = 'right';

                            if (
                                self._pathToKing.length >= 4 &&
                                (self._pathToKing[1].y == self._pathToKing[0].y && self._pathToKing[1].x > self._pathToKing[0].x) &&
                                (self._pathToKing[2].y == self._pathToKing[1].y && self._pathToKing[2].x > self._pathToKing[1].x) &&
                                (self._pathToKing[3].y == self._pathToKing[2].y && self._pathToKing[3].x > self._pathToKing[2].x)
                            )
                            {
                                self._burstState = GameEnum.BurstState.start;
                            }
                        }

                        if (self._burstState == GameEnum.BurstState.start) self._pathToKing.splice(0, 4);
                        else self._pathToKing.splice(0, 1);

                        self.move();

                        main_resolve(true);
                    }
                }
                else {
                    main_resolve(false);
                }
            });
        });
    },

    /**************************************************
     * reuse path for 2 times
     * ignore if calculated path not updated after boom etc
     * cos is just 2 wrong steps
     **************************************************/
    AIMoveToPlayer() {
        var self = this;

        return new Promise(function(main_resolve, main_reject) {
            new Promise(function(resolve, reject) {
                if (self.force == cc.find('Canvas').getComponent('Game')._playerTank.getComponent('TankManager').force) {
                    self.log('AIMoveToPlayer', 1);
                    resolve(false);
                }
                else if (self._pathToPlayer && self._pathToPlayer.length > 0) {
                    self.log('AIMoveToPlayer', 2);
                    resolve(false);
                }
                else {
                    self.pathToPlayer(self.node.position).then(path => {
                        self.log('AIMoveToPlayer', 3, path);
                        self._pathToPlayer = path;
                        resolve(true);
                    });
                }
            }).then(fresh => {
                self.log('AIMoveToPlayer', self._tankMoveAction, self._pathToPlayer);

                if (self._tankMoveAction) {
                    main_resolve(false);
                }
                else if (self._pathToPlayer && self._pathToPlayer.length > 0) {
                    var dest = cc.find("Canvas").getComponent("TiledMapManager").getTilePositionAt(self._pathToPlayer[0].x + 0.5, self._pathToPlayer[0].y + 0.5);

                    // up
                    if (dest.x == self.node.position.x && dest.y > self.node.position.y) {
                        self._tankNewDirection = 'up';

                        if (
                            self._pathToPlayer.length >= 4 &&
                            (self._pathToPlayer[1].x == self._pathToPlayer[0].x && self._pathToPlayer[1].y < self._pathToPlayer[0].y) &&
                            (self._pathToPlayer[2].x == self._pathToPlayer[1].x && self._pathToPlayer[2].y < self._pathToPlayer[1].y) &&
                            (self._pathToPlayer[3].x == self._pathToPlayer[2].x && self._pathToPlayer[3].y < self._pathToPlayer[2].y)
                        ) {
                            self._burstState = GameEnum.BurstState.start;
                        }
                    }
                    // down
                    else if (dest.x == self.node.position.x && dest.y < self.node.position.y) {
                        self._tankNewDirection = 'down';

                        if (
                            self._pathToPlayer.length >= 4 &&
                            (self._pathToPlayer[1].x == self._pathToPlayer[0].x && self._pathToPlayer[1].y > self._pathToPlayer[0].y) &&
                            (self._pathToPlayer[2].x == self._pathToPlayer[1].x && self._pathToPlayer[2].y > self._pathToPlayer[1].y) &&
                            (self._pathToPlayer[3].x == self._pathToPlayer[2].x && self._pathToPlayer[3].y > self._pathToPlayer[2].y)
                        ) {
                            self._burstState = GameEnum.BurstState.start;
                        }
                    }
                    // left
                    else if (dest.y == self.node.position.y && dest.x < self.node.position.x) {
                        self._tankNewDirection = 'left';

                        if (
                            self._pathToPlayer.length >= 4 &&
                            (self._pathToPlayer[1].y == self._pathToPlayer[0].y && self._pathToPlayer[1].x < self._pathToPlayer[0].x) &&
                            (self._pathToPlayer[2].y == self._pathToPlayer[1].y && self._pathToPlayer[2].x < self._pathToPlayer[1].x) &&
                            (self._pathToPlayer[3].y == self._pathToPlayer[2].y && self._pathToPlayer[3].x < self._pathToPlayer[2].x)
                        ) {
                            self._burstState = GameEnum.BurstState.start;
                        }
                    }
                    // right
                    else if (dest.y == self.node.position.y && dest.x > self.node.position.x) {
                        self._tankNewDirection = 'right';

                        if (
                            self._pathToPlayer.length >= 4 &&
                            (self._pathToPlayer[1].y == self._pathToPlayer[0].y && self._pathToPlayer[1].x > self._pathToPlayer[0].x) &&
                            (self._pathToPlayer[2].y == self._pathToPlayer[1].y && self._pathToPlayer[2].x > self._pathToPlayer[1].x) &&
                            (self._pathToPlayer[3].y == self._pathToPlayer[2].y && self._pathToPlayer[3].x > self._pathToPlayer[2].x)
                        ) {
                            self._burstState = GameEnum.BurstState.start;
                        }
                    }

                    if (fresh) {
                        if (self._burstState == GameEnum.BurstState.start) self._pathToPlayer.splice(0, 4);
                        else self._pathToPlayer.splice(0, 1);
                    }
                    else {
                        self._pathToPlayer = null;
                    }

                    self.move();

                    main_resolve(true);
                }
                else {
                    main_resolve(false);
                }
            });
        });
    },

    AIFaceEnemyWithinRange(enemies) {
        var direction = {
            right: 999,
            left: 999,
            up: 999,
            down: 999
        };

        for (var i = 0; i < enemies.length; i++) {
            // kill player but not other human players
            if (!enemies[i].getComponent('TankManager').isAuto && enemies[i].player_id != Util.playerData.uid) continue;

            // right
            if (enemies[i].position.y == this.node.position.y && enemies[i].position.x > this.node.position.x && enemies[i].position.x - this.node.position.x <= GameConst.BulletRange.laser) {
                direction.right = enemies[i].position.x - this.node.position.x;
            }
            // left
            else if (enemies[i].position.y == this.node.position.y && this.node.position.x > enemies[i].position.x && this.node.position.x - enemies[i].position.x <= GameConst.BulletRange.laser) {
                direction.left = this.node.position.x - enemies[i].position.x;
            }
            // up
            else if (enemies[i].position.x == this.node.position.x && enemies[i].position.y > this.node.position.y && enemies[i].position.y - this.node.position.y <= GameConst.BulletRange.laser) {
                direction.up = this.node.position.y - enemies[i].position.y
            }
            // down
            else if (enemies[i].position.x == this.node.position.x && this.node.position.y > enemies[i].position.y && this.node.position.y - enemies[i].position.y <= GameConst.BulletRange.laser) {
                direction.down = enemies[i].position.y - this.node.position.y;
            }
        }

        while (true) {
            var move = {
                direction: null,
                dist: 999
            }

            if (direction.right < move.dist) move = {direction: 'right', dist: direction.right};
            if (direction.left < move.dist) move = {direction: 'left', dist: direction.left};
            if (direction.up < move.dist) move = {direction: 'up', dist: direction.up};
            if (direction.down < move.dist) move = {direction: 'down', dist: direction.down};

            if (move.direction != null) {
                var collide = Util.hitBarrier(this.node.position, move.direction);
                if (collide === true) {
                    // Direction: ["up", "left", "down", "right"],
                    switch (move.direction) {
                        case GameConst.Direction[0]:
                            direction.up = 999;
                            break;
                        case GameConst.Direction[2]:
                            direction.down = 999;
                            break;
                        case GameConst.Direction[1]:
                            direction.left = 999;
                            break;
                        case GameConst.Direction[3]:
                            direction.right = 999;
                            break;
                    }
                }
                else {
                    this._tankNewDirection = move.direction;

                    this.move();
                    return true;
                }
            }
            else {
                break;
            }
        }

        return false;
    },

    pathToPlayer(position) {
        var self = this;

        return new Promise(function(resolve, reject) {
            if (!cc.find('Canvas').getComponent('Game')._playerTank.active) resolve(null);
            else {
                var from = cc.find("Canvas").getComponent("TiledMapManager").getLayerPositionAt(position.x, position.y);
                from.x = Math.floor(from.x);
                from.y = Math.floor(from.y);

                var to = cc.find("Canvas").getComponent("TiledMapManager").getLayerPositionAt(cc.find('Canvas').getComponent('Game')._playerTank.position.x, cc.find('Canvas').getComponent('Game')._playerTank.position.y);
                to.x = Math.floor(to.x);
                to.y = Math.floor(to.y);

                self._pathFinder.findPath(from.x, from.y, to.x, to.y, function(path) {
                    // delete first item because it is current position
                    if (path && path.length > 1) path.splice(0, 1);

                    resolve(path);
                });

                self._pathFinder.calculate();
            }
        });
    },

    pathToKing(position) {
        var self = this;

        return new Promise(function(resolve, reject) {
            var from = cc.find("Canvas").getComponent("TiledMapManager").getLayerPositionAt(position.x, position.y);
            from.x = Math.floor(from.x);
            from.y = Math.floor(from.y);

            var to = cc.find("Canvas").getComponent("TiledMapManager").getLayerPositionAt(cc.find('Canvas').getComponent('Game')._king.position.x, cc.find('Canvas').getComponent('Game')._king.position.y);
            to.x = Math.floor(to.x);
            to.y = Math.floor(to.y);

            self._pathFinder.findPath(from.x, from.y, to.x, to.y, function(path) {
                // delete first item because it is current position
                if (path && path.length > 1) path.splice(0, 1);

                resolve(path);
            });

            self._pathFinder.calculate();
        });
    },


    /**************************************************
    *
    * weapon
    *
    **************************************************/
    fireBullet(range) {
        if (this._fireBullet) {
            this._fireBullet = false;

            switch (this.tankDirection) {
                case GameConst.Direction[0]:
                    this.bullet.angle = 0;
                    this.bullet._deltaX = 0;
                    this.bullet._deltaY = GameConst.BulletRange.normal;
                    break;
                case GameConst.Direction[2]:
                    this.bullet.angle = 180;
                    this.bullet._deltaX = 0;
                    this.bullet._deltaY = -GameConst.BulletRange.normal;
                    break;
                case GameConst.Direction[1]:
                    this.bullet.angle = 90;
                    this.bullet._deltaX = -GameConst.BulletRange.normal;
                    this.bullet._deltaY = 0;
                    break;
                case GameConst.Direction[3]:
                    this.bullet.angle = 270;
                    this.bullet._deltaX = GameConst.BulletRange.normal;
                    this.bullet._deltaY = 0;
                    break;
            }

            this.bullet.position = this.node.position;
            this.bullet.active = true;

            if (this.node.player_id == Util.playerData.uid) cc.find('Canvas').getComponent("SoundManager").playEffectSound("shoot", false);

            cc.tween(this.bullet)
            .by(GameConst.BulletSpeed.normal, {position: cc.v2(this.bullet._deltaX, this.bullet._deltaY)})
            .call(() => {
                this.putBullet();
            })
            .start();
        }
        else if (this._fireLaser) {
            this._fireLaser = false;

            switch (this.tankDirection) {
                case GameConst.Direction[0]:
                    this.laser.angle = 0;
                    this.laser._deltaX = 0;
                    this.laser._deltaY = GameConst.BulletRange.laser;
                    break;
                case GameConst.Direction[2]:
                    this.laser.angle = 180;
                    this.laser._deltaX = 0;
                    this.laser._deltaY = -GameConst.BulletRange.laser;
                    break;
                case GameConst.Direction[1]:
                    this.laser.angle = 90;
                    this.laser._deltaX = -GameConst.BulletRange.laser;
                    this.laser._deltaY = 0;
                    break;
                case GameConst.Direction[3]:
                    this.laser.angle = 270;
                    this.laser._deltaX = GameConst.BulletRange.laser;
                    this.laser._deltaY = 0;
                    break;
            }

            this.laser.position = this.node.position;
            this.laser.active = true;

            if (this.node.player_id == Util.playerData.uid) cc.find('Canvas').getComponent("SoundManager").playEffectSound("shootLaser", false);

            cc.tween(this.laser)
            .by(GameConst.BulletSpeed.laser, {position: cc.v2(this.laser._deltaX, this.laser._deltaY)})
            .call(() => {
                this.putLaser();
            })
            .start();
        }
    },

    prepareBullet() {
        if (this._fireBullet) {
            switch (this.tankDirection) {
                case GameConst.Direction[0]:
                    this.bullet.angle = 0;
                    this.bullet._deltaX = 0;
                    this.bullet._deltaY = GameConst.BulletRange.normal;
                    break;
                case GameConst.Direction[2]:
                    this.bullet.angle = 180;
                    this.bullet._deltaX = 0;
                    this.bullet._deltaY = -GameConst.BulletRange.normal;
                    break;
                case GameConst.Direction[1]:
                    this.bullet.angle = 90;
                    this.bullet._deltaX = -GameConst.BulletRange.normal;
                    this.bullet._deltaY = 0;
                    break;
                case GameConst.Direction[3]:
                    this.bullet.angle = 270;
                    this.bullet._deltaX = GameConst.BulletRange.normal;
                    this.bullet._deltaY = 0;
                    break;
            }

            this.bullet.setPosition(this.node.position);
            this.bullet.active = true;

            if (this.node.player_id == Util.playerData.uid) cc.find('Canvas').getComponent("SoundManager").playEffectSound("shoot", false);
        }
        else if (this._fireLaser) {
            this._fireLaser = false;

            switch (this.tankDirection) {
                case GameConst.Direction[0]:
                    this.laser.angle = 0;
                    this.laser._deltaX = 0;
                    this.laser._deltaY = GameConst.BulletRange.laser;
                    break;
                case GameConst.Direction[2]:
                    this.laser.angle = 180;
                    this.laser._deltaX = 0;
                    this.laser._deltaY = -GameConst.BulletRange.laser;
                    break;
                case GameConst.Direction[1]:
                    this.laser.angle = 90;
                    this.laser._deltaX = -GameConst.BulletRange.laser;
                    this.laser._deltaY = 0;
                    break;
                case GameConst.Direction[3]:
                    this.laser.angle = 270;
                    this.laser._deltaX = GameConst.BulletRange.laser;
                    this.laser._deltaY = 0;
                    break;
            }

            this.laser.setPosition(this.node.position);
            this.laser.active = true;

            if (this.node.player_id == Util.playerData.uid) cc.find('Canvas').getComponent("SoundManager").playEffectSound("shootLaser", false);
        }
    },

    preloadWeapon() {
        this.getBullet();

        if (!this.laser) this.laser = this.getLaser();
        else this.laser._player_id = this.node.player_id;
    },

    getBullet() {
        switch (this.bulletType) {
            case GameEnum.BulletType.light:
                this.bullet = NodePoolManager.getNodeElement(GameEnum.Group.Bullet1);
                break;
            case GameEnum.BulletType.medium:
                this.bullet = NodePoolManager.getNodeElement(GameEnum.Group.Bullet2);
                break;
            case GameEnum.BulletType.heavy:
                this.bullet = NodePoolManager.getNodeElement(GameEnum.Group.Bullet3);
                break;
        }

        if (!this.bullet) {
            this.bullet = cc.instantiate(this.bulletPrefab);
        }
        
        this.bullet.group = Util.translateTextToGroup(GameEnum.Group.Bullet);
        this.bullet._player_id = this.node.player_id;
        this.bullet.type = GameEnum.BulletRange.normal;
        this.bullet.active = false;
        this.bullet._tankManager = this;

        cc.find("Canvas").getComponent("Game").tankWarMap.node.getChildByName("bullet").addChild(this.bullet);
    },

    getLaser() {
        var laser = cc.instantiate(this.laserPrefab);
        laser._player_id = this.node.player_id;
        laser.group = Util.translateTextToGroup(GameEnum.Group.Bullet);
        laser.type = GameEnum.BulletRange.laser;
        laser.active = false;
        laser._tankManager = this;

        cc.find("Canvas").getComponent("Game").tankWarMap.node.getChildByName("bullet").addChild(laser);

        return laser;
    },

    putBullet() {
        this.stopMove();

        if (this.bullet) {
            this.bullet.cleanup();
            this.bullet.active = false;
        }

        this._fireBullet = false;

        if (this.node.player_id == Util.playerData.uid) {
            cc.find('Canvas').getComponent('TankGroupManager').toggleProcessPlayerInput(true);
            cc.find('Canvas').getComponent('Game').enableFire();
            cc.find('Canvas').getComponent('Game').enableSpeed();
        }
    },

    putLaser() {
        this.stopMove();

        if (this.laser) {
            this.laser.cleanup();
            this.laser.active = false;
        }

        this._fireLaser = false;

        if (this.node.player_id == Util.playerData.uid) {
            cc.find('Canvas').getComponent('TankGroupManager').toggleProcessPlayerInput(true);
            cc.find('Canvas').getComponent('Game').enableFire();
            cc.find('Canvas').getComponent('Game').enableSpeed();
        }
    },

    cleanupWeapon() {
        this.putBullet();
        this.putLaser();
    },


    /**************************************************
    *
    * events
    *
    **************************************************/
    toggleDebug() {
        if (Util.debug) {
            if (this._debug) this._debug = false;
            else this._debug = true;
        }
    },

    boom(bullet_id) {
        if (this._dead) return;
        this._dead = true;


        this.cleanup();


        // if (this.node.active) {
            var sound = false, anim = false;

            if (this.node.player_id == Util.playerData.uid) {
                sound = true;
                anim = true;
            }
            else if (bullet_id == Util.playerData.uid) {
                sound = true;
                anim = true;

                if (this.force == cc.find('Canvas').getComponent('Game')._playerTank.getComponent('TankManager').force)
                    cc.find('Canvas').getComponent('Game').killTank(-1);
                else cc.find('Canvas').getComponent('Game').killTank(1);
            }
            else {
                if (Util.withinPlayerRange(this.node.position)) {
                    anim = true;
                }
            }

            if (sound) cc.find('Canvas').getComponent("SoundManager").playEffectSound("explode-tank", false);


            var self = this;
            var end = function() {
                self.cleanup();

                self.node.active = false;

                if (self.node.getChildByName('born').active) {
                    self.node.getChildByName('born').cleanup();
                    self.node.getChildByName('born').active = false;
                }

                if (self.node.getChildByName('explode').active) {
                    self.node.getChildByName('explode').cleanup();
                    self.node.getChildByName('explode').active = false;
                }

                self._pathBlocked = false;
                self._pathToKing = null;
                self._pathToPlayer = null;

                if (self.node.player_id == Util.playerData.uid) {
                    cc.find('Canvas').getComponent('TankGroupManager').emitBoom();
                    cc.find('Canvas').getComponent('Game').gameOver();

                    // self.node.setPosition(GameConst.TankStartingPosition);
                }
                else {
                    self.deleteTank();
                }
            }

            if (anim) {
                this.node.getChildByName('explode').active = true;
                cc.tween(this.node.getChildByName('explode'))
                .to(0, { scale: 0.8 })
                .to(0.15, { scale: 1.2 })
                .to(0.30, { scale: 0.8 })
                .call(() => {
                    end();
                })
                .start();
            }
            else {
                end();
            }
        // }
    },

    deleteTank() {
        this.cleanup();

        this.node.getChildByName('explode').active = false;

        switch (this.bulletType) {
            case GameEnum.BulletType.light:
                NodePoolManager.putNodeElement(GameEnum.Group.Bullet1, this.bullet);
                break;
            case GameEnum.BulletType.medium:
                NodePoolManager.putNodeElement(GameEnum.Group.Bullet2, this.bullet);
                break;
            case GameEnum.BulletType.heavy:
                NodePoolManager.putNodeElement(GameEnum.Group.Bullet3, this.bullet);
                break;
        }

        cc.find('Canvas').getComponent('TankGroupManager').deleteTank(this.force, this.node);

        if (this._label) {
            this._label.removeFromParent(false);
            this._label = null;
        }

        if (this.isAuto) {
            cc.find("Canvas").getComponent("Game").scheduleAITank();
        }

        this.node.active = false;

        // will auto remove tank from parent
        NodePoolManager.putNodeElement(this.node.group, this.node);
    },

    playerRespawn(position) {
        this.node.setPosition(position);
        this._tankDestPosition = position;
        this._tankStartPosition = position;

        this._dead = false;
        this.node.active = true;
    },

    onCollisionEnter(other, self) {
        if (Util.translateGroupToText(other.node.group) == GameEnum.Group.Bullet) {
            if (self.node.player_id != other.node._player_id) {
                // cleanup bullet, if laser dun clean up
                if (other.node.type == GameEnum.BulletRange.normal) other.node._tankManager.putBullet();

                this.boom(other.node._player_id);
            }
        }
        // else if (Util.translateGroupToText(other.node.group) == GameEnum.Group.Barrier) {
        // }
        else if (Util.translateGroupToText(other.node.group) == GameEnum.Group.Tank) {
            // this.boom();
        }
        else if (Util.translateGroupToText(other.node.group) == GameEnum.Group.Shard) {
            switch (other.node.type) {
                case GameConst.Shards.type[0]:
                    if (!other.node.put) {
                        other.node.put = true;
                        NodePoolManager.putNodeElement(GameEnum.Group.ShardBullet, other.node);
                    }

                    if (this.node.player_id == Util.playerData.uid) cc.find("Canvas").getComponent("Game").incrLaser(1);

                    break;
                case GameConst.Shards.type[1]:
                    if (!other.node.put) {
                        other.node.put = true;
                        NodePoolManager.putNodeElement(GameEnum.Group.ShardSpeed, other.node);
                    }

                    if (this.node.player_id == Util.playerData.uid) cc.find("Canvas").getComponent("Game").incrSpeed(1);

                    break;
            }
        }
    },

    onCollisionExit() {
    },

    // lateUpdate: function(dt) {
    // },
});