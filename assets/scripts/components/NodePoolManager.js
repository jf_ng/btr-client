"use strict";

var GameEnum = require("../gameData/GameEnum");
var GameConst = require("../gameData/GameConst");
var Util = require("../Util");

let NodePoolManager = {
    _roomMax: 0,
    _nodePools: [],
    _nodePoolNames: ['TANK', 'SHARD_BULLET', 'SHARD_SPEED', 'BARRIERWOOD', 'BARRIERKING', 'BULLET1', 'BULLET2', 'BULLET3'],

    // pre instantiate prefabs
    preload: function() {
        var self = this;

        return new Promise(function(main_resolve, main_reject) {
            var arr = [];

            // tanks
            for (var i = 0; i <= self._roomMax; i++) {
                var p = new Promise(function(resolve, reject) {
                    cc.resources.load("prefab/Node AnyTank", function (err, prefab) {
                        self.putNodeElement(Util.translateTextToGroup(GameEnum.Group.Tank), cc.instantiate(prefab));
                        resolve();
                    });
                });

                arr.push(p);
            }

            // bullets
            for (var i = 0; i < GameConst.Preload.bullet; i++) {
                var p = new Promise(function(resolve, reject) {
                    cc.resources.load("prefab/Node Bullet 1", function (err, prefab) {
                        self.putNodeElement(GameEnum.Group.Bullet1, cc.instantiate(prefab));
                        resolve();
                    });
                });

                arr.push(p);

                var p = new Promise(function(resolve, reject) {
                    cc.resources.load("prefab/Node Bullet 2", function (err, prefab) {
                        self.putNodeElement(GameEnum.Group.Bullet2, cc.instantiate(prefab));
                        resolve();
                    });
                });

                arr.push(p);

                var p = new Promise(function(resolve, reject) {
                    cc.resources.load("prefab/Node Bullet 3", function (err, prefab) {
                        self.putNodeElement(GameEnum.Group.Bullet3, cc.instantiate(prefab));
                        resolve();
                    });
                });

                arr.push(p);
            }

            // shards
            for (var i = 0; i < GameConst.Preload.shard; i++) {
                var p = new Promise(function(resolve, reject) {
                    cc.resources.load("prefab/Node Bullet Shard", function (err, prefab) {
                        self.putNodeElement(GameEnum.Group.ShardBullet, cc.instantiate(prefab));
                        resolve();
                    });
                });

                arr.push(p);

                var p = new Promise(function(resolve, reject) {
                    cc.resources.load("prefab/Node Speed Shard", function (err, prefab) {
                        self.putNodeElement(GameEnum.Group.ShardSpeed, cc.instantiate(prefab));
                        resolve();
                    });
                });

                arr.push(p);
            }


            Promise.all(arr)
            .then(data => {
                main_resolve();
            });
        });
    },

    initNodePools: function(roomMax) {
        this._roomMax = roomMax;
        
        if (this._nodePools.length > 0) {
            for (let i = 0; i < this._nodePools.length; i++) {
                this._nodePools[i].clear();
            }
        }

        this._nodePools = [];

        for (let i = 0; i < this._nodePoolNames.length; i++) {
            this.createNodePool(this._nodePoolNames[i]);
        }
    },

    createNodePool: function(name) {
        if (!this.getNodePool(name) && !this.getNodeElement(name)) {
            let nodePool = new cc.NodePool(name);
            this._nodePools.push(nodePool);
            return nodePool;
        } else {
            return null;
        }
    },

    getNodePool: function(name) {
        if (this._nodePools.length > 0) {
            for (let i = 0; i < this._nodePools.length; i++) {
                if (this._nodePools[i].poolHandlerComp === name) {
                    return this._nodePools[i];
                }
            }
            return null;
        }
    },

    getNodeElement(name) {
        let nodePool = this.getNodePool(name);
        if (nodePool) {
            let nodeElement = nodePool.get();
            return nodeElement;
        } else {
            return null;
        }
    },

    putNodeElement(name, element) {
        let nodePool = this.getNodePool(name);
        if (nodePool) {
            nodePool.put(element);
        }
    },

    clear() {
        for (let i = 0; i < this._nodePools.length; i++) {
            this._nodePools[i].clear();
        }
    },
};

module.exports = NodePoolManager;