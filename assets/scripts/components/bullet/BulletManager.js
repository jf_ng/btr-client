"use strict";

var NodePoolManager = require("../NodePoolManager");
var GameEnum = require("../../gameData/GameEnum");
var GameConst = require("../../gameData/GameConst");
var Util = require("../../Util");

cc.Class({
    extends: cc.Component,

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        // this.launchStep = GameConst.BulletLaunch.speed;
    },

    start() {
    },

    onCollisionEnter(other, self) {
        if (Util.translateGroupToText(other.node.group) == GameEnum.Group.Barrier) {
            // grass not added as barrier tile
            // if (other.node.type == GameEnum.TileType.tileGrass) {
            //     return;
            // }

            if (self.node.type == GameEnum.BulletRange.normal) self.node._tankManager.putBullet();
            else if (self.node.type == GameEnum.BulletRange.laser) self.node._tankManager.putLaser();

            if (other.node.type == GameEnum.TileType.tileWood) {
                cc.find('Canvas').getComponent("TiledMapManager").removeWood(other.node);
            }
            else if (other.node.type == GameEnum.TileType.tileKing) {
                if (self.node._player_id == Util.playerData.uid) cc.find('Canvas').getComponent("Game").hitKing();
            }
        }
        else if (Util.translateGroupToText(other.node.group) == GameEnum.Group.Bullet) {
            if (self.node.type == GameEnum.BulletRange.normal) self.node._tankManager.putBullet();
        }
    },

    onCollisionExit() {
    },

    // update (dt) {},
});