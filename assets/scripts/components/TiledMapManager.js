"use strict";

var NodePoolManager = require("./NodePoolManager");
var GameEnum = require("../gameData/GameEnum");
var GameConst = require("../gameData/GameConst");
var Util = require("../Util");

var pf = require('easystarjs');

cc.Class({
    extends: cc.Component,

    properties: {
        mainTiledMap: {
            default: null,
            type: cc.TiledMap
        },

        tiledMapAssetSet: {
            default: [],
            type: cc.TiledMapAsset
        },

        tiledMapAssets: {
            default: [],
            type: cc.TiledMapAsset
        },

        tileBarrier: {
            default: null,
            type: cc.Prefab
        },
        tileShardBullet: {
            default: null,
            type: cc.Prefab
        },
        tileShardSpeed: {
            default: null,
            type: cc.Prefab
        },
        tileWood: {
            default: null,
            type: cc.Prefab
        },
        tileKing: {
            default: null,
            type: cc.Prefab
        },

        // tiles not any form of barriers
        _emptyTiles: [],
        _barrierTiles: null,
        _barrierWoods: [],

        _activeShards: [],

        // _pathFinder: null,
        _pathGrid: null,
    },

    onLoad() {
        this._emptyTiles = [];
        this._activeShards = [];
        this._barrierTiles = {};
        this._barrierWoods = [];

        this.initTiledMapData();
    },

    initTiledMapData() {
        this.backgroundLayer = this.mainTiledMap.getLayer("background");
        this.barrierLayer = this.mainTiledMap.getLayer("barrier");
        this.shardLayer = this.mainTiledMap.getObjectGroup("shard");

        this.tileSize = this.mainTiledMap.getTileSize();

        this.mapResWidth = this.backgroundLayer.getLayerSize().width * this.tileSize.width;
        this.mapResHeight = this.backgroundLayer.getLayerSize().height * this.tileSize.height;


        this.initBarriers();


        this.backgroundLayer.enableCulling(true);
        this.barrierLayer.enableCulling(true);
        this.mainTiledMap.getLayer("grass").enableCulling(true);
    },

    initBarriers() {
        var tileGID, pos;
        var gridCol = [];

        for (var i = 0.5; i <= this.backgroundLayer.getLayerSize().width - 0.5; i += 1) {
            gridCol[i - 0.5] = [];

            for (var j = 0.5; j <= this.backgroundLayer.getLayerSize().height - 0.5; j += 1) {
                tileGID = this.barrierLayer.getTileGIDAt(i, j);
                pos = this.getTilePositionAt(i, j);

                if (GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileSteel) {
                    // var barrier = NodePoolManager.getNodeElement(GameEnum.Group.BarrierSteel);
                    // if (!barrier) {
                        barrier = cc.instantiate(this.tileBarrier);
                    // }
                    
                    barrier.group = Util.translateTextToGroup(GameEnum.Group.Barrier);
                    barrier.type = GameEnum.TileType.tileSteel;
                    barrier.position = pos;

                    /*
                    * do not add into tiledlayer. draw calls will increase
                    * add into tiledobjectgroup
                    */
                    this.node.getComponent("Game").tankWarMap.node.getChildByName("shard").addChild(barrier);

                    this._barrierTiles[pos.x + ',' + pos.y] = {
                        type: GameEnum.TileType.tileSteel,
                        tileX: i,
                        tileY: j,
                        mapX: pos.x,
                        mapY: pos.y
                    };

                    gridCol[i - 0.5].push(1);
                }
                else if (GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileBarrier1) {
                    // var barrier = NodePoolManager.getNodeElement(GameEnum.Group.Barrier1);
                    // if (!barrier) {
                        barrier = cc.instantiate(this.tileBarrier);
                    // }
                    
                    barrier.group = Util.translateTextToGroup(GameEnum.Group.Barrier);
                    barrier.type = GameEnum.TileType.tileBarrier1;
                    barrier.position = pos;

                    this.node.getComponent("Game").tankWarMap.node.getChildByName("shard").addChild(barrier);

                    this._barrierTiles[pos.x + ',' + pos.y] = {
                        type: GameEnum.TileType.tileBarrier1,
                        tileX: i,
                        tileY: j,
                        mapX: pos.x,
                        mapY: pos.y
                    };

                    gridCol[i - 0.5].push(1);
                }
                else if (GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileBarrier2) {
                    // var barrier = NodePoolManager.getNodeElement(GameEnum.Group.Barrier2);
                    // if (!barrier) {
                        barrier = cc.instantiate(this.tileBarrier);
                    // }
                    
                    barrier.group = Util.translateTextToGroup(GameEnum.Group.Barrier);
                    barrier.type = GameEnum.TileType.tileBarrier2;
                    barrier.position = pos;

                    this.node.getComponent("Game").tankWarMap.node.getChildByName("shard").addChild(barrier);

                    this._barrierTiles[pos.x + ',' + pos.y] = {
                        type: GameEnum.TileType.tileBarrier2,
                        tileX: i,
                        tileY: j,
                        mapX: pos.x,
                        mapY: pos.y
                    };

                    gridCol[i - 0.5].push(1);
                }
                else if (GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileWood) {
                    // var barrier = NodePoolManager.getNodeElement(Util.translateTextToGroup(GameEnum.Group.Barrier));
                    var barrier = NodePoolManager.getNodeElement(GameEnum.Group.BarrierWood);
                    if (!barrier) {
                        barrier = cc.instantiate(this.tileBarrier);
                    }
                    
                    barrier.group = Util.translateTextToGroup(GameEnum.Group.Barrier);
                    barrier.type = GameEnum.TileType.tileWood;
                    barrier.position = pos;

                    this.node.getComponent("Game").tankWarMap.node.getChildByName("shard").addChild(barrier);

                    this._barrierTiles[pos.x + ',' + pos.y] = {
                        type: GameEnum.TileType.tileWood,
                        tileX: i,
                        tileY: j,
                        mapX: pos.x,
                        mapY: pos.y
                    };
                }
                else if (GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileGrass) {
                    // var barrier = NodePoolManager.getNodeElement(GameEnum.Group.BarrierGrass);
                    // if (!barrier) {
                    //     barrier = cc.instantiate(this.tileBarrier);
                    // }
                    
                    // barrier.group = Util.translateTextToGroup(GameEnum.Group.Barrier);
                    // barrier.type = GameEnum.TileType.tileGrass;
                    // barrier.position = pos;

                    // cc.find("Canvas").getComponent("Game").tankWarMap.node.getChildByName("shard").addChild(barrier);
                }
                else {
                    // remove king and surrounding woods
                    if
                    (
                        (i == 1.5 && j == this.backgroundLayer.getLayerSize().height / 2) ||
                        (i == 1.5 && j == this.backgroundLayer.getLayerSize().height / 2 - 1) ||
                        (i == 1.5 && j == this.backgroundLayer.getLayerSize().height / 2 + 1) ||
                        (i == 2.5 && j == this.backgroundLayer.getLayerSize().height / 2) ||
                        (i == this.backgroundLayer.getLayerSize().width - 1.5 && j == this.backgroundLayer.getLayerSize().height / 2) ||
                        (i == this.backgroundLayer.getLayerSize().width - 1.5 && j == this.backgroundLayer.getLayerSize().height / 2 - 1) ||
                        (i == this.backgroundLayer.getLayerSize().width - 1.5 && j == this.backgroundLayer.getLayerSize().height / 2 + 1) ||
                        (i == this.backgroundLayer.getLayerSize().width - 2.5 && j == this.backgroundLayer.getLayerSize().height / 2)
                    )
                    {

                    }
                    else {
                        this._emptyTiles.push({
                            tileX: i,
                            tileY: j,
                            mapX: pos.x,
                            mapY: pos.y
                        });
                    }

                    gridCol[i - 0.5].push(0);
                }
            }
        }

        this._pathGrid = [];
        for (var i = 0; i < this.backgroundLayer.getLayerSize().height; i++) {
            this._pathGrid[i] = [];

            for (var j = 0; j < this.backgroundLayer.getLayerSize().width; j++) {
                this._pathGrid[i].push(gridCol[j][i]);
            }
        }
    },

    createKing(force) {
        var pos, x, y, woods = {};
        this._barrierWoods = {};

        switch (force) {
            case GameEnum.Force.Blue:
                x = 1.5;
                y = this.backgroundLayer.getLayerSize().height / 2;

                // add surrounding woods
                woods.left = [x, y - 1];
                woods.right = [x, y + 1];
                woods.front = [x + 1, y];

                break;
            case GameEnum.Force.Red:
                x = this.backgroundLayer.getLayerSize().width - 1.5;
                y = this.backgroundLayer.getLayerSize().height / 2;

                // add surrounding woods
                woods.right = [x, y - 1];
                woods.left = [x, y + 1];
                woods.front = [x - 1, y];

                break;
        }

        var king = NodePoolManager.getNodeElement(GameEnum.Group.BarrierKing);
        if (!king) {
            king = cc.instantiate(this.tileKing);
        }
        
        pos = this.getTilePositionAt(x, y);

        king.group = Util.translateTextToGroup(GameEnum.Group.Barrier);
        king.type = GameEnum.TileType.tileKing;
        king.force = force;
        king.setPosition(pos);

        this.node.getComponent("Game").tankWarMap.node.getChildByName("shard").addChild(king);

        this._barrierTiles[pos.x + ',' + pos.y] = {
            type: GameEnum.TileType.tileKing,
            tileX: x,
            tileY: y,
            mapX: pos.x,
            mapY: pos.y
        };


        var keys = Object.keys(woods);
        for (var i = 0; i < keys.length; i++) {
            var barrier = NodePoolManager.getNodeElement(GameEnum.Group.BarrierWood);
            if (!barrier) {
                barrier = cc.instantiate(this.tileWood);
            }
            
            pos = this.getTilePositionAt(woods[keys[i]][0], woods[keys[i]][1]);

            barrier.group = Util.translateTextToGroup(GameEnum.Group.Barrier);
            barrier.type = GameEnum.TileType.tileWood;
            barrier.setPosition(pos);
            barrier.direction = keys[i];

            this.node.getComponent("Game").tankWarMap.node.getChildByName("shard").addChild(barrier);
            this._barrierWoods[keys[i]] = barrier;

            this._barrierTiles[pos.x + ',' + pos.y] = {
                type: GameEnum.TileType.tileKing,
                tileX: woods[keys[i]][0],
                tileY: woods[keys[i]][1],
                mapX: pos.x,
                mapY: pos.y
            };
        }

        return king;
    },

    removeWood(node) {
        this.node.getComponent("Game").hitKingWood(node.direction);

        NodePoolManager.putNodeElement(GameEnum.Group.BarrierWood, node);

        delete this._barrierTiles[node.x + ',' + node.y];
        delete this._barrierWoods[node.direction];
    },

    removeWoodByPosition(position) {
        if (this._barrierWoods.hasOwnProperty(position)) {
            NodePoolManager.putNodeElement(GameEnum.Group.BarrierWood, this._barrierWoods[position]);

            delete this._barrierTiles[this._barrierWoods[position].x + ',' + this._barrierWoods[position].y];
            delete this._barrierWoods[position];
        }
    },

    removeKing(node) {
        NodePoolManager.putNodeElement(GameEnum.Group.BarrierKing, node);
        delete this._barrierTiles[node.x + ',' + node.y];

        var keys = Object.keys(this._barrierWoods);
        for (var i = 0; i < keys.length; i++) {
            this.removeWood(this._barrierWoods[keys[i]]);
        }
    },

    getEmptyTiles() {
        return this._emptyTiles;
    },

    getBarrierTileAtPos(x, y) {
        return this._barrierTiles[x + ',' + y];
    },

    /**************************************************
    *
    * drop shards within player screen area
    *
    **************************************************/
    dropShards(position) {
        var current = this.getLayerPositionAt(position.x, position.y);

        var limitWidth = Math.ceil(cc.winSize.width / 2 / this.tileSize.width);
        var limitHeight = Math.ceil(cc.winSize.height / 2 / this.tileSize.height);

        var tileGID, arr = [];
        for (var i = 1; i < limitWidth; i++) {
            for (var j = 1; j < limitHeight; j++) {
                // top left
                try {
                    tileGID = this.barrierLayer.getTileGIDAt(current.x - i, current.y - j);
                    if (GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileNone || GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileGrass) {
                        arr.push(this.getTilePositionAt(current.x - i, current.y - j));
                    }
                } catch (err) {}

                // top right
                try {
                    tileGID = this.barrierLayer.getTileGIDAt(current.x + i, current.y - j);
                    if (GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileNone || GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileGrass) {
                        arr.push(this.getTilePositionAt(current.x + i, current.y - j));
                    }
                } catch (err) {}

                // btm left
                try {
                    tileGID = this.barrierLayer.getTileGIDAt(current.x - i, current.y + j);
                    if (GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileNone || GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileGrass) {
                        arr.push(this.getTilePositionAt(current.x - i, current.y + j));
                    }
                } catch (err) {}

                // btm right
                try { 
                    tileGID = this.barrierLayer.getTileGIDAt(current.x + i, current.y + j);
                    if (GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileNone || GameConst.GidToTileType[tileGID] === GameEnum.TileType.tileGrass) {
                        arr.push(this.getTilePositionAt(current.x + i, current.y + j));
                    }
                } catch (err) {}
            }
        }

        var count = Util.chance.integer({ min: 2, max: 5 });
        var shard, type, pos;

        for (var i = 0; i < count; i++) {
            type = Util.chance.weighted(GameConst.Shards.type, GameConst.Shards.rate);
            switch (type) {
                case GameConst.Shards.type[0]:
                    shard = NodePoolManager.getNodeElement(GameEnum.Group.ShardBullet);
                    if (!shard) {
                        shard = cc.instantiate(this.tileShardBullet);
                    }
                    
                    shard.group = Util.translateTextToGroup(GameEnum.Group.ShardBullet);

                    break;
                case GameConst.Shards.type[1]:
                    shard = NodePoolManager.getNodeElement(GameEnum.Group.ShardSpeed);
                    if (!shard) {
                        shard = cc.instantiate(this.tileShardSpeed);
                    }

                    shard.group = Util.translateTextToGroup(GameEnum.Group.ShardBullet);

                    break;
            }

            shard.type = type;
            shard.put = false;

            pos = Util.chance.pickset(arr)[0];
            shard.position = pos;

            this._activeShards.push(shard);

            this.node.getComponent("Game").tankWarMap.node.getChildByName("shard").addChild(shard);
        }

        arr = null;
    },

    clearShards() {
        var count = this._activeShards.length;
        for (var i = 0; i < count; i++) {
            switch (this._activeShards[0].type) {
                case GameConst.Shards.type[0]:
                    if (!this._activeShards[0].put) {
                        this._activeShards[0].put = true;
                        NodePoolManager.putNodeElement(GameEnum.Group.ShardBullet, this._activeShards[0]);
                    }

                    break;
                case GameConst.Shards.type[1]:
                    if (!this._activeShards[0].put) {
                        this._activeShards[0].put = true;
                        NodePoolManager.putNodeElement(GameEnum.Group.ShardSpeed, this._activeShards[0]);
                    }

                    break;
            }

            this._activeShards.splice(0, 1);
        }
    },

    // world position to layer
    getLayerPositionAt(posX, posY) {
        var x = 0, y = 0;

        if (posX > 0) {
            x = ((this.mapResWidth / 2) + posX) / this.tileSize.width;
        }
        else if (posX < 0) {
            x = (this.barrierLayer.getLayerSize().width / 2) - (-posX / this.tileSize.width);
        }
        else {
            x = this.barrierLayer.getLayerSize().width / 2;
        }

        y = ((this.mapResHeight / 2) - posY) / this.tileSize.height;
        
        return cc.v2(x, y);
    },

    // layer position to world
    getTilePositionAt(posX, posY) {
        var x = 0, y = 0;

        if (posX > this.backgroundLayer.getLayerSize().width / 2) {
            x = (posX - this.backgroundLayer.getLayerSize().width / 2) * this.tileSize.width;
        }
        else if (posX < this.backgroundLayer.getLayerSize().width / 2) {
            x = (posX - this.backgroundLayer.getLayerSize().width / 2) * this.tileSize.width;
        }

        if (posY > this.backgroundLayer.getLayerSize().height / 2) {
            y = (posY - this.backgroundLayer.getLayerSize().height / 2) * -this.tileSize.height;
        }
        else if (posY < this.backgroundLayer.getLayerSize().height / 2) {
            y = (this.backgroundLayer.getLayerSize().height / 2 - posY) * this.tileSize.height;
        }

        return cc.v2(x, y);
    },

    getPathFinder() {
        var pathFinder = new pf.js();
        pathFinder.setGrid(this._pathGrid);
        pathFinder.setAcceptableTiles([0]);
        pathFinder.setIterationsPerCalculation(1000);
        pathFinder.disableDiagonals();
        pathFinder.disableCornerCutting();
        // pathFinder.enableSync();

        return pathFinder;
    },

    // update (dt) {},
});