"use strict";

var GameEnum = require("../gameData/GameEnum");
var GameConst = require("../gameData/GameConst");
var Util = require("../Util");

cc.Class({
    extends: cc.Component,

    properties: {
        tanksParentNode: {
            default: null,
            type: cc.Node
        },

        playerTank: {
            default: null,
            type: cc.Node
        },

        _pause: false,

        _shardScheduleCount: 0,
        
        _playerUpdate: null, // player update data to be emitted

        _delAIBlue: 0, // count of AI to be deleted
        _delAIRed: 0, // count of AI to be deleted


        _processPlayerInput: true,

        _playerTankPendingDirection: null,
        _playerTankNewDirection: null,
        _playerTankBullet: null,
        _playerTankSpeed: null,

        _redTanks: null,
        _blueTanks: null,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._processPlayerInput = true;

        this._redTanks = [];
        this._blueTanks = [];
    },

    start () {
        // +2 tiles size because can be moving towards each other
        // Util.playerWidthRange = cc.winSize.width + 2 * cc.find("Canvas").getComponent("TiledMapManager").tileSize.width;
        // Util.playerHeightRange = cc.winSize.height + 2 * cc.find("Canvas").getComponent("TiledMapManager").tileSize.height;
        Util.playerWidthRange = cc.winSize.width;
        Util.playerHeightRange = cc.winSize.height;
    },

    begin() {
        this._shardScheduleCount = 0;

        this._playerTankPendingDirection = null;
        this._playerTankBullet = null;
        this._playerTankSpeed = false;
        this._playerUpdate = {
            player_id: Util.playerData.uid,
            player_name: Util.playerData.name,
            player_photo: Util.playerData.photo,
            player_tank: Util.playerData.tank
        };

        this.schedule(this.scheduler, 2);
    },

    showNames() {
        for (var i = 0; i < this.tanksParentNode.children.length; i++) {
            this.tanksParentNode.children[i].getComponent('TankManager').showName();
        }
    },

    pause(bool) {
        this._pause = bool;

        if (bool) {
            this._shardScheduleCount = 0;
        }

        for (var i = 0; i < this.tanksParentNode.children.length; i++) {
            this.tanksParentNode.children[i].getComponent('TankManager').pause(bool);
        }
    },

    playerRespawn(position) {
        this.emitRespawn(position);
        this.node.getComponent('Game')._playerTank.getComponent("TankManager").playerRespawn(position);
    },

    deleteAITank(force) {
        (force == 'Blue') ? this._delAIBlue++ : this._delAIRed++;
    },

    launchBullet(range) {
        // if (this.node.getComponent('Game')._playerTank.active && this.node.getComponent('Game')._playerTank.getComponent("TankManager")._begin) {
        if (this.node.getComponent('Game')._playerTank.active) {
            this._playerTankBullet = range;

            this.processInput();

            return true;
        }

        return false;
    },

    speed() {
        // if (this.node.getComponent('Game')._playerTank.active && this.node.getComponent('Game')._playerTank.getComponent("TankManager")._begin) {
        if (this.node.getComponent('Game')._playerTank.active) {
            this._playerTankSpeed = true;

            this.processInput();

            return true;
        }

        return false;
    },

    changeDirection(direction) {
        // if (this.node.getComponent('Game')._playerTank.active && this.node.getComponent('Game')._playerTank.getComponent("TankManager")._begin) {
        if (this.node.getComponent('Game')._playerTank.active) {
            this._playerTankNewDirection = direction;

            this.node.getComponent('Game')._playerTank.getComponent("TankManager").showNewDirection(direction);

            this.processInput();

            return true;
        }

        return false;
    },

    toggleProcessPlayerInput(bool) {
        this._processPlayerInput = bool;
    },

    processInput() {
        if (!this._processPlayerInput) return;
        this._processPlayerInput = false;

        this._playerTankPendingDirection = this._playerTankNewDirection;

        this.emitPlayerUpdate();

        this.node.getComponent('Game')._playerTank.getComponent("TankManager").inputCallback(this._playerTankPendingDirection, this._playerTankSpeed, this._playerTankBullet);

        this._playerTankNewDirection = null;
        this._playerTankPendingDirection = null;
        this._playerTankBullet = null;
        this._playerTankSpeed = false;
    },

    emitPlayerUpdate() {
        // if (!this._playerTankBullet && !this._playerTankSpeed && !this._playerTankPendingDirection) return;

        this._playerUpdate.time = Date.now();
        this._playerUpdate.player_position = this.node.getComponent('Game')._playerTank.getComponent('TankManager')._tankStartPosition;
        this._playerUpdate.direction = (this._playerTankPendingDirection) ? this._playerTankPendingDirection : this.node.getComponent('Game')._playerTank.getComponent('TankManager').tankDirection;
        this._playerUpdate.bullet = this._playerTankBullet;
        this._playerUpdate.speed = this._playerTankSpeed;
        this._playerUpdate.type = 'update';


        this.node.getComponent('Game').emit('update', this._playerUpdate);
    },

    emitBoom() {
        this._playerUpdate.time = Date.now();
        this._playerUpdate.player_position = this.node.getComponent('Game')._playerTank.position;
        this._playerUpdate.direction = this.node.getComponent('Game')._playerTank.getComponent('TankManager').tankDirection;
        this._playerUpdate.type = 'boom';


        this.node.getComponent('Game').emit('update', this._playerUpdate);
    },

    emitRespawn(position) {
        this._playerUpdate.time = Date.now();
        this._playerUpdate.player_position = position;
        this._playerUpdate.direction = this.node.getComponent('Game')._playerTank.getComponent('TankManager').tankDirection;
        this._playerUpdate.type = 'respawn';
        this._playerUpdate.player_force = (this.node.getComponent('Game')._playerTank.getComponent('TankManager').force == GameEnum.Force.Blue) ? 'Blue' : 'Red';

        this.node.getComponent('Game').emit('update', this._playerUpdate);
    },

    emitCollide() {
        this._playerUpdate.time = Date.now();
        this._playerUpdate.player_position = this.node.getComponent('Game')._playerTank.position;
        this._playerUpdate.direction = this.node.getComponent('Game')._playerTank.getComponent('TankManager').tankDirection;
        this._playerUpdate.type = 'collide';


        this.node.getComponent('Game').emit('update', this._playerUpdate);
    },

    emitHitKing() {
        this.node.getComponent('Game').emit('king_hit', {});
    },

    emitHitKingWood(position) {
        this.node.getComponent('Game').emit('king_wood_hit', {position: position});
    },

    /**************************************************
    * process server updates
    * do not need to hide because scheduler will do that

    {
      "player_id": 75688390,
      "player_name": "p_75688390",
      "player_photo": null,
      "player_tank": "3-1",
      "time": 1595253953747,
      "player_position": {
        "x": 360,
        "y": 432,
        "z": 0
      },
      "direction": "up",
      "bullet": null,
      "speed": false,
      "moving": true
    }

    **************************************************/
    processUpdates(updates) {
        // console.log(updates);

        if (updates.length <= 0 || !this.node.getComponent('Game')._playerTank.active) return;

        for (var i = 0; i < updates.length; i++) {
            try {
                var json = JSON.parse(updates[i]);

                if (json.player_id == Util.playerData.uid) continue;

                var tank = this.tanksParentNode.getChildByName(String(json.player_id));

                // create tank
                if (!tank && json.type != 'boom') {
                    tank = this.node.getComponent('Game').createEnemyTank(json);
                    // if (tank) tank.getComponent('TankManager').begin(true);

                    continue;
                }

                if (tank.update_time >= json.time) continue;


                if (json.type == 'boom') {
                    tank.getComponent('TankManager').boom();
                }
                else if (json.type == 'collide') {
                    tank.getComponent('TankManager').stopMove();

                    // set time
                    tank.update_time = json.time;

                    // set starting position
                    tank.getComponent('TankManager')._tankStartPosition = json.player_position;
                    tank.setPosition(json.player_position);

                    tank.getComponent('TankManager').tankDirection = json.direction;
                }
                else if (json.type == 'update') {
                    if (Util.withinPlayerRange(json.player_position)) {
                        if (!tank.getComponent('TankManager')._dead) {
                            // set time
                            tank.update_time = json.time;

                            // set starting position
                            tank.getComponent('TankManager')._tankStartPosition = json.player_position;


                            if (!tank.active || json.speed || json.bullet || (tank.position.x == GameConst.TankStartingPosition.x && tank.position.y == GameConst.TankStartingPosition.y)) tank.setPosition(json.player_position);
                            if (!tank.active || json.speed || json.bullet) tank.getComponent('TankManager').tankDirection = json.direction;


                            tank.getComponent('TankManager').hide(false);

                            tank.getComponent("TankManager").inputCallback(json.direction, json.speed, json.bullet);
                        }
                    }
                    else {
                        // update direction and position first
                        if (!tank.getComponent('TankManager')._dead) {
                            // set time
                            tank.update_time = json.time;

                            // set position
                            tank.setPosition(json.player_position);

                            // set direction
                            tank.getComponent('TankManager').tankDirection = json.direction;
                        }

                        // tank.getComponent('TankManager').hide(true); // leave it to scheduler
                    }
                }
            } catch (e) {
                if (updates[i].player_id == 'king') {
                    this.node.getComponent('Game').processKing(updates[i]);
                }
            }
        }
    },

    scheduler() {
        /*
        *
        * hide tanks if out of range, unhide if within range
        * move AI tanks
        *
        */ 
        for (var i = 0; i < this.tanksParentNode.children.length; i++) {
            if (this.tanksParentNode.children[i] == this.node.getComponent('Game')._playerTank) continue;

            if (this._delAIBlue > 0 && this.tanksParentNode.children[i].getComponent('TankManager').isAuto && this.tanksParentNode.children[i].getComponent('TankManager').force == GameEnum.Force.Blue) {
                this.tanksParentNode.children[i].getComponent('TankManager').deleteTank();

                i--;
                this._delAIBlue--;

                continue;
            }
            else if (this._delAIRed > 0 && this.tanksParentNode.children[i].getComponent('TankManager').isAuto && this.tanksParentNode.children[i].getComponent('TankManager').force == GameEnum.Force.Red) {
                this.tanksParentNode.children[i].getComponent('TankManager').deleteTank();

                i--;
                this._delAIRed--;

                continue;
            }

            if (!this.node.getComponent('Game')._playerTank.active) {
                this.tanksParentNode.children[i].getComponent('TankManager').hide(true);
            }
            else {
                if (!Util.withinPlayerRange(this.tanksParentNode.children[i].position)) {
                    if (!this.tanksParentNode.children[i].getComponent('TankManager').isAuto) {
                        this.tanksParentNode.children[i].getComponent('TankManager').hide(true);
                    }
                    else {
                        if (!this.tanksParentNode.children[i].getComponent('TankManager').dead) {
                            // move to king
                            if (cc.find('Canvas').getComponent('Game')._king) {
                                this.tanksParentNode.children[i].getComponent('TankManager').AIMoveCallback().then(move => {});
                            }
                            else {
                                this.tanksParentNode.children[i].getComponent('TankManager').hide(true);
                            }
                        }
                        else {
                            this.tanksParentNode.children[i].getComponent('TankManager').hide(true);
                        }
                    }
                }
                // else if (!this._pause && this.tanksParentNode.children[i].getComponent('TankManager')._begin) {
                else {
                    if (!this._pause) {
                        this.tanksParentNode.children[i].getComponent('TankManager').hide(false);
                    }
                    else {
                        this.tanksParentNode.children[i].getComponent('TankManager').hide(true);
                    }
                }
            }
        }


        // drop shards
        if (!this._pause && this.node.getComponent('Game')._playerTank.active && ++this._shardScheduleCount % GameConst.Shards.mod == 0) {
            // clear shards
            cc.find("Canvas").getComponent("TiledMapManager").clearShards();

            // drop shards
            if (this.node.getComponent('Game')._playerTank) cc.find("Canvas").getComponent("TiledMapManager").dropShards(this.node.getComponent('Game')._playerTank.getComponent('TankManager')._tankDestPosition);
        }
    },

    addTank(force, tank) {
        if (force == GameEnum.Force.Blue) {
            this._blueTanks.push(tank);
        }
        else {
            this._redTanks.push(tank);
        }
    },

    deleteTank(force, tank) {
        if (force == GameEnum.Force.Blue) {
            for (var i = 0; i < this._blueTanks.length; i++) {
                if (this._blueTanks[i].player_id == tank.player_id) {
                    this._blueTanks.splice(i, 1);
                    break;
                }
            }
        }
        else {
            for (var i = 0; i < this._redTanks.length; i++) {
                if (this._redTanks[i].player_id == tank.player_id) {
                    this._redTanks.splice(i, 1);
                    break;
                }
            }
        }
    },

    getForceTanks(force) {
        if (force == GameEnum.Force.Blue) {
            return this._blueTanks;
        }
        else {
            return this._redTanks;
        }
    },

    // update (dt) {},

    // lateUpdate: function(dt) {},
});
