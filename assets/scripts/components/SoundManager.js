"use strict";

var Util = require("../Util");

cc.Class({
    extends: cc.Component,
    editor: {
        menu: "CustomComponent/AudioControl",
    },
    properties: {
        loop: false,

        musicsVolume: {
            default: 0.5,
            range: [0, 1, 0.01],
            notify: function() {
                this.setMusicsVolume();
            }
        },

        effectsVolume: {
            default: 1,
            range: [0, 1, 0.01],
            notify: function() {
                this.setEffectsVolume();
            }
        },

        audioClipPool: {
            default: [],
            type: cc.AudioClip
        },

        bgmHome: {
            default: null,
            type: cc.AudioClip
        },

        bgmClipPool: {
            default: [],
            type: cc.AudioClip
        },

        _effectId: null,

        _bgmIndex: 0,
        _audioId: null,
    },

    start() {
        this.setMusicsVolume();
        this.setEffectsVolume();

        // duplicate each bgm 3 times for loop
        var arr = [];
        for (var i = 0; i < this.bgmClipPool.length; i++) {
            arr.push(this.bgmClipPool[i]);
            arr.push(this.bgmClipPool[i]);
            arr.push(this.bgmClipPool[i]);
        }

        this.bgmClipPool = arr;
    },

    playHome() {
        cc.audioEngine.stopMusic();
        if (Util.playerData.settings.bgm > 0) {
            this._audioId = cc.audioEngine.playMusic(this.bgmHome, true);
        }
    },

    playMusic() {
        cc.audioEngine.stopMusic();
        if (Util.playerData.settings.bgm > 0) {
            cc.find('Canvas').getComponent('SoundManager')._audioId = cc.audioEngine.playMusic(cc.find('Canvas').getComponent('SoundManager').bgmClipPool[cc.find('Canvas').getComponent('SoundManager')._bgmIndex], cc.find('Canvas').getComponent('SoundManager').loop);
            cc.audioEngine.setFinishCallback(cc.find('Canvas').getComponent('SoundManager')._audioId, cc.find('Canvas').getComponent('SoundManager').shuffleBGM);
        }
    },

    shuffleBGM() {
        cc.find('Canvas').getComponent('SoundManager')._bgmIndex++;
        if (cc.find('Canvas').getComponent('SoundManager')._bgmIndex == cc.find('Canvas').getComponent('SoundManager').bgmClipPool.length) cc.find('Canvas').getComponent('SoundManager')._bgmIndex = 0;
        
        cc.find('Canvas').getComponent('SoundManager').scheduleOnce(function() {
            cc.find('Canvas').getComponent('SoundManager').playMusic();
        });
    },

    playEffectSound(command, loop, callback) {
        if (Util.playerData.settings.sfx <= 0) {
            if (typeof callback === "function") {
                callback();
            }

            return false;
        }

        if (loop === null && loop === undefined) {
            var loop = this.loop;
        }
        if (command !== null && command !== undefined || this.audioClipPool.length > 0) {
            switch (command) {
                case "shoot":
                    this._effectId = cc.audioEngine.playEffect(this.audioClipPool[0], loop);
                    break;
                case "shootLaser":
                    this._effectId = cc.audioEngine.playEffect(this.audioClipPool[1], loop);
                    break;
                case "speed":
                    this._effectId = cc.audioEngine.playEffect(this.audioClipPool[2], loop);
                    break;
                case "health-up":
                    this._effectId = cc.audioEngine.playEffect(this.audioClipPool[3], loop);
                    break;
                case "laser-up":
                    this._effectId = cc.audioEngine.playEffect(this.audioClipPool[4], loop);
                    break;
                case "king":
                    this._effectId = cc.audioEngine.playEffect(this.audioClipPool[5], loop);
                    break;
                case "bonus":
                    this._effectId = cc.audioEngine.playEffect(this.audioClipPool[6], loop);
                    break;
                case "explode-tank":
                    this._effectId = cc.audioEngine.playEffect(this.audioClipPool[7], loop);
                    break;
                case "explode-king":
                    this._effectId = cc.audioEngine.playEffect(this.audioClipPool[8], loop);
                    break;
                case "timer":
                    this._effectId = cc.audioEngine.playEffect(this.audioClipPool[9], loop);
                    break;
                default:
            }
        }

        if (typeof callback === "function") {
            cc.audioEngine.setFinishCallback(this._effectId, callback);
        }

        return this._effectId;
    },

    stopEffectSound(id) {
        cc.audioEngine.stopEffect(id);
    },

    pauseMusic() {
        cc.audioEngine.pauseAll();
    },

    resumeMusic() {
        cc.audioEngine.resumeAll();
    },

    setMusicsVolume() {
        cc.audioEngine.setMusicVolume(this.musicsVolume);
    },

    setEffectsVolume() {
        cc.audioEngine.setEffectsVolume(this.effectsVolume);
    },

    stopAll() {
        cc.audioEngine.stopAll();
        this._audioId = null;
        this._effectId = null;
    },
});