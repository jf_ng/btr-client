"use strict";

var Util = require("./Util");

cc.Class({
    extends: cc.Component,

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
    },

    start () {
        this.populate();

        if (cc.sys.os == cc.sys.OS_IOS) {
            this.node.getChildByName('group').getChildByName('restore').active = true;
        }
    },

    populate() {
        // delete existing
        var parent = this.node.getChildByName('group').getChildByName('content');
        var count = parent.children.length;
        for (var j = 2; j < count; j++) {
            parent.children[2].removeFromParent(false);
            // parent.children[1].destroy();
        }


        if (Util.getConfig().fbinstant) {
            if (FBInstant.getSupportedAPIs().includes('payments.purchaseAsync')) {
            // if (true) {
                var self = this;

                FBInstant.payments.onReady(function () {
                    FBInstant.payments.getCatalogAsync().then(function (catalog) {
                        if (catalog.length > 0) {
                            for (var i = 0; i < catalog.length; i++) {
                                self.createPurchase(catalog[i].productID, catalog[i].title, catalog[i].description.split('*').filter(item => item), catalog[i].price, catalog[i].priceCurrencyCode);
                            }
                        }
                        else {
                            self.node.getChildByName('group').getChildByName('content').getChildByName('empty').active = true;
                        }
                    });
                });
            }
            else {
                this.node.getChildByName('group').getChildByName('content').getChildByName('empty').active = true;

                if (cc.sys.os == cc.sys.OS_IOS) {
                    Util.createAlert('In App Purchases are supported on web and Android 6 and above, not on iOS', function() {});
                }
            }
        }
        else {
            if (Util.getConfig().sdkbox) {
                var keys = Object.keys(Util.iap);
                if (keys.length <= 0) {
                    this.node.getChildByName('group').getChildByName('content').getChildByName('empty').active = true;
                }
                else {
                    for (var i = 0; i < keys.length; i++) {
                        var iap = JSON.parse(Util.iap[keys[i]]);

                        var id = iap.id;
                        if (cc.sys.os == cc.sys.OS_IOS) {
                            if (id == 1) id = 4;
                            else if (id == 2) id = 5;
                            else if (id == 2) id = 6;
                        }

                        this.createPurchase(id, iap.name, JSON.parse(iap.description), iap.price, 'USD');
                    }
                }
            }
            else if (Util.getConfig().sdkhub) {
                var self = this;

                sdkhub.getFeePlugin().callFuncWithParam("isEnvReady");
                Util.hms.iapCallback = function(products) {
                    // {"msg":"obtainProductInfo at 0","index":0,"productId":"2","priceType":0,"price":"SGD 4.40","getMicrosPrice":4400000,"originalLocalPrice":"SGD 4.40","originalMicroPrice":4400000,"currency":"SGD","productName":"Captain","productDesc":"No Ads\n999x Speed\n999x Laser","subSpecialPriceMicros":0,"subSpecialPeriodCycles":0,"subProductLevel":0}
                    for (var i = 0; i < products.length; i++) {
                        self.createPurchase(products[i].productId, products[i].productName, products[i].productDesc.split('\n'), products[i].price.replace(/[^0-9.]/g,''), products[i].currency);
                    }
                }

                var params = {
                    "productIdList": "1,2,3",
                    "priceType": 0
                };
                sdkhub.getFeePlugin().callFuncWithParam("obtainProductInfo", params);

                // var ownedPurchases = [], params = 0, productID, paymentID;
                // Util.hms.iapCallback = function(owned) {
                //     ownedPurchases = owned;

                //     // {"inAppPurchaseData":"{\"autoRenewing\":false,\"orderId\":\"20200903231332145bd2543c25.102822167\",\"packageName\":\"lol.verygames.btr.huawei\",\"applicationId\":102822167,\"kind\":0,\"productId\":\"1\",\"productName\":\"Specialist\",\"purchaseTime\":1599148076000,\"purchaseTimeMillis\":1599148076000,\"purchaseState\":0,\"developerPayload\":\"EXT\",\"purchaseToken\":\"0000017454a72c744f2df6618a10243418e3df46cd825b76a5e8c10a3969ef5a9c0f56ef4cffbd5ax5347.5.102822167\",\"consumptionState\":0,\"confirmed\":0,\"currency\":\"SGD\",\"price\":146,\"country\":\"SG\",\"payOrderId\":\"sandbox20200903114756072058329\",\"payType\":\"34\"}","purchaseState":0}
                //     for (var i = 0; i < ownedPurchases.length; i++) {
                //         var data = JSON.parse(ownedPurchases[i]['inAppPurchaseData']);

                //         //Please check if purchaseState == 0
                //         // -1: initialized, 0: purchased, 1: canceled, 2: refunded
                //         if (data['purchaseState'] != 0) continue;

                //         productID = data['productId'];
                //         paymentID = data['orderId'] + ',' + data['payOrderId'];

                //         sdkhub.getFeePlugin().callFuncWithParam("consumeOwnedPurchase", ownedPurchases[i]['inAppPurchaseData']);

                //         break; // shouldnt be more than 1
                //     }

                //     Util.hms.iapCallback = null;
                // }

                // Util.hms.iapConsumeCallback = function() {
                //     if (productID) {
                //         self.consumeProduct(productID, paymentID);
                //     }
                // }

                // sdkhub.getFeePlugin().callFuncWithParam("obtainOwnedPurchases", params);
            }
        }
    },

    createPurchase(id, name, description, price, currency) {
        var parent = this.node.getChildByName('group').getChildByName('content');

        var node = cc.instantiate(parent.children[1]);
        node.parent = parent;

        node.getChildByName('name').getComponent(cc.Label).string = name;

        node.getChildByName('button').getChildByName('Background').getChildByName('Label').getComponent(cc.Label).string = currency + ' ' + price;

        var desc = '';
        for (var j = 0; j < description.length; j++) {
            desc += '* ' + description[j] + '<br/>';
        }
        node.getChildByName('desc').getComponent(cc.RichText).string = desc;


        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
        clickEventHandler.component = "Iap";// This is the code file name
        clickEventHandler.handler = "makePurchase";

        if (Util.getConfig().sdkbox) clickEventHandler.customEventData = name.toLowerCase();
        else clickEventHandler.customEventData = id;

        var button = node.getChildByName('button').getComponent(cc.Button);
        button.clickEvents = [];
        button.clickEvents.push(clickEventHandler);


        node.active = true;
    },

    close(event, customEventData) {
        // Util.showLoading();
        Util.loadScene('home');
    },

    makePurchase(event, customEventData) {
        var self = this;


        if (Util.getConfig().fbinstant) {
            FBInstant.payments.purchaseAsync({
                productID: customEventData,
                developerPayload: '',
            }).then(function (purchase) {
                var params = 'token=' + Util.playerData.token;
                params += '&game_id=' + Util.playerData.game_id;
                params += '&source=' + 'fbinstant';
                params += '&iap=' + purchase.productID;
                params += '&transaction_id=' + purchase.paymentID;

                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("POST", Util.getConfig().server_url + '/makePurchase', true);
                xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4) {
                        if (xmlhttp.status === 200) {
                        } else {
                        }
                    }
                }
                xmlhttp.onerror = function () {};
                xmlhttp.send(params);


                FBInstant.payments.consumePurchaseAsync(purchase.purchaseToken).then(function () {
                    // Purchase successfully consumed!
                    // Game should now provision the product to the player
                    self.consumeProduct(purchase.productID, purchase.paymentID);
                }).catch(function(error) {
                    Util.error(error);
                });
            });
        }
        else if (Util.getConfig().sdkbox) {
            Util.showLoading();

            // {"transactionID":"GPA.3351-3930-6233-77186","currencyCode":"SGD","receiptCipheredPayload":"RAxjLF4f0Cs5I7ChEuHRpwEnQH3R4y396v1ezS1AJSKdv1dmLj4+EK5s4ykADNA3ezheLOZU6mM/WUmt4TRXFFPO4BaxUvbfkiD/7Ubi/fCoFH4bdrYb5/D557Gydy6czfZagaxxJJLwAu9PXJHL8H6Tq2CaTwNdy/NIodA7rbFw/i9iI/xkAqFBx6/ePwB0IBjmievRQM317cHggQljhHpBcwYviguXYhvJRI6TeBGkDnrezaZrRPSWKLAepVnAdcAGXqOiYSv8DvcYnEvLG6TXJkWy7iVUC5xOstJdfkUy/pa/MP8ICTR/2ZtTHqnj7R8omiIRfpKrJ7DEiGCgfQ==","title":"Booster (Battle Tanks Reloaded)","receipt":"{\"orderId\":\"GPA.3351-3930-6233-77186\",\"packageName\":\"lol.verygames.btr\",\"productId\":\"3\",\"purchaseTime\":1599573959385,\"purchaseState\":0,\"purchaseToken\":\"negleoegmopfohnjocjclegc.AO-J1OwDtpKJH4fuGkz2eBAlu4gL1_2OW94DD_LBagmWxrvciRsEg7mREb_owKveCi6BLLi7Jn106Mg4Qv5K2RLkL8aIaLZj-qCm_m-zzz69g38SMJ4YT90\",\"acknowledged\":false}","price":"$2.98","id":"3","priceValue":2.9800000190734863,"description":"999x Speed\n999x Laser","name":"booster"}
            Util.sdkbox.callback = function(success, product, msg) {
                if (success) {
                    self.consumeProduct(product.id, product.transactionID);
                }

                Util.removeLoading();

                Util.sdkbox.callback = null;
            }

            sdkbox.IAP.purchase(customEventData);
        }
        else if (Util.getConfig().sdkhub) {
            Util.showLoading();

            var params = {
                "Product_Id": customEventData,
            }
            sdkhub.getFeePlugin().feeForProduct(params);


            var ownedPurchases = [], productID, paymentID;

            Util.hms.iapCallback = function(owned) {
                ownedPurchases = owned;

                var consuming = false;

                // {"inAppPurchaseData":"{\"autoRenewing\":false,\"orderId\":\"20200903231332145bd2543c25.102822167\",\"packageName\":\"lol.verygames.btr.huawei\",\"applicationId\":102822167,\"kind\":0,\"productId\":\"1\",\"productName\":\"Specialist\",\"purchaseTime\":1599148076000,\"purchaseTimeMillis\":1599148076000,\"purchaseState\":0,\"developerPayload\":\"EXT\",\"purchaseToken\":\"0000017454a72c744f2df6618a10243418e3df46cd825b76a5e8c10a3969ef5a9c0f56ef4cffbd5ax5347.5.102822167\",\"consumptionState\":0,\"confirmed\":0,\"currency\":\"SGD\",\"price\":146,\"country\":\"SG\",\"payOrderId\":\"sandbox20200903114756072058329\",\"payType\":\"34\"}","purchaseState":0}
                for (var i = 0; i < ownedPurchases.length; i++) {
                    var data = JSON.parse(ownedPurchases[i]['inAppPurchaseData']);

                    //Please check if purchaseState == 0
                    // -1: initialized, 0: purchased, 1: canceled, 2: refunded
                    if (data['purchaseState'] != 0) continue;

                    productID = data['productId'];
                    paymentID = data['orderId'] + ',' + data['payOrderId'];

                    sdkhub.getFeePlugin().callFuncWithParam("consumeOwnedPurchase", ownedPurchases[i]['inAppPurchaseData']);

                    consuming = true;
                    break; // shouldnt be more than 1
                }

                if (!consuming) Util.removeLoading();

                Util.hms.iapCallback = null;
            }

            Util.hms.iapConsumeCallback = function() {
                if (productID) {
                    self.consumeProduct(productID, paymentID);
                }

                Util.removeLoading();
            }
        }
    },

    consumeProduct(productID, paymentID) {
        switch (productID) {
            case '1':
            case '4':
                Util.playerData.user_data.unlock.push('skin');
                Util.playerData.user_data.unlock.push('name');

                Util.savePlayerData();

                break;
            case '2':
            case '5':
                Util.playerData.user_data.unlock.push('ads');
                Util.playerData.inventory.laser += 999;
                Util.playerData.inventory.speed += 999;

                Util.savePlayerData();

                break;
            case '3':
            case '6':
                Util.playerData.inventory.laser += 999;
                Util.playerData.inventory.speed += 999;

                Util.savePlayerData();

                break;
        }


        var source;

        if (Util.getConfig().sdkhub) {
            source = 'hms';
            Util.ga.GameAnalytics.addBusinessEvent('', '', '', productID, 'iap-hms');
        }
        else if (Util.getConfig().fbinstant) {
            source = 'fbinstant';
            Util.ga.GameAnalytics.addBusinessEvent('', '', '', productID, 'iap-fbinstant');
        }
        else {
            source = cc.sys.os;
            Util.ga.GameAnalytics.addBusinessEvent('', '', '', productID, 'iap-' + cc.sys.os.toLowerCase());
        }


        var params = 'token=' + Util.playerData.token;
        params += '&game_id=' + Util.playerData.game_id;
        params += '&source=' + source;
        params += '&iap=' + productID;
        params += '&transaction_id=' + paymentID;

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", Util.getConfig().server_url + '/makePurchase', true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("Cache-Control", "no-cache");
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4) {
                if (xmlhttp.status === 200) {
                } else {
                }

                params = 'token=' + Util.playerData.token;
                params += '&game_id=' + Util.playerData.game_id;
                params += '&transaction_id=' + paymentID;

                xmlhttp = new XMLHttpRequest();
                xmlhttp.open("POST", Util.getConfig().server_url + '/consumePurchase', true);
                xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4) {
                        if (xmlhttp.status === 200) {
                        } else {
                        }
                    }

                    Util.createAlert('Thank you for your purchase!', function() {});
                }
                xmlhttp.onerror = function () {};
                xmlhttp.send(params);
            }
        }
        xmlhttp.onerror = function () {
            Util.createAlert('Thank you for your purchase!', function() {});
        };
        xmlhttp.send(params);


        if (Util.getConfig().sdkhub) Util.ga.GameAnalytics.addBusinessEvent('', '', '', productID, 'iap-hms');
        else if (Util.getConfig().fbinstant) Util.ga.GameAnalytics.addBusinessEvent('', '', '', productID, 'iap-fbinstant');
        else Util.ga.GameAnalytics.addBusinessEvent('', '', '', productID, 'iap-' + cc.sys.os);
    },

    restorePurchase(event, customEventData) {
        var self = this;
        Util.createChoiceAlert(
            'Do you want to restore your previous in-app purchases?',
            {
                text: 'Yes',
                cb: function() {
                    var params = 'token=' + Util.playerData.token;
                    params += '&game_id=' + Util.playerData.game_id;

                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.open("POST", Util.getConfig().server_url + '/restorePurchase', true);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState === 4) {
                            // if (xmlhttp.status === 200) {
                            // } else {
                            // }
                            Util.createAlert('Your previous in-app purchases have been restored (if any).', function() {
                                Util.loadScene('iap');
                            });
                        }
                    }
                    xmlhttp.onerror = function () {};
                    xmlhttp.send(params);
                }
            },
            {
                text: 'No',
                cb: function() {}
            }
        );
    },

    // update (dt) {},
});
