"use strict";

var GameEnum = require("./gameData/GameEnum");
var GameConst = require("./gameData/GameConst");
var NodePoolManager = require("./components/NodePoolManager");
var Util = require("./Util");

import ProgressBarEx from "./ProgressBarEx";
// import JoystickEnum from "./joystick/JoystickEnum";
// import JoystickEvent from "./joystick/JoystickEvent";

import io from 'socket.io-client';

cc.Class({
    extends: cc.Component,

    editor: {
        // executeInEditMode: true
    },

    properties: {
        playGame: {
            default: null,
            type: cc.Node
        },
        tankWarMap: {
            default: null,
            type: cc.TiledMap
        },
        tankSpriteAtlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        anyTank: {
            default: null,
            type: cc.Prefab
        },
        gameMenu: {
            default: null,
            type: cc.Node
        },

        speed: {
            default: 2,
            type: cc.Integer,
            notify: function() {
                this.updateSpeedUI();
            }
        },
        laser: {
            default: 2,
            type: cc.Integer,
            notify: function() {
                this.updateLaserUI();
            }
        },


        nodeActionFire: {
            default: null,
            type: cc.Node
        },
        nodeActionSpeed: {
            default: null,
            type: cc.Node
        },
        nodeActionLaser: {
            default: null,
            type: cc.Node
        },


        _playing: false,
        _playerTank: null,

        _king: {
            default: null,
            type: cc.Node
        },
        _kingHealth: {
            default: 0,
            type: cc.Integer,
            notify: function() {
                this.updateKingHealth();
            }
        },
        _kingSchedule: null,
        _kingTime: null,
        _kingTimerLabel: null,
        _kingDestroy: false,

        // for offline king
        _kingOfflineSchedule: null,
        _kingOfflineTick: 0,
        _kingCount: 0,


        _playerData: null,


        reconnectionAttempts: 5,
        reconnectionCounts: 0,

        _offlineMode: false,
        _connected: false,
        _socket: null,
        _room: null,
        _spawnPosition: null,
        _joinRoom: null,

        _players: null, // array of all real players
    },

    onLoad() {
        // cc.view.enableAntiAlias(false);
        cc.macro.ENABLE_WEBGL_ANTIALIAS = false;

        this._joinRoom = cc.sys.localStorage.getItem('join_room');
        cc.sys.localStorage.removeItem('join_room');

        this._connected = false;
        this._room = null;
        this._players = {};

        this.player_tank = null;

        this._playerData = {
            force: null,
            killTank: 0,
            killKing: 0,
        }


        var supported = false;
        if (Util.getConfig().fbinstant && FBInstant.getSupportedAPIs().includes('shareAsync')) supported = true;

        if (!Util.getConfig().fbinstant || !supported) {
            this.gameMenu.getChildByName('game over').getChildByName('group').getChildByName('layout').getChildByName('action').getChildByName('challenge').active = false;
        }
    },

    start() {
        var self = this;

        // warn low end device
        // ignore ios because all devices can upgrade to latest os
        var p1 = new Promise(function(resolve, reject) {
            if (cc.sys.isMobile && (cc.sys.os == cc.sys.OS_ANDROID && cc.sys.osMainVersion < 7)) {
                if (!Util.playerData.warn_old_device) {
                    Util.createAlert('You may not experience good gameplay on an old device', function() {
                        Util.playerData.warn_old_device = true;
                        Util.savePlayerData();

                        resolve();
                    });
                }
                else {
                    resolve();
                }
            }
            else resolve();
        });

        var p2 = new Promise(function(resolve, reject) {
            if (cc.sys.isMobile) {
                // JoystickEvent.getInstance().on(JoystickEnum.JoystickEventType.TOUCH_START, this.onTouchStart, this);
                // JoystickEvent.getInstance().on(JoystickEnum.JoystickEventType.TOUCH_MOVE, this.onTouchMove, this);
                // JoystickEvent.getInstance().on(JoystickEnum.JoystickEventType.TOUCH_END, this.onTouchEnd, this);
                resolve();
            }
            else {
                self.gameMenu.getChildByName('joystick').active = false;

                // show keys
                if (!Util.playerData.show_keyboard) {
                    Util.playerData.show_keyboard = true;
                    Util.savePlayerData();

                    self._showKeyboard = resolve;
                    self.keyboard(null, 'open');
                }
                else {
                    resolve();
                }
            }
        });

        Promise.all([p1, p2])
        .then(data => {
            self.showHUD(false);
        
            self.networkStatus(true, 'Setting up game. Please wait...');

            self.initConnection();
        });
    },

    initConnection() {
        this.reconnectionAttempts = 5;
        this.reconnectionCounts = 0;

        if (Util.getConfig().fbinstant) {
            this._socket = io(Util.getConfig().game_https_url + '/btr', {
              path: '/btr',
              transports: ['websocket'],
              reconnectionAttempts: this.reconnectionAttempts,
              timeout: 10000
            });
        }
        else {
            this._socket = io(Util.getConfig().game_http_url + '/btr', {
              path: '/btr',
              transports: ['websocket'],
              reconnectionAttempts: this.reconnectionAttempts,
              timeout: 10000
            });
        }

        /**************************************************
        **************************************************/

        this._socket.on('connect_error', (error) => {
            Util.log('connect_error');
            Util.log(error);

            /**************************************************
            * allow offline mode
            **************************************************/
            if (!this._connected) {
                // this.networkStatus(false);
                // this.showHUD(false);

                // Util.createAlert('No network detected. Please try again.', function() {
                //     Util.loadScene('game');
                // });
                if (!this._offlineMode) {
                    this._offlineMode = true;

                    if (this._socket) this._socket.disconnect();
                    this._socket = null;


                    var force = Util.chance.pickset(['Blue', 'Red'])[0];
                    this._playerData.force = force;

                    this._room = {
                        room_id: null,
                        room_max: GameConst.RoomMax,
                        blue: (force == 'Blue') ? 1 : 0,
                        red: (force == 'Red') ? 1 : 0,
                    }

                    this._spawnPosition = this.randomSpawnLocation();

                    // start input
                    this.pauseInput(false);

                    this.networkStatus(false);
                    this.showHUD(true);

                    this.initGame();


                    // schedule king
                    this._kingOfflineSchedule = function() {
                        this._kingOfflineTick++;

                        if (this._kingOfflineTick % (GameConst.KingInterval / 60000) == 0) {
                            this._kingCount++;
                            var king = (this._kingCount % 2 == 0) ? 'Blue' : 'Red';

                            this.processKing({
                                'force': king,
                                'duration': GameConst.KingDuration,
                                'status': 'appear'
                            });

                            this.scheduleOnce(function() {
                                this.processKing({
                                    'force': king,
                                    'duration': 0,
                                    'status': 'announce disappear'
                                });
                            }, GameConst.KingDuration / 1000);
                        }
                        else if (this._kingOfflineTick % (GameConst.KingInterval / 60000) == GameConst.KingTickAnnounce) {
                            this.processKing({
                                'force': (this._kingCount % 2 == 0) ? 'Red' : 'Blue',
                                'duration': 60000,
                                'status': 'announce appear'
                            });
                        }

                        
                    }
                    this.schedule(this._kingOfflineSchedule, 60);
                }
            }
            else if (++this.reconnectionCounts >= this.reconnectionAttempts) {
                this.networkStatus(false);

                Util.createAlert('You are disconnected from the game. A new game will be started.', function() {
                    Util.loadScene('game');
                });
            }
        });

        /**************************************************
        **************************************************/

        this._socket.on('connect_timeout', (timeout) => {
            Util.log('connect_timeout');
        });

        /**************************************************
        **************************************************/

        this._socket.on('error', (error) => {
            Util.log('connection error');
            Util.log(error);
        });

        /**************************************************
        **************************************************/

        this._socket.on('disconnect', (reason) => {
            if (this._joinRoom === false) return;

            // the disconnection was initiated by the server, you need to reconnect manually
            // else the socket will automatically try to reconnect
            Util.log('connection disconnect');
            Util.log(reason);

            // stop tanks
            this.node.getComponent("TankGroupManager").pause(true);

            // stop input
            this.pauseInput(true);

            this.networkStatus(true, 'Reconnecting...');
        });

        /**************************************************
        **************************************************/

        this._socket.on('connect', () => {
            Util.log('connection connect');
            this.reconnectionCounts = 0;

            // join room
            this._spawnPosition = this.randomSpawnLocation();
            this.emit('join-room', {
                room_id: (this._room && this._room.room_id) ? this._room.room_id : ( (this._joinRoom) ? this._joinRoom : null ),
                force: this._playerData.force,
                player_position: this._spawnPosition,
                player_id: Util.playerData.uid,
                player_name: Util.playerData.name,
                player_photo: Util.playerData.photo,
                player_tank: Util.playerData.tank
            });
        });

        /**************************************************
        **************************************************/

        this._socket.on('king', (data) => {
            this.processKing(data);
        });

        /**************************************************
        **************************************************/

        // receives players info after joining room
        this._socket.on('join-room', (data) => {
            // console.log(data);
            if (!data.success) {
                this.networkStatus(false);

                this._joinRoom = false;
                this._socket.disconnect();

                Util.createAlert('Sorry, failed to join room', function() {
                    // show interstitial
                    Util.showInterstitial().then(data => {
                        Util.loadScene('home');
                    });
                });
            }
            else {
                this._room = data.room;
                this._playerData.force = data.force;
                this._players = data.players;

                // start input
                this.pauseInput(false);

                this.networkStatus(false);
                this.showHUD(true);

                if (!this._connected) {
                    this._connected = true;

                    this.initGame();
                }
                else {
                    // add new player who joined during self dc
                    var keys = Object.keys(this._players);
                    for (var i = 0; i < keys.length; i++) {
                        if (keys[i] == Util.playerData.uid) continue;

                        var found = false;
                        for (let j = 0; j < this.tankWarMap.node.getChildByName("tank").childrenCount; j++) {
                            if (this.tankWarMap.node.getChildByName("tank").children[j].player_id == keys[i]) {
                                found = true;
                                break;
                            }
                        }

                        if (!found) {
                            var tank = this.createEnemyTank(this._players[keys[i]]);
                            if (tank) {
                                this.node.getComponent("TankGroupManager").deleteAITank(this._players[keys[i]].player_force);

                                // if (this._playerTank.active) tank.getComponent('TankManager').begin(true);
                            }
                        }
                    }
                }

                // start tanks
                this.node.getComponent("TankGroupManager").pause(false);
            }
        });

        /**************************************************
        **************************************************/

        // new player joined
        this._socket.on('player-join', (data) => {
            // console.log(data);

            if (data.player_id != Util.playerData.uid && !this.tankWarMap.node.getChildByName('tank').getChildByName(String(data.player_id))) {
                // add to _players
                this._players[data.player_id] = data;

                var tank = this.createEnemyTank(data);
                if (tank) {
                    this.node.getComponent("TankGroupManager").deleteAITank(data.player_force);

                    // if (this._playerTank.active) tank.getComponent('TankManager').begin(true);
                }
            }
        });

        /**************************************************
        **************************************************/

        this._socket.on('updates', (data) => {
            if (!this._playing) return;
            // console.log(data);
            
            this.node.getComponent("TankGroupManager").processUpdates(data);
        });

        /**************************************************
        **************************************************/

        // player left
        this._socket.on('player-left', (data) => {
            // console.log('player-left', data);
            var tank = this.tankWarMap.node.getChildByName('tank').getChildByName(String(data));
            if (tank) {
                tank.getComponent('TankManager').deleteTank();
                this.createAITank();
            }
        });
    },

    initGame() {
        // controls
        if (Util.playerData.control == 'left') {
            this.node.getChildByName('HUD').getChildByName('joystick').getComponent(cc.Widget).left = 0.08;
            this.node.getChildByName('HUD').getChildByName('joystick').getComponent(cc.Widget).isAlignLeft = true;
            this.node.getChildByName('HUD').getChildByName('joystick').getComponent(cc.Widget).isAlignRight = false;

            this.node.getChildByName('HUD').getChildByName('bottom bar').getComponent(cc.Widget).right = 0.1;
            this.node.getChildByName('HUD').getChildByName('bottom bar').getComponent(cc.Widget).isAlignLeft = false;
            this.node.getChildByName('HUD').getChildByName('bottom bar').getComponent(cc.Widget).isAlignRight = true;
        }
        else {
            this.node.getChildByName('HUD').getChildByName('joystick').getComponent(cc.Widget).right = cc.winSize.width * 0.08;
            this.node.getChildByName('HUD').getChildByName('joystick').getComponent(cc.Widget).isAlignLeft = false;
            this.node.getChildByName('HUD').getChildByName('joystick').getComponent(cc.Widget).isAlignRight = true;

            this.node.getChildByName('HUD').getChildByName('bottom bar').getComponent(cc.Widget).left = cc.winSize.width * 0.1;
            this.node.getChildByName('HUD').getChildByName('bottom bar').getComponent(cc.Widget).isAlignLeft = true;
            this.node.getChildByName('HUD').getChildByName('bottom bar').getComponent(cc.Widget).isAlignRight = false;
        }
        this.node.getChildByName('HUD').getChildByName('joystick').getComponent(cc.Widget).updateAlignment();
        this.node.getChildByName('HUD').getChildByName('bottom bar').getComponent(cc.Widget).updateAlignment();


        // this.health = 1;
        // this.healthShards = 0;
        // this.laserShards = 0;

        this.laser = Util.playerData.inventory.laser;
        this.incrLaser(0); // @todo

        this.speed = Util.playerData.inventory.speed;
        this.incrSpeed(0); // @todo

        
        this.initGameMenuInfo();

        NodePoolManager.initNodePools(this._room.room_max);

        // enable collision
        cc.director.getCollisionManager().enabled = true;

        // preload prefabs
        var self = this;
        NodePoolManager.preload().then(data => {
            self._playing = true;
            self.startGame();
        });
    },

    startGame() {
        this.createPlayerTank();

        
        var keys = Object.keys(this._players);
        for (var i = 0; i < keys.length; i++) {
            if (keys[i] == Util.playerData.uid) continue;

            this.createEnemyTank(this._players[keys[i]]);
        }


        var count = this._room.room_max - this.tankWarMap.node.getChildByName('tank').children.length;
        Util.log(count + ' AI');
        for (var i = 0; i < count; i++) {
            this.createAITank();
        }

        this.tankWarMap.getComponent('CameraFollow').target = this._playerTank;
        this.tankWarMap.getComponent('CameraFollow').follow();

        // for (var i = 0; i < this.tankWarMap.node.getChildByName('tank').children.length; i++) {
        //     if (this.tankWarMap.node.getChildByName('tank').children[i] == this._playerTank) continue;

        //     this.tankWarMap.node.getChildByName('tank').children[i].getComponent('TankManager').begin();
        // }

        this.node.getComponent("TankGroupManager").begin();

        this.node.getComponent("SoundManager").playMusic();

        this.initListenHandlers();


        this.node.getComponent("TankGroupManager").showNames();


        this._startTime = Date.now();
        Util.ga.GameAnalytics.addProgressionEvent(Util.ga.EGAProgressionStatus.Start, "game");
        cc.sys.localStorage.setItem('start_game', true);
    },

    clearTanks() {
        this._playerTank = null;
        for (let i = 0; i < this.tankWarMap.node.getChildByName("tank").childrenCount; i++) {
            this.tankWarMap.node.getChildByName("tank").children[i].getComponent('TankManager').deleteTank();
        }
    },

    createAITank() {
        // determine force
        var blue = 0, red = 0;
        for (let j = 0; j < this.tankWarMap.node.getChildByName("tank").childrenCount; j++) {
            (this.tankWarMap.node.getChildByName("tank").children[j].getComponent("TankManager").force == GameEnum.Force.Blue) ? blue++ : red++;
        }

        var force;
        if (blue > red) force = 'Red';
        else force = 'Blue';

        if (this._playing) {
            var tank = NodePoolManager.getNodeElement(Util.translateTextToGroup(GameEnum.Group.Tank));
            if (!tank) tank = cc.instantiate(this.anyTank);

            tank.getComponent("TankManager").initTank(
                Util.chance.guid(),
                Util.randomName(),
                force,
                null,
                GameConst.Direction[0],
                Util.translateTextToGroup(GameEnum.Group.Tank),
                true,
                this.randomSpawnLocation(),
                Util.chance.pickset(GameEnum.TankType)[0]
            );

            this.tankWarMap.node.getChildByName("tank").addChild(tank, 0, tank.player_id);
            // if (begin) tank.getComponent("TankManager").begin(true);
        }
    },

    scheduleAITank() {
        this.scheduleOnce(function() {
            this.createAITank();
        }, 5);
    },

    createEnemyTank(data) {
        // console.log('Added player', data);

        // check if tank exist
        var tank = this.tankWarMap.node.getChildByName('tank').getChildByName(String(data.player_id));
        if (tank) {
            return tank;
        }
        else {
            tank = NodePoolManager.getNodeElement(Util.translateTextToGroup(GameEnum.Group.Tank));
            if (!tank) {
                tank = cc.instantiate(this.anyTank);
            }


            // create label
            var label = cc.instantiate(this.node.getChildByName('HUD').getChildByName('label').children[0]);
            label.parent = this.node.getChildByName('HUD').getChildByName('label');
            label.getChildByName('name').getComponent(cc.Label).string = data.player_name;


            tank.getComponent("TankManager").initTank(
                // {player_id: 50364563, player_name: "p_50364563", player_photo: null, player_force: "Blue"}
                data.player_id,
                data.player_name,
                data.player_force,
                data.player_photo,
                data.direction,
                Util.translateTextToGroup(GameEnum.Group.Tank),
                false,
                (data.player_position) ? data.player_position : GameConst.TankStartingPosition, // top left
                data.player_tank,
                label
            );

            this.tankWarMap.node.getChildByName("tank").addChild(tank, 0, String(data.player_id));
            tank.active = false; // hide first

            return tank;
        }
    },

    createPlayerTank() {
        this._playerTank = NodePoolManager.getNodeElement(Util.translateTextToGroup(GameEnum.Group.Tank));
        if (!this._playerTank) {
            this._playerTank = cc.instantiate(this.anyTank);
        }
        
        this._playerTank.getComponent("TankManager").initTank(
            Util.playerData.uid,
            Util.playerData.name,
            this._playerData.force,
            Util.playerData.photo,
            GameConst.Direction[0],
            Util.translateTextToGroup(GameEnum.Group.Tank),
            false,
            this._spawnPosition,
            Util.playerData.tank
        );

        this.tankWarMap.node.getChildByName("tank").addChild(this._playerTank, 0, String(Util.playerData.uid));
    },

    randomSpawnLocation() {
        var tile = Util.chance.pickset(this.node.getComponent("TiledMapManager").getEmptyTiles())[0];
        return cc.v2(tile.mapX, tile.mapY);
        // return cc.v2(144,288); // @todo
    },

    killTank(incr) {
        this._playerData.killTank += incr;
    },


    emit(event, data) {
        if (this._socket) this._socket.emit(event, data);
    },


    /**************************************************
    *
    * king
    *
    * appear, force, duration, status
    *
    * announce appear
    * appear
    * announce disappear
    * disappear (server side usage)
    * dead
    *
    **************************************************/
    processKing(data) {
        // console.log(data);

        if (data.status == 'appear' && this._king) {
            if (!data.hasOwnProperty('room_king')) return;

            if (data.room_king.health <= 0) this.kingDestroy(true); // just in case
            else {
                // update health only
                if (this._kingHealth > data.room_king.health) this._kingHealth = data.room_king.health;
            }


            // process wood
            if (data.room_king.hasOwnProperty('wood_front')) {
                this.node.getComponent("TiledMapManager").removeWoodByPosition('front');
            }

            if (data.room_king.hasOwnProperty('wood_left')) {
                this.node.getComponent("TiledMapManager").removeWoodByPosition('left');
            }

            if (data.room_king.hasOwnProperty('wood_right')) {
                this.node.getComponent("TiledMapManager").removeWoodByPosition('right');
            }

            return;
        }

        if ((data.status == 'appear' || data.status == 'announce disappear') && !this._king && this._kingDestroy) {
            return;
        }


        this.unschedule(this._kingSchedule);


        if (data.status == 'announce appear' || data.status == 'appear') {
            if (data.status == 'announce appear') this.kingDestroy(); // just in case

            this.gameMenu.getChildByName('top bar').getChildByName('target').active = true;

            if (data.force == 'Blue') {
                this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('label blue').active = true;
                if (data.status == 'appear') this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('health blue').active = true;
                else this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('health blue').active = false;

                this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('label red').active = false;
                this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('health red').active = false;

                this._kingTimerLabel = this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('label blue').getComponent(cc.Label);
            }
            else {
                this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('label blue').active = false;
                this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('health blue').active = false;

                this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('label red').active = true;
                if (data.status == 'appear') this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('health red').active = true;
                else this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('health red').active = false;

                this._kingTimerLabel = this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('label red').getComponent(cc.Label);
            }
        }
        else {
            this.gameMenu.getChildByName('top bar').getChildByName('target').active = false;

            this.kingDestroy();
        }


        var msg = null;

        if (data.status == 'announce appear') {
            msg = ' Force King will appear soon';

            this._kingDestroy = false;
        }
        else if (data.status == 'appear' && !this._king) {
            msg = ' Force King has appeared on the extreme ' + ((data.force == 'Blue') ? 'left' : 'right' ) + ' of the map!';
        }
        else if (data.status == 'announce disappear') {
            msg = ' Force King has disappeared';
        }

        if (msg) {
            if (data.force == 'Blue') {
                msg = 'Blue' + msg;

                this.node.getChildByName('HUD').getChildByName('blue king').active = true;
                this.node.getChildByName('HUD').getChildByName('blue king').getChildByName('status').getComponent(cc.Label).string = msg;

                this.node.getChildByName('HUD').getChildByName('red king').active = false;

                this.scheduleOnce(function() {
                    this.node.getChildByName('HUD').getChildByName('blue king').active = false;
                }, 5);
            }
            else {
                msg = 'Red' + msg;

                this.node.getChildByName('HUD').getChildByName('red king').active = true;
                this.node.getChildByName('HUD').getChildByName('red king').getChildByName('status').getComponent(cc.Label).string = msg;

                this.node.getChildByName('HUD').getChildByName('blue king').active = false;

                this.scheduleOnce(function() {
                    this.node.getChildByName('HUD').getChildByName('red king').active = false;
                }, 5);
            }
        }


        if (data.status == 'appear' && !this._king && data.force == 'Blue') {
            this.createKing(GameEnum.Force.Blue);
        }
        else if (data.status == 'appear' && !this._king && data.force == 'Red') {
            this.createKing(GameEnum.Force.Red);
        }


        // round to nearest thousand
        this._kingTime = (Math.round(data.duration / 1000) * 1000) / 1000;

        if (data.hasOwnProperty('room_king')) this._kingHealth = (data.room_king.health);
        else this._kingHealth = 10;


        var self = this;
        if (this._kingTime > 0) {
            this._kingSchedule = function() {
                self._kingTime--;

                var minutes = Math.floor(self._kingTime / 60);
                var seconds = self._kingTime - minutes * 60;

                self._kingTimerLabel.string = minutes + ':' + seconds;

                if (self._kingTime <= 5 && self._kingTime > 0) {
                    self.node.getComponent("SoundManager").playEffectSound('timer', false);
                }

                if (self._kingTime <= 0) {
                    self.unschedule(self._kingSchedule);
                    self._kingSchedule = null;

                    self.gameMenu.getChildByName('top bar').getChildByName('target').active = false;
                }
            };

            this.schedule(this._kingSchedule, 1);
        }
    },

    createKing(force) {
        this._king = this.node.getComponent("TiledMapManager").createKing(force);
    },

    hitKing() {
        if (!this._king) return;

        this.node.getComponent("TankGroupManager").emitHitKing();

        this._kingHealth--;

        if (this._kingHealth <= 0) {
            this.kingDestroy(true);

            this._playerData.killKing++;
        }
    },

    hitKingWood(position) {
        if (!this._king) return;

        this.node.getComponent("TankGroupManager").emitHitKingWood(position);
    },

    updateKingHealth() {
        if (!this._king) return;

        switch (this._king.force) {
            case GameEnum.Force.Blue:
                this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('health blue').getComponent(cc.Label).string = this._kingHealth;
                break;
            case GameEnum.Force.Red:
                this.gameMenu.getChildByName('top bar').getChildByName('target').getChildByName('health red').getComponent(cc.Label).string = this._kingHealth;
                break;
        }
    },

    kingDestroy(destroy) {
        if (!this._king) return;

        this.unschedule(this._kingSchedule);

        this.gameMenu.getChildByName('top bar').getChildByName('target').active = false;

        var force = this._king.force;

        this.node.getComponent("TiledMapManager").removeKing(this._king);
        this._king = null;
        this._kingDestroy = true;

        if (destroy) {
            var self = this;
            this.node.getComponent("SoundManager").playEffectSound('explode-king', false, function() {
                // reward
                if ((force == GameEnum.Force.Blue && self._playerData.force == 'Red') || (force == GameEnum.Force.Red && self._playerData.force == 'Blue')) {
                    self.node.getComponent("SoundManager").playEffectSound('bonus', false);

                    self.incrLaser(10);
                    self.incrSpeed(10);
                }
            });
        }
    },


    /**************************************************
    *
    * hud
    *
    **************************************************/
    initGameMenuInfo() {
        this.gameMenu.group = Util.translateTextToGroup(GameEnum.Group.HUD); // put into hud group

        if (Util.playerData.user_data.unlock.includes('name')) {
        	this.gameMenu.getChildByName('menu').getChildByName('group').getChildByName('layout').getChildByName('names').getChildByName('na').active = false;
        	this.gameMenu.getChildByName('menu').getChildByName('group').getChildByName('layout').getChildByName('names').getChildByName('toggle').getComponent(cc.Toggle).interactable = true;

        	if (Util.playerData.settings.names == 1) {
        		this.gameMenu.getChildByName('menu').getChildByName('group').getChildByName('layout').getChildByName('names').getChildByName('toggle').getComponent(cc.Toggle).check();
        	}
        	else {
        		this.gameMenu.getChildByName('menu').getChildByName('group').getChildByName('layout').getChildByName('names').getChildByName('toggle').getComponent(cc.Toggle).uncheck();
        	}
        }

        if (Util.playerData.settings.bgm > 0) {
    		this.gameMenu.getChildByName('menu').getChildByName('group').getChildByName('layout').getChildByName('music').getChildByName('toggle').getComponent(cc.Toggle).check();
    	}
    	else {
    		this.gameMenu.getChildByName('menu').getChildByName('group').getChildByName('layout').getChildByName('music').getChildByName('toggle').getComponent(cc.Toggle).uncheck();
    	}

    	if (Util.playerData.settings.sfx > 0) {
    		this.gameMenu.getChildByName('menu').getChildByName('group').getChildByName('layout').getChildByName('sound').getChildByName('toggle').getComponent(cc.Toggle).check();
    	}
    	else {
    		this.gameMenu.getChildByName('menu').getChildByName('group').getChildByName('layout').getChildByName('sound').getChildByName('toggle').getComponent(cc.Toggle).uncheck();
    	}
    },

    incrLaser(incr) {
        this.laser += incr;
        if (this.laser > 0) this.nodeActionLaser.getChildByName('active').getComponent(cc.Button).interactable = true;
        else this.nodeActionLaser.getChildByName('active').getComponent(cc.Button).interactable = false;

        if (incr > 0) {
            this.node.getComponent("SoundManager").playEffectSound('laser-up', false);
        }

        Util.playerData.inventory.laser = this.laser;
        // Util.savePlayerData();
    },

    updateLaserUI() {
        this.gameMenu.getChildByName('bottom bar').getChildByName('laser').getChildByName('value').getComponent(cc.RichText).string = '<outline color=#654321 size=1>' + this.laser + '</outline>';
    },

    incrSpeed(incr) {
        this.speed += incr;
        if (this.speed > 0) this.nodeActionSpeed.getChildByName('active').getComponent(cc.Button).interactable = true;
        else this.nodeActionSpeed.getChildByName('active').getComponent(cc.Button).interactable = false;

        if (incr > 0) this.node.getComponent("SoundManager").playEffectSound('health-up', false);

        Util.playerData.inventory.speed = this.speed;
        // Util.savePlayerData();
    },

    updateSpeedUI() {
        this.gameMenu.getChildByName('bottom bar').getChildByName('speed').getChildByName('value').getComponent(cc.RichText).string = '<outline color=#654321 size=1>' + this.speed + '</outline>';
    },

    menu(event, customEventData) {
    	if (customEventData == 'open') {
    		this.gameMenu.getChildByName('menu').active = true;
    	}
    	else {
    		this.gameMenu.getChildByName('menu').active = false;
    	}
    },

    keyboard(event, customEventData) {
        if (customEventData == 'open') {
            this.node.getChildByName('HUD').getChildByName('keyboard').active = true;
        }
        else {
            this.node.getChildByName('HUD').getChildByName('keyboard').active = false;
            if (this._showKeyboard) {
                this._showKeyboard();
                this._showKeyboard = null;
            }
        }
    },

    gameOver() {
        // set score
        for (var i = 0; i < Util.playerData.scores.length; i++) {
            if (Util.playerData.scores[i].user_id == Util.playerData.uid) {
                Util.playerData.scores[i].eagle += this._playerData.killKing;
                Util.playerData.scores[i].tank += this._playerData.killTank;
                break;
            }
        }
        Util.savePlayerData();

    	var self = this;

        // show interstitial
        Util.showInterstitial().then(data => {
            // update inventory
            self.gameMenu.getChildByName('game over').getChildByName('group').getChildByName('layout').getChildByName('weapon').getChildByName('speed').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.speed + 'x Speed';
            self.gameMenu.getChildByName('game over').getChildByName('group').getChildByName('layout').getChildByName('weapon').getChildByName('laser').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.laser + 'x Laser';

            self.gameMenu.getChildByName('game over').active = true;
        });

        // force garbage collection
        cc.sys.garbageCollect();
    },

    settings(event, customEventData) {
    	switch (customEventData) {
    		case 'music':
    			if (event.isChecked) {
    				Util.playerData.settings.bgm = 1;
    			}
    			else {
    				Util.playerData.settings.bgm = 0;
    			}

    			this.node.getComponent("SoundManager").playMusic();

    			break;
    		case 'sound':
    			if (event.isChecked) Util.playerData.settings.sfx = 1;
    			else Util.playerData.settings.sfx = 0;

    			break;
    		case 'names':
    			if (event.isChecked) Util.playerData.settings.names = 1;
    			else Util.playerData.settings.names = 0;

    			this.node.getComponent("TankGroupManager").showNames();
    			
    			break;
    	}

    	Util.savePlayerData();
    },

    exit(event, customEventData) {
    	this.gameMenu.getChildByName('menu').active = false;
    	this.gameMenu.getChildByName('game over').active = false;

    	var self = this;
    	Util.createChoiceAlert(
    		'Are you sure you want to exit the game?',
    		{
    			text: 'Yes',
    			cb: function() {
                    self._joinRoom = false;
                    if (self._socket) self._socket.disconnect();

                    var params = 'token=' + Util.playerData.token;
                    params += '&game_id=' + Util.playerData.game_id;
                    params += '&speed=' + Util.playerData.inventory.speed;
                    params += '&laser=' + Util.playerData.inventory.laser;
                    params += '&duration=' + (Date.now() - self._startTime);
                    params += '&tank=' + self._playerData.killTank;
                    params += '&king=' + self._playerData.killKing;

                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.open("POST", Util.getConfig().server_url + '/gameSession', true);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState === 4) {
                            if (xmlhttp.status === 200) {
                            } else {
                            }
                        }
                    }
                    xmlhttp.onerror = function () {};
                    xmlhttp.send(params);


                    // set score
                    for (var i = 0; i < Util.playerData.scores.length; i++) {
                        if (Util.playerData.scores[i].user_id == Util.playerData.uid) {
                            Util.playerData.scores[i].eagle += self._playerData.killKing;
                            Util.playerData.scores[i].tank += self._playerData.killTank;
                            break;
                        }
                    }
                    Util.savePlayerData();

                    Util.ga.GameAnalytics.addProgressionEvent(Util.ga.EGAProgressionStatus.Complete, "game", "", "", (self._playerData.killTank + self._playerData.killKing));

    				cc.sys.localStorage.setItem('end_game', true);


                    self.unschedule(self._kingSchedule);
                    self.unschedule(self._kingOfflineSchedule);

                    // force garbage collection
                    cc.sys.garbageCollect();

    				Util.loadScene('home');
    			}
    		},
    		{
    			text: 'No',
    			cb: function() {
    				switch (customEventData) {
    					case 'menu':
	    					self.gameMenu.getChildByName('menu').active = true;
    						break;
    					case 'game over':
	    					self.gameMenu.getChildByName('game over').active = true;
    						break;
    				}
    			}
    		}
    	);
    },

    continue(event, customEventData) {
    	this.node.getComponent("TankGroupManager").playerRespawn(this.randomSpawnLocation());

        this.tankWarMap.getComponent('CameraFollow').target = this._playerTank;
        this.tankWarMap.getComponent('CameraFollow').follow();

        this.gameMenu.getChildByName('game over').active = false;
    },

    showRewarded(event, customEventData) {
        this.gameMenu.getChildByName('game over').active = false;

        var self = this;

        var updateServer = function(speed, laser) {
            Util.savePlayerData();

            var params = 'token=' + Util.playerData.token;
            params += '&game_id=' + Util.playerData.game_id;
            params += '&speed=' + speed;
            params += '&laser=' + laser;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", Util.getConfig().server_url + '/claimRewarded', true);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp.setRequestHeader("Cache-Control", "no-cache");
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState === 4) {
                    if (xmlhttp.status === 200) {
                        var response = JSON.parse(xmlhttp.responseText);
                        if (response.status === 'success') {
                        } else {}
                    } else {}
                }
            }
            xmlhttp.onerror = function () {};
            xmlhttp.send(params);
        };

        Util.createChoiceAlert(
            'Watch a video ads to receive 20x Speed and 10x Laser?',
            {
                text: 'Yes',
                cb: function() {
                    self.node.getComponent("SoundManager").pauseMusic();

                    Util.showRewarded().then(data => {
                        if (data === 'wait') {
                            Util.createAlert('Loading ads. Please try again in a while.', function() {
                                self.gameMenu.getChildByName('game over').active = true;
                                self.node.getComponent("SoundManager").resumeMusic();
                            });
                        }
                        else if (data === 'success' && Util.rewardedSuccess) {
                            Util.playerData.inventory.laser += 10;
                            Util.playerData.inventory.speed += 20;
                            Util.savePlayerData();

                            // update inventory
                            self.gameMenu.getChildByName('game over').getChildByName('group').getChildByName('layout').getChildByName('weapon').getChildByName('speed').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.speed + 'x Speed';
                            self.gameMenu.getChildByName('game over').getChildByName('group').getChildByName('layout').getChildByName('weapon').getChildByName('laser').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.laser + 'x Laser';

                            self.node.getComponent("SoundManager").playEffectSound("bonus", false);

                            updateServer(20, 10);
                            self.incrLaser(10);
                            self.incrSpeed(20);

                            Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "speed", 20, "rewarded_video", "");
                            Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "laser", 10, "rewarded_video", "");

                            self.gameMenu.getChildByName('game over').active = true;
                            self.node.getComponent("SoundManager").resumeMusic();
                        }
                        else {
                            Util.showInterstitial().then(data => {
                                if (data === 'wait') {
                                    Util.createAlert('Loading ads. Please try again in a while.', function() {
                                        self.gameMenu.getChildByName('game over').active = true;
                                        self.node.getComponent("SoundManager").resumeMusic();
                                    });
                                }
                                else if (data === 'success') {
                                    Util.createAlert('A non-video ads was shown instead. You have received 4x Speed and 4x Laser.', function() {
                                        Util.playerData.inventory.laser += 4;
                                        Util.playerData.inventory.speed += 4;
                                        Util.savePlayerData();

                                        // update inventory
                                        self.gameMenu.getChildByName('game over').getChildByName('group').getChildByName('layout').getChildByName('weapon').getChildByName('speed').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.speed + 'x Speed';
                                        self.gameMenu.getChildByName('game over').getChildByName('group').getChildByName('layout').getChildByName('weapon').getChildByName('laser').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.laser + 'x Laser';

                                        self.node.getComponent("SoundManager").playEffectSound("bonus", false);

                                        updateServer(4, 4);
                                        self.incrLaser(4);
                                        self.incrSpeed(4);

                                        Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "speed", 4, "rewarded_interstitial", "");
                                        Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "laser", 4, "rewarded_interstitial", "");

                                        self.gameMenu.getChildByName('game over').active = true;
                                        self.node.getComponent("SoundManager").resumeMusic();
                                    });
                                }
                                else if (data === 'fail') {
                                    Util.createAlert('Sorry, we are unable to find any ads to show you.', function() {
                                        self.gameMenu.getChildByName('game over').active = true;
                                        self.node.getComponent("SoundManager").resumeMusic();
                                    });
                                }
                            });
                        }
                    });
                }
            },
            {
                text: 'No',
                cb: function() {
                    self.gameMenu.getChildByName('game over').active = true;
                    self.node.getComponent("SoundManager").resumeMusic();
                }
            }
        );
    },

    challenge(event, customEventData) {
        var self = this;

        self.gameMenu.getChildByName('game over').active = false;

        if (!Util.playerData.hasOwnProperty('fbinstant_share') || Util.playerData.fbinstant_share <= 0 || Date.now() - Util.playerData.fbinstant_share >= 86400000) {
            Util.createChoiceAlert(
                'Challenge your friends and receive 30x Speed, 20x Laser?',
                {
                    text: 'Yes',
                    cb: function() {
                        FBInstant.shareAsync({
                            intent: 'CHALLENGE',
                            image: Util.challengeBase64,
                            text: 'This is good fun!',
                            data: { no: 'yes' },
                        }).then(function() {
                            Util.playerData.inventory.laser += 20;
                            Util.playerData.inventory.speed += 30;
                            Util.playerData.fbinstant_share = Date.now();
                            Util.savePlayerData();

                            // update inventory
                            self.gameMenu.getChildByName('game over').getChildByName('group').getChildByName('layout').getChildByName('weapon').getChildByName('speed').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.speed + 'x Speed';
                            self.gameMenu.getChildByName('game over').getChildByName('group').getChildByName('layout').getChildByName('weapon').getChildByName('laser').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.laser + 'x Laser';

                            self.node.getComponent("SoundManager").playEffectSound("bonus", false);

                            self.incrLaser(4);
                            self.incrSpeed(4);

                            var params = 'token=' + Util.playerData.token;
                            params += '&game_id=' + Util.playerData.game_id;
                            params += '&speed=' + 30;
                            params += '&laser=' + 20;

                            var xmlhttp = new XMLHttpRequest();
                            xmlhttp.open("POST", Util.getConfig().server_url + '/claimShare', true);
                            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                            xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                            xmlhttp.onreadystatechange = function() {
                                if (xmlhttp.readyState === 4) {
                                    if (xmlhttp.status === 200) {
                                        self.gameMenu.getChildByName('game over').active = true;

                                        var response = JSON.parse(xmlhttp.responseText);
                                        if (response.status === 'success') {
                                        } else {}
                                    }
                                }
                            }
                            xmlhttp.onerror = function () {
                                self.gameMenu.getChildByName('game over').active = true;
                            };
                            xmlhttp.send(params);

                            Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "speed", 30, "fbinstant_share", "");
                            Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "laser", 20, "fbinstant_share", "");
                        });
                    }
                },
                {
                    text: 'No',
                    cb: function() {
                        self.gameMenu.getChildByName('game over').active = true;
                    }
                }
            );
        }
        else if (Util.playerData.hasOwnProperty('fbinstant_share')) {
            Util.createAlert('You can only claim the rewards once daily', function() {
                self.gameMenu.getChildByName('game over').active = true;
            });
        }
    },

    networkStatus(show, status) {
        this.node.getChildByName('HUD').getChildByName('network').active = show;
        this.node.getChildByName('HUD').getChildByName('network').getChildByName('status').getComponent(cc.Label).string = status;
    },

    showHUD(bool) {
        this.node.getChildByName('HUD').getChildByName('joystick').active = (cc.sys.isMobile) ? bool : false;
        this.node.getChildByName('HUD').getChildByName('top bar').active = bool;
        this.node.getChildByName('HUD').getChildByName('bottom bar').active = bool;
        this.node.getChildByName('HUD').getChildByName('menu button').active = bool;
    },

    players(event, customEventData) {
        if (customEventData == 'open') {
            this.node.getChildByName('HUD').getChildByName('players').active = true;

            // delete existing
            var parent = this.node.getChildByName('HUD').getChildByName('players').getChildByName('texture').getChildByName('list').getChildByName('view').getChildByName('content');
            var count = parent.children.length;
            for (var j = 1; j < count; j++) {
                parent.children[1].removeFromParent(false);
            }


            for (let j = 0; j < this.tankWarMap.node.getChildByName("tank").childrenCount; j++) {
                if (this.tankWarMap.node.getChildByName("tank").children[j].player_id == Util.playerData.uid) continue;

                var player = cc.instantiate(parent.children[0]);
                player.parent = parent;

                player.getChildByName('name').getComponent(cc.Label).string = this.tankWarMap.node.getChildByName("tank").children[j].player_name;

                // photo
                if (!this.tankWarMap.node.getChildByName("tank").children[j].player_photo) {
                    player.getChildByName('action').getChildByName('photo').active = false;
                }
                else {
                    player.getChildByName('action').getChildByName('photo').active = true;

                    var clickEventHandler = new cc.Component.EventHandler();
                    clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
                    clickEventHandler.component = "Game";// This is the code file name
                    clickEventHandler.handler = "showPhoto";
                    clickEventHandler.customEventData = this.tankWarMap.node.getChildByName("tank").children[j].player_photo;

                    var button = player.getChildByName('action').getChildByName('photo').getComponent(cc.Button);
                    button.clickEvents = [];
                    button.clickEvents.push(clickEventHandler);
                }

                // add
                if (!this.tankWarMap.node.getChildByName("tank").children[j].getComponent('TankManager').isAuto) {
                    var found = false;
                    for (var k = 0; k < Util.playerData.friends.length; k++) {
                        if (Util.playerData.friends[k].friend_id == this.tankWarMap.node.getChildByName("tank").children[j].player_id) {
                            found = true;
                            break;
                        }
                    }

                    if (found) {
                        player.getChildByName('action').getChildByName('add').active = false;
                    }
                    else {
                        player.getChildByName('action').getChildByName('add').active = true;

                        var clickEventHandler = new cc.Component.EventHandler();
                        clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
                        clickEventHandler.component = "Game";// This is the code file name
                        clickEventHandler.handler = "addFriend";
                        clickEventHandler.customEventData = this.tankWarMap.node.getChildByName("tank").children[j].player_id;

                        var button = player.getChildByName('action').getChildByName('add').getComponent(cc.Button);
                        button.clickEvents = [];
                        button.clickEvents.push(clickEventHandler);
                    }
                }
                else {
                    player.getChildByName('action').getChildByName('add').active = false;
                }

                player.active = true;
            }
        }
        else {
            this.node.getChildByName('HUD').getChildByName('players').active = false;
        }
    },

    addFriend(event, customEventData) {
        // console.log(customEventData);
        this.players(null, 'close');

        var self = this;
        Util.createChoiceAlert(
            'Add player as friend?',
            {
                text: 'Yes',
                cb: function() {
                    Util.showLoading();

                    var params = 'token=' + Util.playerData.token;
                    params += '&game_id=' + Util.playerData.game_id;
                    params += '&id=' + customEventData;

                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.open("POST", Util.getConfig().server_url + '/addFriend', true);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState === 4) {
                            if (xmlhttp.status === 200) {
                                var response = JSON.parse(xmlhttp.responseText);
                                if (response.status === 'success') {
                                    Util.playerData.friends = response.friends;

                                    Util.savePlayerData();

                                    Util.createAlert('Friend added successfully. However the person will not see your chat messages if your friend request has not been approved.', function() {
                                        Util.removeLoading();

                                        self.players(null, 'open');
                                    });
                                } else {
                                    Util.createAlert('Failed to add friend', function() {
                                        Util.removeLoading();

                                        self.players(null, 'open');
                                    });
                                }
                            }
                        }
                    }
                    xmlhttp.onerror = function () {
                        Util.createAlert('Failed to add friend', function() {
                            Util.removeLoading();

                            self.players(null, 'open');
                        });
                    };
                    xmlhttp.send(params);
                }
            },
            {
                text: 'No',
                cb: function() {
                    self.players(null, 'open');
                }
            }
        );
    },

    showPhoto(event, customEventData) {
        this.players(null, 'close');

        var self = this;
        Util.showImage(customEventData, function() {
            self.players(null, 'open');
        });
    },


    /**************************************************
    *
    * events
    *
    **************************************************/
    initListenHandlers() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onPlayerKeyDownCallback, this);
    },

    unListenHandlers() {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onPlayerKeyDownCallback, this);
    },

    onPlayerKeyDownCallback(event) {
        switch (event.keyCode) {
            case cc.macro.KEY.w:
                if (!this._playerTank) return;
                // this._playerTank.getComponent('TankManager').begin();
                this.node.getComponent("TankGroupManager").changeDirection(GameConst.Direction[0]);
                break;
            case cc.macro.KEY.a:
                if (!this._playerTank) return;
                // this._playerTank.getComponent('TankManager').begin();
                this.node.getComponent("TankGroupManager").changeDirection(GameConst.Direction[1]);
                break;
            case cc.macro.KEY.s:
                if (!this._playerTank) return;
                // this._playerTank.getComponent('TankManager').begin();
                this.node.getComponent("TankGroupManager").changeDirection(GameConst.Direction[2]);
                break;
            case cc.macro.KEY.d:
                if (!this._playerTank) return;
                // this._playerTank.getComponent('TankManager').begin();
                this.node.getComponent("TankGroupManager").changeDirection(GameConst.Direction[3]);
                break;
            case cc.macro.KEY.j:
                if (!this._playerTank) return;
                // this._playerTank.getComponent('TankManager').begin();
                this.actionSpeed();
                break;
            case cc.macro.KEY.k:
                if (!this._playerTank) return;
                // this._playerTank.getComponent('TankManager').begin();
                this.actionFire();
                break;
            case cc.macro.KEY.l:
                if (!this._playerTank) return;
                // this._playerTank.getComponent('TankManager').begin();
                this.actionLaser();
                break;
        }
    },

    onPlayerDirectionCallback(event, customEventData) {
        switch (customEventData) {
            case 'up':
                this.onPlayerKeyDownCallback({keyCode: cc.macro.KEY.w});
                break;
            case 'left':
                this.onPlayerKeyDownCallback({keyCode: cc.macro.KEY.a});
                break;
            case 'down':
                this.onPlayerKeyDownCallback({keyCode: cc.macro.KEY.s});
                break;
            case 'right':
                this.onPlayerKeyDownCallback({keyCode: cc.macro.KEY.d});
                break;
        }
    },

    pauseInput(bool) {
        if (!this._playing) return;

        this.node.getChildByName('HUD').getChildByName('joystick').active = !bool;
        this.node.getChildByName('HUD').getChildByName('bottom bar').active = !bool;

        if (bool) {
            this.unListenHandlers();
        }
        else {
            this.initListenHandlers();
        }
    },

    onTouchStart() {
    },

    onTouchEnd(event, data) {
    },


    // /**************************************************
    // *
    // * up    x=0 y=1
    // * down  x=0 y=-1
    // * left  x=-1 y=0
    // * right x=1 y=0
    // *
    // **************************************************/
    // onTouchMove(event, data) {
    //     // up
    //     if (Math.abs(data.moveDistance.x) < 0.9 && data.moveDistance.y > 0.998) {
    //         this._playerTank.getComponent('TankManager').begin();
    //         this.node.getComponent("TankGroupManager").changeDirection(GameConst.Direction[0]) = GameConst.Direction[0];
    //         console.log(GameConst.Direction[0], data.moveDistance.x, data.moveDistance.y)
    //     }
    //     // down
    //     else if (Math.abs(data.moveDistance.x) < 0.9 && data.moveDistance.y < -0.998) {
    //         this._playerTank.getComponent('TankManager').begin();
    //         this.node.getComponent("TankGroupManager").changeDirection(GameConst.Direction[0]) = GameConst.Direction[2];
    //         console.log(GameConst.Direction[2], data.moveDistance.x, data.moveDistance.y)
    //     }
    //     // left
    //     else if (Math.abs(data.moveDistance.y) < 0.9 && data.moveDistance.x < -0.998) {
    //         this._playerTank.getComponent('TankManager').begin();
    //         this.node.getComponent("TankGroupManager").changeDirection(GameConst.Direction[0]) = GameConst.Direction[1];
    //         console.log(GameConst.Direction[1], data.moveDistance.x, data.moveDistance.y)
    //     }
    //     // right
    //     else if (Math.abs(data.moveDistance.y) < 0.9 && data.moveDistance.x > 0.998) {
    //         this._playerTank.getComponent('TankManager').begin();
    //         this.node.getComponent("TankGroupManager").changeDirection(GameConst.Direction[0]) = GameConst.Direction[3];
    //         console.log(GameConst.Direction[3], data.moveDistance.x, data.moveDistance.y)
    //     }
    // },

    disableFire() {
        /**************************************************
        * disable fire
        **************************************************/
        this.nodeActionFire.getChildByName('active').getComponent(cc.Button).interactable = false;

        this.nodeActionFire.getChildByName('progress').active = true;
        this.nodeActionFire.getChildByName('progress').getComponent(ProgressBarEx).progressTo(1, 0);


        /**************************************************
        * disable laser
        **************************************************/
        this.nodeActionLaser.getChildByName('active').getComponent(cc.Button).interactable = false;
        
        this.nodeActionLaser.getChildByName('progress').active = true;
        this.nodeActionLaser.getChildByName('progress').getComponent(ProgressBarEx).progressTo(1, 0);


        /**************************************************
        * disable speed
        **************************************************/
        if (!this.nodeActionSpeed.getChildByName('progress').active) this.disableSpeed();
    },

    enableFire() {
        /**************************************************
        * enable fire
        **************************************************/
        this.nodeActionFire.getChildByName('progress').getComponent(ProgressBarEx).progressTo(0, 1);
        this.nodeActionFire.getChildByName('progress').active = false;

        this.nodeActionFire.getChildByName('active').getComponent(cc.Button).interactable = true;


        /**************************************************
        * enable laser
        **************************************************/
        this.nodeActionLaser.getChildByName('progress').getComponent(ProgressBarEx).progressTo(0, 1);
        this.nodeActionLaser.getChildByName('progress').active = false;

        if (this.laser > 0) {
            this.nodeActionLaser.getChildByName('active').getComponent(cc.Button).interactable = true;
        }
    },

    actionFire(event, customEventData) {
        if (!this._playerTank.active || !this.nodeActionFire.getChildByName('active').getComponent(cc.Button).interactable) return;

        // this._playerTank.getComponent('TankManager').begin();

        if (this.node.getComponent("TankGroupManager").launchBullet(GameEnum.BulletRange.normal)) {
            this.disableFire();
        }
    },

    actionLaser(event, customEventData) {
        if (!this._playerTank.active || !this.nodeActionLaser.getChildByName('active').getComponent(cc.Button).interactable || this.laser <= 0) return;

        // this._playerTank.getComponent('TankManager').begin();

        if (this.node.getComponent("TankGroupManager").launchBullet(GameEnum.BulletRange.laser)) {
            this.incrLaser(-1);
            this.disableFire();
        }
    },

    disableSpeed() {
        this.nodeActionSpeed.getChildByName('active').getComponent(cc.Button).interactable = false;

        this.nodeActionSpeed.getChildByName('progress').active = true;
        this.nodeActionSpeed.getChildByName('progress').getComponent(ProgressBarEx).progressTo(1, 0);


        if (!this.nodeActionFire.getChildByName('progress').active) this.disableFire();
    },

    enableSpeed() {
        this.nodeActionSpeed.getChildByName('progress').getComponent(ProgressBarEx).progressTo(0, 1);
        this.nodeActionSpeed.getChildByName('progress').active = false;
        
        if (this.speed > 0) {
            this.nodeActionSpeed.getChildByName('active').getComponent(cc.Button).interactable = true;
        }
    },

    actionSpeed(event, customEventData) {
        if (!this._playerTank.active || !this.nodeActionSpeed.getChildByName('active').getComponent(cc.Button).interactable || this.speed <= 0) return;
        // this._playerTank.getComponent('TankManager').begin();

        if (this.node.getComponent("TankGroupManager").speed()) {
            this.incrSpeed(-1);
            this.disableSpeed();
        }
    },
});