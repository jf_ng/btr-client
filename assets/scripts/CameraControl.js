"use strict";

cc.Class({
    extends: cc.Component,

    properties: {
        target: {
            default: null,
            type: cc.Node
        },

        camera: cc.Camera,
    },

    // use this for initialization
    onLoad: function () {
    },

    lateUpdate: function(dt) {
        if (!this.target) return;

        this.node.position = this.target.position;
    },
});