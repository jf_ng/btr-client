"use strict";

var Util = require("./Util");

cc.Class({
    extends: cc.Component,

    editor: {
        // executeInEditMode: true
    },

    properties: {
        viewRect: null,
        scrollView: null,
        scrollContent: null,

        _searchId: null,

        _worldList: null,
    },

    onLoad() {
        Util.cacheInterstitial();
        Util.cacheRewarded();
    },

    start() {
        this.setControl(null, Util.playerData.control);

        this.node.getComponent("SoundManager").playHome();


        if (Util.playerData.settings.bgm > 0) {
            this.node.getChildByName('sound').getChildByName('texture').getChildByName('layout').getChildByName('music').getChildByName('toggle').getComponent(cc.Toggle).check();
        }
        else {
            this.node.getChildByName('sound').getChildByName('texture').getChildByName('layout').getChildByName('music').getChildByName('toggle').getComponent(cc.Toggle).uncheck();
        }

        if (Util.playerData.settings.sfx > 0) {
            this.node.getChildByName('sound').getChildByName('texture').getChildByName('layout').getChildByName('sound').getChildByName('toggle').getComponent(cc.Toggle).check();
        }
        else {
            this.node.getChildByName('sound').getChildByName('texture').getChildByName('layout').getChildByName('sound').getChildByName('toggle').getComponent(cc.Toggle).uncheck();
        }


        // set name
        this.node.getChildByName('info').getChildByName('name').getComponent(cc.EditBox).string = Util.playerData.name;

        // set score
        for (var i = 0; i < Util.playerData.scores.length; i++) {
            if (Util.playerData.scores[i].user_id == Util.playerData.uid) {
                this.node.getChildByName('score').getChildByName('group').getChildByName('king').getChildByName('value').getComponent(cc.Label).string = Util.playerData.scores[i].eagle;
                this.node.getChildByName('score').getChildByName('group').getChildByName('tank').getChildByName('value').getComponent(cc.Label).string = Util.playerData.scores[i].tank;
                break;
            }
        }

        // set game id
        this.node.getChildByName('info').getChildByName('game id').getComponent(cc.Label).string = 'Game ID: ' + Util.playerData.game_id;

        // set inventory
        this.node.getChildByName('info').getChildByName('inventory').getChildByName('speed').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.speed + 'x Speed';
        this.node.getChildByName('info').getChildByName('inventory').getChildByName('laser').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.laser + 'x Laser';


        // red icons
        if (Util.playerData.user_data.new_chat_message > 0 || Util.playerData.user_data.new_friend_request > 0) {
            this.node.getChildByName('friend group').getChildByName('friend').active = false;
            this.node.getChildByName('friend group').getChildByName('friend red').active = true;
        }
        else {
            this.node.getChildByName('friend group').getChildByName('friend').active = true;
            this.node.getChildByName('friend group').getChildByName('friend red').active = false;
        }

        if (Util.playerData.user_data.new_chat_message > 0) {
            this.node.getChildByName('friend group').getChildByName('group').getChildByName('chat').active = false;
            this.node.getChildByName('friend group').getChildByName('group').getChildByName('chat red').active = true;
        }
        else {
            this.node.getChildByName('friend group').getChildByName('group').getChildByName('chat').active = true;
            this.node.getChildByName('friend group').getChildByName('group').getChildByName('chat red').active = false;
        }

        if (Util.playerData.user_data.new_friend_request > 0) {
            this.node.getChildByName('friend group').getChildByName('group').getChildByName('list').active = false;
            this.node.getChildByName('friend group').getChildByName('group').getChildByName('list red').active = true;
        }
        else {
            this.node.getChildByName('friend group').getChildByName('group').getChildByName('list').active = true;
            this.node.getChildByName('friend group').getChildByName('group').getChildByName('list red').active = false;
        }


        // hide buttons if fbinstant
        if (Util.getConfig().fbinstant) {
            this.node.getChildByName('menu group').getChildByName('group').getChildByName('facebook').active = false;
            this.node.getChildByName('menu group').getChildByName('group').getChildByName('rate').active = false;
        }
        else {
            this.node.getChildByName('friend group').getChildByName('group').getChildByName('challenge').active = false;
        }
    },

    changeName(event, customEventData) {
        var name = this.node.getChildByName('info').getChildByName('name').getComponent(cc.EditBox).string.trim();
        if (name == Util.playerData.name) {
            return;
        }


        var params = 'token=' + Util.playerData.token;
        params += '&game_id=' + Util.playerData.game_id;
        params += '&name=' + name;

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", Util.getConfig().server_url + '/changeName', true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("Cache-Control", "no-cache");
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4) {
                if (xmlhttp.status === 200) {
                    var response = JSON.parse(xmlhttp.responseText);
                    if (response.status === 'success') {
                        Util.playerData.name = response.name;
                        Util.savePlayerData();
                    } else {}
                } else {}
            }
        }
        xmlhttp.onerror = function () {};
        xmlhttp.send(params);
    },

    showMenu(event, customEventData) {
        if (customEventData === true || customEventData === 'true') {
            cc.tween(this.node.getChildByName('menu group'))
            .by(0, { position: cc.v2(-cc.winSize.width, 0) })
            .call(() => {
                this.node.getChildByName('menu group').getChildByName('menu').active = false;
                this.node.getChildByName('menu group').getChildByName('group').active = true;
            })
            .to(0.5, { position: cc.v2(-cc.winSize.width / 2 * 0.9, this.node.getChildByName('menu group').position.y) })
            .start()
        }
        else {
            cc.tween(this.node.getChildByName('menu group'))
            .by(0.5, { position: cc.v2(-this.node.getChildByName('menu group').width, 0) })
            .to(0, { position: cc.v2(-cc.winSize.width / 2 * 0.9, this.node.getChildByName('menu group').position.y) })
            .call(() => {
                this.node.getChildByName('menu group').getChildByName('menu').active = true;
                this.node.getChildByName('menu group').getChildByName('group').active = false;
            })
            .start()
        }
    },

    menuAction(event, customEventData) {
        switch (customEventData) {
            case 'control':
                this.node.getChildByName('control').active = true;
                break;
            case 'sound':
                this.node.getChildByName('sound').active = true;
                break;
            case 'facebook':
                cc.sys.openURL('https://www.facebook.com/Battle-Tanks-Reloaded-100637411766390');
                break;
            case 'rate us':
                break;
            case 'skin':
                this.node.getChildByName('skin').active = true;
                break;
            case 'close':
                this.node.getChildByName('control').active = false;
                this.node.getChildByName('sound').active = false;
                this.node.getChildByName('skin').active = false;
                break;
        }
    },

    setSkin(event, customEventData) {
        if (!Util.playerData.user_data.unlock.includes('skin')) {
            Util.createAlert('Sorry, you cannot change tank yet. Please consider purchasing this option.', function() {});
        }
        else {
            Util.playerData.tank = customEventData;
            Util.createAlert('You have successfully changed your tank skin.', function() {});
        }
    },

    setControl(event, customEventData) {
        switch (customEventData) {
            case 'left':
                this.node.getChildByName('control').getChildByName('texture').getChildByName('content').getChildByName('left').getChildByName('active').active = true;
                this.node.getChildByName('control').getChildByName('texture').getChildByName('content').getChildByName('left').getChildByName('disable').active = false;

                this.node.getChildByName('control').getChildByName('texture').getChildByName('content').getChildByName('right').getChildByName('active').active = false;
                this.node.getChildByName('control').getChildByName('texture').getChildByName('content').getChildByName('right').getChildByName('disable').active = true;

                Util.playerData.control = 'left';

                break;
            case 'right':
                this.node.getChildByName('control').getChildByName('texture').getChildByName('content').getChildByName('left').getChildByName('active').active = false;
                this.node.getChildByName('control').getChildByName('texture').getChildByName('content').getChildByName('left').getChildByName('disable').active = true;

                this.node.getChildByName('control').getChildByName('texture').getChildByName('content').getChildByName('right').getChildByName('active').active = true;
                this.node.getChildByName('control').getChildByName('texture').getChildByName('content').getChildByName('right').getChildByName('disable').active = false;

                Util.playerData.control = 'right';

                break;
        }

        Util.savePlayerData();
    },

    sound(event, customEventData) {
        switch (customEventData) {
            case 'music':
                if (event.isChecked) {
                    Util.playerData.settings.bgm = 1;
                }
                else {
                    Util.playerData.settings.bgm = 0;
                }

                this.node.getComponent("SoundManager").playHome();

                break;
            case 'sound':
                if (event.isChecked) Util.playerData.settings.sfx = 1;
                else Util.playerData.settings.sfx = 0;

                break;
        }

        Util.savePlayerData();
    },

    showFriend(event, customEventData) {
        if (customEventData === true || customEventData === 'true') {
            cc.tween(this.node.getChildByName('friend group'))
            .by(0, { position: cc.v2(cc.winSize.width, 0) })
            .call(() => {
                this.node.getChildByName('friend group').getChildByName('friend').active = false;
                this.node.getChildByName('friend group').getChildByName('friend red').active = false;
                this.node.getChildByName('friend group').getChildByName('group').active = true;
            })
            .to(0.5, { position: cc.v2(cc.winSize.width / 2 * 0.9, this.node.getChildByName('friend group').position.y) })
            .start()
        }
        else {
            cc.tween(this.node.getChildByName('friend group'))
            .by(0.5, { position: cc.v2(this.node.getChildByName('friend group').width, 0) })
            .to(0, { position: cc.v2(cc.winSize.width / 2 * 0.9, this.node.getChildByName('friend group').position.y) })
            .call(() => {
                if (Util.playerData.user_data.new_chat_message > 0 || Util.playerData.user_data.new_friend_request > 0) {
                    this.node.getChildByName('friend group').getChildByName('friend').active = false;
                    this.node.getChildByName('friend group').getChildByName('friend red').active = true;
                }
                else {
                    this.node.getChildByName('friend group').getChildByName('friend').active = true;
                    this.node.getChildByName('friend group').getChildByName('friend red').active = false;
                }

                this.node.getChildByName('friend group').getChildByName('group').active = false;
            })
            .start()
        }
    },

    friendAction(event, customEventData) {
        switch (customEventData) {
            case 'list':
                // **************************************************
                // show requests first
                // **************************************************
                var request = false;
                for (var i = 0; i < Util.playerData.friends.length; i++) {
                    if (Util.playerData.friends[i].status == 'request') {
                        request = true;
                        break;
                    }
                }

                if (request) {
                    this.showFriendList(true);

                    this.scrollView = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('request').active = true;
                    this.scrollView = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('list').active = false;
                    this.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').active = false;

                    this.node.getChildByName('friend list').active = true;

                    this.scrollView = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('request').getChildByName('scrollview').getChildByName('view');
                    this.scrollContent = this.scrollView.getChildByName('content');
                }
                else {
                    this.showFriendList();

                    this.scrollView = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('request').active = false;
                    this.scrollView = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('list').active = true;
                    this.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').active = false;

                    this.node.getChildByName('friend list').active = true;

                    this.scrollView = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('list').getChildByName('view');
                    this.scrollContent = this.scrollView.getChildByName('content');
                }
                
                break;
            case 'world':
                this.showWorldList();

                this.scrollView = this.node.getChildByName('friend world').getChildByName('texture').getChildByName('list').active = true;

                this.node.getChildByName('friend world').active = true;

                this.scrollView = this.node.getChildByName('friend world').getChildByName('texture').getChildByName('list').getChildByName('view');
                this.scrollContent = this.scrollView.getChildByName('content');
                
                break;
            case 'search':
                this.scrollView = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('list').active = false;
                this.scrollView = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('request').active = false;

                this.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').active = true;
                this.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('input').getChildByName('editbox').getComponent(cc.EditBox).focus();
                
                break;
            case 'leaderboard':
                this.showFriendRanking();

                this.node.getChildByName('friend ranking').active = true;

                this.scrollView = this.node.getChildByName('friend ranking').getChildByName('texture').getChildByName('list').getChildByName('view');
                this.scrollContent = this.scrollView.getChildByName('content');
                
                break;
            case 'chat':
                this.node.getComponent("SoundManager").stopAll();
                // Util.showLoading();
                Util.loadScene('chat');
                break;
            case 'play':
                this.node.getChildByName('friend play').active = true;

                this.scrollView = this.node.getChildByName('friend play').getChildByName('texture').getChildByName('list').getChildByName('view');
                this.scrollContent = this.scrollView.getChildByName('content');

                this.showFriendRooms();

                break;
            case 'share':
                if (Util.getConfig().fbinstant) {
                    var apis = FBInstant.getSupportedAPIs();
                    if (apis.includes('shareAsync')) {
                        if (!Util.playerData.hasOwnProperty('fbinstant_share') || Util.playerData.fbinstant_share <= 0 || Date.now() - Util.playerData.fbinstant_share >= 86400000) {
                            var self = this;

                            Util.createChoiceAlert(
                                'Challenge your friends and receive 30x Speed, 20x Laser?',
                                {
                                    text: 'Yes',
                                    cb: function() {
                                        FBInstant.shareAsync({
                                            intent: 'CHALLENGE',
                                            image: Util.challengeBase64,
                                            text: 'This is good fun!',
                                            data: { no: 'yes' },
                                        }).then(function() {
                                            Util.playerData.inventory.laser += 20;
                                            Util.playerData.inventory.speed += 30;
                                            Util.playerData.fbinstant_share = Date.now();
                                            Util.savePlayerData();

                                            self.node.getChildByName('info').getChildByName('inventory').getChildByName('speed').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.speed + 'x Speed';
                                            self.node.getChildByName('info').getChildByName('inventory').getChildByName('laser').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.laser + 'x Laser';

                                            self.node.getComponent("SoundManager").playEffectSound("bonus", false);

                                            var params = 'token=' + Util.playerData.token;
                                            params += '&game_id=' + Util.playerData.game_id;
                                            params += '&speed=' + 30;
                                            params += '&laser=' + 20;

                                            var xmlhttp = new XMLHttpRequest();
                                            xmlhttp.open("POST", Util.getConfig().server_url + '/claimShare', true);
                                            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                                            xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                                            xmlhttp.onreadystatechange = function() {
                                                if (xmlhttp.readyState === 4) {
                                                    if (xmlhttp.status === 200) {
                                                        var response = JSON.parse(xmlhttp.responseText);
                                                        if (response.status === 'success') {
                                                        } else {}
                                                    } else {}
                                                }
                                            }
                                            xmlhttp.onerror = function () {};
                                            xmlhttp.send(params);

                                            Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "speed", 30, "fbinstant_share", "");
                                            Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "laser", 20, "fbinstant_share", "");
                                        });
                                    }
                                },
                                {
                                    text: 'No',
                                    cb: function() {}
                                }
                            );
                        }
                        else if (Util.playerData.hasOwnProperty('fbinstant_share')) {
                            Util.createAlert('You can only claim the rewards once daily', function() {});
                        }
                    }
                }

                break;
            case 'close':
                this.node.getChildByName('friend list').active = false;
                this.node.getChildByName('friend ranking').active = false;
                this.node.getChildByName('friend play').active = false;
                this.node.getChildByName('friend world').active = false;

                this.scrollView = null;
                this.scrollContent = null;

                break;
        }
    },

    showFriendList(showRequest) {
        Util.playerData.user_data.new_friend_request = 0;
        Util.savePlayerData();

        
        // delete existing
        var parent = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('request').getChildByName('scrollview').getChildByName('view').getChildByName('content');
        var count = parent.children.length;
        for (var j = 1; j < count; j++) {
            parent.children[1].removeFromParent(false);
            // parent.children[1].destroy();
        }

        // delete existing
        var parent = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('list').getChildByName('view').getChildByName('content');
        var count = parent.children.length;
        for (var j = 1; j < count; j++) {
            parent.children[1].removeFromParent(false);
            // parent.children[1].destroy();
        }


        if (showRequest) {
            parent = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('request').getChildByName('scrollview').getChildByName('view').getChildByName('content');
        }
        else {
            parent = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('list').getChildByName('view').getChildByName('content');
        }


        for (var i = 0; i < Util.playerData.friends.length; i++) {
            if (showRequest && Util.playerData.friends[i].status != 'request') continue;

            var friend = cc.instantiate(parent.children[0]);
            friend.parent = parent;

            friend.getChildByName('name').getComponent(cc.Label).string = Util.playerData.friends[i].friend_name;


            var clickEventHandler = new cc.Component.EventHandler();
            clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
            clickEventHandler.component = "Home";// This is the code file name
            clickEventHandler.handler = "showPhoto";
            clickEventHandler.customEventData = Util.playerData.friends[i].friend_photo;

            var button = friend.getComponent(cc.Button);
            button.clickEvents = [];
            button.clickEvents.push(clickEventHandler);


            var clickEventHandler = new cc.Component.EventHandler();
            clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
            clickEventHandler.component = "Home";// This is the code file name
            clickEventHandler.handler = "blockFriend";
            clickEventHandler.customEventData = Util.playerData.friends[i].friend_id;

            var button = friend.getChildByName('action').getChildByName('block').getComponent(cc.Button);
            button.clickEvents = [];
            button.clickEvents.push(clickEventHandler);


            var clickEventHandler = new cc.Component.EventHandler();
            clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
            clickEventHandler.component = "Home";// This is the code file name
            clickEventHandler.handler = "deleteFriend";
            clickEventHandler.customEventData = Util.playerData.friends[i].friend_id;

            var button = friend.getChildByName('action').getChildByName('delete').getComponent(cc.Button);
            button.clickEvents = [];
            button.clickEvents.push(clickEventHandler);


            if (showRequest) {
                var clickEventHandler = new cc.Component.EventHandler();
                clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
                clickEventHandler.component = "Home";// This is the code file name
                clickEventHandler.handler = "acceptFriend";
                clickEventHandler.customEventData = Util.playerData.friends[i].friend_id;

                var button = friend.getChildByName('action').getChildByName('accept').getComponent(cc.Button);
                button.clickEvents = [];
                button.clickEvents.push(clickEventHandler);
            }
            else {
                var clickEventHandler = new cc.Component.EventHandler();
                clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
                clickEventHandler.component = "Home";// This is the code file name
                clickEventHandler.handler = "chatFriend";
                clickEventHandler.customEventData = Util.playerData.friends[i].friend_id;

                var button = friend.getChildByName('action').getChildByName('chat').getComponent(cc.Button);
                button.clickEvents = [];
                button.clickEvents.push(clickEventHandler);
            }


            friend.active = true;
        }
    },

    showWorldList(showRequest) {
        // delete existing
        var parent = this.node.getChildByName('friend world').getChildByName('texture').getChildByName('list').getChildByName('view').getChildByName('content');
        var count = parent.children.length;
        for (var j = 1; j < count; j++) {
            parent.children[1].removeFromParent(false);
            // parent.children[1].destroy();
        }


        Util.showLoading().then(data => {
            var self = this;
            var parent = this.node.getChildByName('friend world').getChildByName('texture').getChildByName('list').getChildByName('view').getChildByName('content');

            new Promise(function(resolve, reject) {
                if (self._worldList) resolve();
                else {
                    var params = 'token=' + Util.playerData.token;
                    params += '&game_id=' + Util.playerData.game_id;

                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.open("POST", Util.getConfig().server_url + '/worldList', true);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState === 4) {
                            if (xmlhttp.status === 200) {
                                var response = JSON.parse(xmlhttp.responseText);
                                if (response.status === 'success') {
                                    self._worldList = response.list;
                                    resolve();
                                } else {
                                    Util.createAlert('Failed to find players', function() {
                                        Util.removeLoading();
                                        reject();
                                    });
                                }
                            }
                        }
                    }
                    xmlhttp.onerror = function () {
                        Util.createAlert('Failed to find players', function() {
                            Util.removeLoading();
                            reject();
                        });
                    };
                    xmlhttp.send(params);
                }
            })
            .then(data => {
                var list = {}, photos = {};
                var promises = [];

                new Promise(function(resolve, reject) {
                    for (var i = 0; i < self._worldList.length; i++) {
                        if (list.hasOwnProperty(self._worldList[i].user_id) || self._worldList[i].user_id == Util.playerData.uid) continue;

                        list[self._worldList[i].user_id] = self._worldList[i];

                        if (self._worldList[i].photo) {
                            var p = new Promise(function(resolve, reject) {
                                var id = i;
                                cc.assetManager.loadRemote(self._worldList[id].photo, function (err, texture) {
                                    photos[id] = texture;
                                    resolve();
                                });
                            });

                            promises.push(p);
                        }
                    }

                    resolve();
                })
                .then(data => {
                    Promise.all(promises)
                    .then(data => {
                        list = {};

                        for (var i = 0; i < self._worldList.length; i++) {
                            if (list.hasOwnProperty(self._worldList[i].user_id) || self._worldList[i].user_id == Util.playerData.uid) continue;
                            list[self._worldList[i].user_id] = self._worldList[i];

                            var user = self._worldList[i];

                            var player = cc.instantiate(parent.children[0]);
                            player.parent = parent;

                            player.getChildByName('action').getChildByName('name').getComponent(cc.Label).string = user.name;


                            if (user.photo) {
                                player.getChildByName('photo').getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(photos[i]);

                                // resize
                                var ratio = 100 / player.getChildByName('photo').height;
                                player.getChildByName('photo').width *= ratio;
                                player.getChildByName('photo').height *= ratio;

                                player.getChildByName('photo').active = true;

                                var clickEventHandler = new cc.Component.EventHandler();
                                clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
                                clickEventHandler.component = "Home";// This is the code file name
                                clickEventHandler.handler = "showPhoto";
                                clickEventHandler.customEventData = self._worldList[i].photo;

                                var button = player.getChildByName('photo').getComponent(cc.Button);
                                button.clickEvents = [];
                                button.clickEvents.push(clickEventHandler);
                            }
                            else {
                                player.getChildByName('photo').active = false;
                            }

                            var clickEventHandler = new cc.Component.EventHandler();
                            clickEventHandler.target = this.node; // This node is the node to which your event handler code component belongs
                            clickEventHandler.component = "Home";// This is the code file name
                            clickEventHandler.handler = "addFriend";
                            clickEventHandler.customEventData = self._worldList[i].user_id;

                            var button = player.getChildByName('action').getChildByName('add').getComponent(cc.Button);
                            button.clickEvents = [];
                            button.clickEvents.push(clickEventHandler);


                            player.active = true;
                        }

                        Util.removeLoading();
                    });
                });
            });
        });
    },

    showPhoto(event, customEventData) {
        Util.showImage(customEventData);
    },

    searchFriend(event, customEventData) {
        var id = this.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('input').getChildByName('editbox').getComponent(cc.EditBox).string.trim();
        if (!id) return;

        this.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('result').active = false;

        var params = 'token=' + Util.playerData.token;
        params += '&game_id=' + Util.playerData.game_id;
        params += '&id=' + id;

        Util.showLoading();

        var self = this;

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", Util.getConfig().server_url + '/searchFriend', true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("Cache-Control", "no-cache");
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4) {
                if (xmlhttp.status === 200) {
                    var response = JSON.parse(xmlhttp.responseText);
                    if (response.status === 'success') {
                        // self._searchId = response.friend.user_id;
                        var clickEventHandler = new cc.Component.EventHandler();
                        clickEventHandler.target = self.node; // This node is the node to which your event handler code component belongs
                        clickEventHandler.component = "Home";// This is the code file name
                        clickEventHandler.handler = "addFriend";
                        clickEventHandler.customEventData = response.friend.user_id;

                        var button = self.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('result').getChildByName('button').getComponent(cc.Button);
                        button.clickEvents = [];
                        button.clickEvents.push(clickEventHandler);


                        self.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('result').getChildByName('name').getComponent(cc.Label).string = response.friend.name;

                        
                        if (response.friend.photo) {
                            cc.assetManager.loadRemote(response.friend.photo, function (err, texture) {
                                self.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('result').active = true;

                                self.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('result').getChildByName('photo').getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(texture);

                                // resize
                                var ratio = 100 / self.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('result').getChildByName('photo').height;
                                self.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('result').getChildByName('photo').width *= ratio;
                                self.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('result').getChildByName('photo').height *= ratio;

                                self.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('result').getChildByName('photo').active = true;
                            });
                        }
                        else {
                            self.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('result').getChildByName('photo').active = false;
                            self.node.getChildByName('friend list').getChildByName('texture').getChildByName('search').getChildByName('result').active = true;
                        }

                        Util.removeLoading();
                    } else {
                        Util.createAlert('So such player found', function() {
                            Util.removeLoading();
                        });
                    }
                }
            }
        }
        xmlhttp.onerror = function () {
            Util.createAlert('Failed to process search request', function() {
                Util.removeLoading();
            });
        };
        xmlhttp.send(params);
    },

    addFriend(event, customEventData) {
        // if (!this._searchId) return;
        console.log(customEventData)

        Util.showLoading();
        var self = this;

        var params = 'token=' + Util.playerData.token;
        params += '&game_id=' + Util.playerData.game_id;
        params += '&id=' + customEventData;

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", Util.getConfig().server_url + '/addFriend', true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("Cache-Control", "no-cache");
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4) {
                if (xmlhttp.status === 200) {
                    var response = JSON.parse(xmlhttp.responseText);
                    if (response.status === 'success') {
                        Util.playerData.friends = response.friends;

                        Util.savePlayerData();

                        // self._searchId = null;

                        Util.removeLoading();

                        Util.createAlert('Friend added successfully. However the person will not see your chat messages if your friend request has not been approved.');
                    } else {
                        Util.createAlert('Failed to add friend', function() {
                            Util.removeLoading();
                        });
                    }
                }
            }
        }
        xmlhttp.onerror = function () {
            Util.createAlert('Failed to add friend', function() {
                Util.removeLoading();
            });
        };
        xmlhttp.send(params);
    },

    acceptFriend(event, customEventData) {
        var self = this;

        Util.createChoiceAlert(
            'Accept this friend request?',
            {
                text: 'Yes',
                cb: function() {
                    Util.showLoading();

                    var params = 'token=' + Util.playerData.token;
                    params += '&game_id=' + Util.playerData.game_id;
                    params += '&id=' + customEventData;

                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.open("POST", Util.getConfig().server_url + '/acceptFriend', true);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState === 4) {
                            if (xmlhttp.status === 200) {
                                var response = JSON.parse(xmlhttp.responseText);
                                if (response.status === 'success') {
                                    Util.playerData.friends = response.friends;

                                    Util.savePlayerData();

                                    self.friendAction(null, 'list');

                                    Util.removeLoading();
                                } else {
                                    Util.createAlert('Failed to accept friend', function() {
                                        Util.removeLoading();
                                    });
                                }
                            }
                        }
                    }
                    xmlhttp.onerror = function () {
                        Util.createAlert('Failed to accept friend', function() {
                            Util.removeLoading();
                        });
                    };
                    xmlhttp.send(params);
                }
            },
            {
                text: 'No',
                cb: function() {}
            }
        );
    },

    blockFriend(event, customEventData) {
        var self = this;

        Util.createChoiceAlert(
            'Are you sure you want to block this friend?',
            {
                text: 'Yes',
                cb: function() {
                    Util.showLoading();

                    var params = 'token=' + Util.playerData.token;
                    params += '&game_id=' + Util.playerData.game_id;
                    params += '&id=' + customEventData;

                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.open("POST", Util.getConfig().server_url + '/blockFriend', true);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState === 4) {
                            if (xmlhttp.status === 200) {
                                var response = JSON.parse(xmlhttp.responseText);
                                if (response.status === 'success') {
                                    for (var i = 0; i < Util.playerData.friends.length; i++) {
                                        if (Util.playerData.friends[i].friend_id == customEventData) {
                                            Util.playerData.friends.splice(i, 1);
                                            break;
                                        }
                                    }

                                    Util.savePlayerData();

                                    self.friendAction(null, 'list');

                                    Util.removeLoading();
                                } else {
                                    Util.createAlert('Failed to block friend', function() {
                                        Util.removeLoading();
                                    });
                                }
                            }
                        }
                    }
                    xmlhttp.onerror = function () {
                        Util.createAlert('Failed to block friend', function() {
                            Util.removeLoading();
                        });
                    };
                    xmlhttp.send(params);
                }
            },
            {
                text: 'No',
                cb: function() {}
            }
        );
    },

    deleteFriend(event, customEventData) {
        var self = this;

        Util.createChoiceAlert(
            'Are you sure you want to delete this friend?',
            {
                text: 'Yes',
                cb: function() {
                    Util.showLoading();

                    var params = 'token=' + Util.playerData.token;
                    params += '&game_id=' + Util.playerData.game_id;
                    params += '&id=' + customEventData;

                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.open("POST", Util.getConfig().server_url + '/deleteFriend', true);
                    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState === 4) {
                            if (xmlhttp.status === 200) {
                                var response = JSON.parse(xmlhttp.responseText);
                                if (response.status === 'success') {
                                    for (var i = 0; i < Util.playerData.friends.length; i++) {
                                        if (Util.playerData.friends[i].friend_id == customEventData) {
                                            Util.playerData.friends.splice(i, 1);
                                            break;
                                        }
                                    }

                                    Util.savePlayerData();

                                    self.friendAction(null, 'list');

                                    Util.removeLoading();
                                } else {
                                    Util.createAlert('Failed to delete friend', function() {
                                        Util.removeLoading();
                                    });
                                }
                            }
                        }
                    }
                    xmlhttp.onerror = function () {
                        Util.createAlert('Failed to delete friend', function() {
                            Util.removeLoading();
                        });
                    };
                    xmlhttp.send(params);
                }
            },
            {
                text: 'No',
                cb: function() {}
            }
        );
    },

    showFriendRanking() {
        // delete existing
        var parent = this.node.getChildByName('friend ranking').getChildByName('texture').getChildByName('list').getChildByName('view').getChildByName('content');
        var count = parent.children.length;
        for (var j = 1; j < count; j++) {
            parent.children[1].removeFromParent(false);
            // parent.children[1].destroy();
        }

        for (var i = 0; i < Util.playerData.scores.length; i++) {
            var friend = cc.instantiate(parent.children[0]);
            friend.parent = parent;

            friend.getChildByName('info').getChildByName('eagle').getChildByName('value').getComponent(cc.Label).string = Util.playerData.scores[i].eagle
            friend.getChildByName('info').getChildByName('tank').getChildByName('value').getComponent(cc.Label).string = Util.playerData.scores[i].tank;

            if (Util.playerData.scores[i].user_id == Util.playerData.uid) {
                friend.getChildByName('name').getComponent(cc.Label).string = Util.playerData.name;
            }
            else {
                for (var j = 0; j < Util.playerData.friends.length; j++) {
                    if (Util.playerData.friends[j].friend_id == Util.playerData.scores[i].user_id) {
                        friend.getChildByName('name').getComponent(cc.Label).string = Util.playerData.friends[j].friend_name;
                        break;
                    }
                }
            }

            friend.active = true;
        }
    },

    showFriendRooms() {
        // delete existing
        var parent = this.node.getChildByName('friend play').getChildByName('texture').getChildByName('list').getChildByName('view').getChildByName('content');
        var count = parent.children.length;
        for (var j = 2; j < count; j++) {
            parent.children[2].removeFromParent(false);
            // parent.children[1].destroy();
        }


        if (Util.playerData.friends.length <= 0) {
            parent.getChildByName('info').active = true;
        }
        else {
            Util.showLoading();

            var self = this;

            var params = 'token=' + Util.playerData.token;
            params += '&game_id=' + Util.playerData.game_id;
            params += '&friends=' + encodeURIComponent(JSON.stringify(Util.playerData.friends));

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", Util.getConfig().server_url + '/friendRooms', true);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp.setRequestHeader("Cache-Control", "no-cache");
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState === 4) {
                    if (xmlhttp.status === 200) {
                        var response = JSON.parse(xmlhttp.responseText);
                        if (response.status === 'success') {
                            var rooms = {};

                            for (var i = 0; i < response.data.length; i++) {
                                if (response.data[i][1] == null) continue;

                                if (!rooms.hasOwnProperty(response.data[i][1])) rooms[response.data[i][1]] = [];

                                rooms[response.data[i][1]].push(response.data[i][0]);
                            }

                            Util.removeLoading();
                            
                            // console.log(rooms);
                            var keys = Object.keys(rooms);
                            if (keys.length == 0) {
                                parent.getChildByName('info').active = true;
                            }
                            else {
                                for (var i = 0; i < keys.length; i++) {
                                    var room = cc.instantiate(parent.children[1]);
                                    room.parent = parent;

                                    var label = '';

                                    for (var k = 0; k < rooms[keys[i]].length; k++) {
                                        // find friend name
                                        var name;
                                        for (var j = 0; j < Util.playerData.friends.length; j++) {
                                            if (Util.playerData.friends[j].friend_id == rooms[keys[i]][k]) {
                                                name = Util.playerData.friends[j].friend_name;
                                                break;
                                            }
                                        }

                                        if (k == 0) label = name;
                                        else if (k < 3) label += ', ' + name;
                                        else break;
                                    }

                                    if (rooms[keys[i]].length >= 3) {
                                        label += ' and ' + (rooms[keys[i]].length - 3) + ' more';
                                    }

                                    room.getChildByName('name').getComponent(cc.Label).string = label;

                                    var clickEventHandler = new cc.Component.EventHandler();
                                    clickEventHandler.target = self.node; // This node is the node to which your event handler code component belongs
                                    clickEventHandler.component = "Home";// This is the code file name
                                    clickEventHandler.handler = "join";
                                    clickEventHandler.customEventData = keys[i];

                                    var button = room.getChildByName('join').getComponent(cc.Button);
                                    button.clickEvents = [];
                                    button.clickEvents.push(clickEventHandler);

                                    room.active = true;
                                }
                            }
                        } else {
                            Util.createAlert('Sorry, an unexpected error has occurred.', function() {
                                Util.removeLoading();
                            });
                        }
                    }
                }
            }
            xmlhttp.onerror = function () {
                Util.createAlert('Sorry, an unexpected error has occurred.', function() {
                    Util.removeLoading();
                });
            };
            xmlhttp.send(params);
        }
    },

    chatFriend(event, customEventData) {
        cc.sys.localStorage.setItem('chat_friend', customEventData);
        // Util.showLoading();
        Util.loadScene('chat');
    },

    showIap() {
        // Util.showLoading();
        Util.loadScene('iap');
    },

    rewarded() {
        this.node.getChildByName('info').getChildByName('inventory').getChildByName('rewarded').getComponent(cc.Button).interactable = false;

        var self = this;

        var updateServer = function(speed, laser) {
            self.node.getChildByName('info').getChildByName('inventory').getChildByName('rewarded').getComponent(cc.Button).interactable = true;

            var params = 'token=' + Util.playerData.token;
            params += '&game_id=' + Util.playerData.game_id;
            params += '&speed=' + speed;
            params += '&laser=' + laser;

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", Util.getConfig().server_url + '/claimRewarded', true);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp.setRequestHeader("Cache-Control", "no-cache");
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState === 4) {
                    if (xmlhttp.status === 200) {
                        var response = JSON.parse(xmlhttp.responseText);
                        if (response.status === 'success') {
                        } else {
                        }

                        self.fbinstantCreateShortcut();
                    }
                }
            }
            xmlhttp.onerror = function () {
                self.fbinstantCreateShortcut();
            };
            xmlhttp.send(params);
        };

        Util.createChoiceAlert(
            'Watch a video ads to receive 20x Speed and 10x Laser?',
            {
                text: 'Yes',
                cb: function() {
                    self.node.getComponent("SoundManager").stopAll();

                    Util.showRewarded().then(data => {
                        if (data === 'wait') {
                            Util.createAlert('Loading ads. Please try again in a while.', function() {
                                self.node.getChildByName('info').getChildByName('inventory').getChildByName('rewarded').getComponent(cc.Button).interactable = true;
                            });
                        }
                        else if (data === 'success' && Util.rewardedSuccess) {
                            Util.playerData.inventory.laser += 10;
                            Util.playerData.inventory.speed += 20;
                            Util.savePlayerData();

                            self.node.getChildByName('info').getChildByName('inventory').getChildByName('speed').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.speed + 'x Speed';
                            self.node.getChildByName('info').getChildByName('inventory').getChildByName('laser').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.laser + 'x Laser';

                            self.node.getComponent("SoundManager").playEffectSound("bonus", false);

                            updateServer(20, 10);

                            Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "speed", 20, "rewarded_video", "");
                            Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "laser", 10, "rewarded_video", "");
                        }
                        else {
                            Util.showInterstitial().then(data => {
                                if (data === 'wait') {
                                    Util.createAlert('Loading ads. Please try again in a while.', function() {
                                        self.node.getChildByName('info').getChildByName('inventory').getChildByName('rewarded').getComponent(cc.Button).interactable = true;
                                    });
                                }
                                else if (data === 'success') {
                                    Util.createAlert('A non-video ads was shown instead. You have received 4x Speed and 4x Laser.', function() {
                                        Util.playerData.inventory.laser += 4;
                                        Util.playerData.inventory.speed += 4;
                                        Util.savePlayerData();

                                        self.node.getChildByName('info').getChildByName('inventory').getChildByName('speed').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.speed + 'x Speed';
                                        self.node.getChildByName('info').getChildByName('inventory').getChildByName('laser').getChildByName('label').getComponent(cc.Label).string = Util.playerData.inventory.laser + 'x Laser';

                                        self.node.getComponent("SoundManager").playEffectSound("bonus", false);

                                        updateServer(4, 4);

                                        Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "speed", 4, "rewarded_interstitial", "");
                                        Util.ga.GameAnalytics.addResourceEvent(Util.ga.EGAResourceFlowType.Source, "laser", 4, "rewarded_interstitial", "");
                                    });
                                }
                                else if (data === 'fail') {
                                    Util.createAlert('Sorry, we are unable to find any ads to show you.', function() {
                                        self.node.getChildByName('info').getChildByName('inventory').getChildByName('rewarded').getComponent(cc.Button).interactable = true;
                                    });
                                }
                            });
                        }
                    });
                }
            },
            {
                text: 'No',
                cb: function() {
                    self.node.getChildByName('info').getChildByName('inventory').getChildByName('rewarded').getComponent(cc.Button).interactable = true;
                }
            }
        );
    },

    fbinstantCreateShortcut() {
        if (Util.getConfig().fbinstant) {
            FBInstant.canCreateShortcutAsync()
            .then(function(canCreateShortcut) {
                if (canCreateShortcut) {
                    FBInstant.createShortcutAsync()
                    .then(function() {
                        // Shortcut created
                    })
                    .catch(function() {
                        // Shortcut not created
                    });
                }
            });
        }
    },

    play() {
        // Util.showLoading();

        if (Util.getConfig().fbinstant) {
            var apis = FBInstant.getSupportedAPIs();
            if (apis.includes('player.canSubscribeBotAsync')) {
                FBInstant.player.canSubscribeBotAsync().then(function(can_subscribe) {
                    if (can_subscribe && apis.includes('player.subscribeBotAsync')) {
                        Util.createChoiceAlert(
                            'Shall we notify you to claim your free gifts?',
                            {
                                text: 'Yes',
                                cb: function() {
                                    FBInstant.player.subscribeBotAsync().then(data => {
                                        var params = 'token=' + Util.playerData.token;
                                        params += '&game_id=' + Util.playerData.game_id;

                                        var self = this;

                                        var xmlhttp = new XMLHttpRequest();
                                        xmlhttp.open("POST", Util.getConfig().server_url + '/fbSubscribeBot', true);
                                        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                                        xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                                        xmlhttp.onreadystatechange = function() {
                                            if (xmlhttp.readyState === 4) {
                                                if (xmlhttp.status === 200) {
                                                    Util.loadScene('game');
                                                } else {
                                                    Util.loadScene('game');
                                                }
                                            }
                                        }
                                        xmlhttp.onerror = function () {
                                            Util.loadScene('game');
                                        };
                                        xmlhttp.send(params);
                                    }).catch(function (e) {
                                        Util.error(e);
                                        Util.loadScene('game');
                                    });
                                }
                            },
                            {
                                text: 'No',
                                cb: function() {
                                    Util.loadScene('game');
                                }
                            }
                        );
                    }
                }).catch(function (e) {
                    Util.error(e);
                    Util.loadScene('game');
                });
            }
        }
        else {
            Util.loadScene('game');
        }
    },

    join(event, customEventData) {
        // console.log(customEventData);
        cc.sys.localStorage.setItem('join_room', customEventData);

        // Util.showLoading();
        Util.loadScene('game');
    },

    update(dt) {
        if (this.scrollView) {
            this.viewRect = cc.rect(-this.scrollView.width / 2, - this.scrollContent.y - this.scrollView.height / 2, this.scrollView.width, this.scrollView.height);

            for (let i = 0; i < this.scrollContent.children.length; i++) {
                if (this.viewRect.intersects(this.scrollContent.children[i].getBoundingBox())) {
                    this.scrollContent.children[i].opacity = 255;
                } else {
                    this.scrollContent.children[i].opacity = 0;
                }
            }
        }
    },
});