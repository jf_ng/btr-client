"use strict";

var Util = require("./Util");

cc.Class({
    extends: cc.Component,

    editor: {
        // executeInEditMode: true
    },

    properties: {
        _onLoadPromise: null,
        _config: null
    },

    onLoad() {
        cc.sys.localStorage.removeItem('btr_config');
        this._config = Util.getConfig();
        // Util.firebase.auth().signOut();
    },

    start() {
        var maintenance = false, update = false;
        var self = this, gamemeta;

        this.setProgress(0.1);


        // **************************************************
        // app version and server maintenance
        // **************************************************
        this.setStatus('Checking game meta');

        var p1 = new Promise(function(resolve, reject) {
            Util.log('>>> btr version : ' + self._config.version);

            if (self._config.fbinstant && typeof FBInstant !== "undefined") {
                self._config.fbinstant = true;
                Util.ga.GameAnalytics.configureBuild("fbinstant-" + self._config.version);
            }
            else {
                self._config.fbinstant = false;
            }

            self.incrProgress(0.1);
            resolve();
        });

        var p2 = new Promise(function(resolve, reject) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", 'https://s3-us-west-2.amazonaws.com/verygames.lol/games/btr/gamemeta.json', true);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp.setRequestHeader("Cache-Control", "no-cache");
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState === 4) {
                    if (xmlhttp.status === 200) {
                        gamemeta = JSON.parse(xmlhttp.responseText);

                        self._config.server_url = gamemeta.server_url;
                        self._config.game_http_url = gamemeta.game_http_url;
                        self._config.game_https_url = gamemeta.game_https_url;
                        
                        self.incrProgress(0.1);
                        resolve();
                    }
                }
            }
            xmlhttp.onerror = function () {
                reject('Failed to load remote game meta');
            };
            xmlhttp.send();
        });

        // init sdkhub
        var p3 = new Promise(function(resolve, reject) {
            if (self._config.fbinstant && typeof FBInstant !== "undefined") {
                self._config.sdkhub = false;
                
                resolve();
            }
            else {
                self.scheduleOnce(function() {
                    if ('undefined' === typeof(sdkhub)) {
                        self._config.sdkhub = false;

                        Util.ga.GameAnalytics.configureBuild(cc.sys.os + "-" + self._config.version);

                        resolve();
                    }
                    else {
                        self._config.sdkhub = true;
                        Util.initSdkhub();

                        Util.ga.GameAnalytics.configureBuild("hms-" + self._config.version);

                        resolve();
                    }
                }, 2);
            }
        });

        new Promise(function(main_resolve, main_reject) {
            Promise.all([p1, p2, p3])
            .then(data => {
                Util.ga.GameAnalytics.initialize("69a21f665c108fed56fb2b812b7982c9", "7cb1ed1992e259c2e36d7390f1c4354ff3a5f647");

                self.setStatus('Checking game version');

                var p5 = new Promise(function(resolve, reject) {
                    if (self._config.version >= gamemeta.version) {
                        self.incrProgress(0.1);
                        resolve();
                    } else {
                        update = true;
                        reject('Please download and install a newer version of the game from the app store. Thank you.');
                    }
                });

                self.setStatus('Checking server maintenance');

                var p6 = new Promise(function(resolve, reject) {
                    if (!gamemeta.maintenance) {
                        self.incrProgress(0.1);
                        resolve();
                    } else {
                        maintenance = true;
                        reject(gamemeta.maintenance_message);
                    }
                });

                var p7 = new Promise(function(resolve, reject) {
                    Util.getPlayerData().then(playerData => {
                        Util.playerData = playerData;

                        self.incrProgress(0.1);
                        resolve();
                    });
                });

                Promise.all([p5, p6, p7])
                .then(data => {
                    // **************************************************
                    // login anonymous if no account
                    // **************************************************
                    self.setStatus('Checking account');

                    // register(email_verified, phone_number, fbinstant_id, hms_id)
                    new Promise(function(resolve, reject) {
                        function registerCallback(success) {
                            if (success) resolve();
                            else reject('Registration failed');
                        };

                        if (self._config.fbinstant) {
                            if (Util.playerData.account_new) {
                                Util.playerData.uid = FBInstant.player.getID();
                                Util.playerData.is_anonymous = false;
                                Util.playerData.language = FBInstant.getLocale();
                                Util.playerData.fbinstant_id = FBInstant.player.getID();
                                Util.playerData.name = FBInstant.player.getName();

                                var apis = FBInstant.getSupportedAPIs();
                                if (apis.includes('player.getPhoto')) Util.playerData.photo = encodeURIComponent(FBInstant.player.getPhoto());

                                self.register(false, null, FBInstant.player.getID(), null, registerCallback);
                            }
                            else {
                                resolve();
                            }
                        }
                        else {
                            Util.firebase.auth().onAuthStateChanged(function(user) {
                                if (user) { // uid displayName photoURL email emailVerified phoneNumber isAnonymous
                                    Util.log('User is signed in');

                                    // **************************************************
                                    // register with server
                                    // **************************************************
                                    if (Util.playerData.account_new) {
                                        Util.playerData.uid = user.uid;
                                        Util.playerData.is_anonymous = user.isAnonymous;
                                        Util.playerData.email = user.email;
                                        Util.playerData.language = cc.sys.language;


                                        if (self._config.sdkhub) {
                                            // {"displayName":"jf","playerSign":"CErR1Ix8rInM+\/jEzHAnxKSj1132aJBCkXYTu7FCsKkwSZibmmtZLjuoKuTEAd59KhiBUiGtB7lvWB65EsVvLBtl3z\/2yjQCWHRg+2GcEG\/ZORN0yqTF3MB4Th7kV3m68EhUMC4tV38OeT7mqOuKktvot4AxRzshQxuoJ+9yOzUgDEWBH3RGffGFq522IhwHL7VZFC1gwRzs9cABtwGO4te92aaM1lgx9AKckcROoYw8K49zrLVFx6o4zQVaskjHWzutd8QdHk55Ha3podTRWwC5Ef\/P9bg4Bps9RIDQNJy2A9a3RqewLPRwNX9cOtGxLphH\/r2majJRjEwRQP+UKg==","userID":"5185082309200144","playerId":"5185082309200144","iconImageUri":"","signTS":"1599118645073","hiResImageUri":"","msg":"login success","level":"1"}
                                            Util.hms.callback = function(code, msg) {
                                                var userInfo = JSON.parse(msg);

                                                Util.playerData.hms_id = userInfo.userID;
                                                Util.playerData.name = userInfo.displayName;
                                                Util.playerData.photo = userInfo.hiResImageUri;

                                                Util.hms.callback = null;

                                                self.register(false, null, null, userInfo.userID, registerCallback);
                                            };

                                            sdkhub.getUserPlugin().login();
                                        }
                                        else {
                                            self.register(user.emailVerified, user.phoneNumber, null, null, registerCallback);
                                        }
                                    }
                                    else {
                                        resolve();
                                    }
                                } else {
                                    Util.log('User is signed out');

                                    Util.flush().then(data => {
                                        Util.playerData.account_new = true;

                                        Util.firebase.auth().signInAnonymously().catch(function(error) {
                                            reject('Sign in failed');
                                        });
                                    });
                                }
                            });
                        } // end if (self._config.fbinstant && Util.playerData.account_new)
                    })
                    .then(function(value) {
                        self.incrProgress(0.2);


                        // **************************************************
                        // fbinstant getConnectedPlayersAsync
                        // **************************************************
                        new Promise(function(resolve, reject) {
                            var players = [];

                            if (self._config.fbinstant) {
                                if (FBInstant.getSupportedAPIs().includes('player.getConnectedPlayersAsync')) {
                                    FBInstant.player.getConnectedPlayersAsync()
                                    .then(function(arr) {
                                        players = arr.map(function(player) {
                                            return {
                                                id: player.getID(),
                                                name: player.getName(),
                                            }
                                        });

                                        resolve(players);
                                    });
                                }
                                else resolve(players);
                            }
                            else resolve(players);
                        })
                        .then(function(friends) {
                            // **************************************************
                            // get game data
                            // **************************************************
                            self.setStatus('Retrieving game data');

                            new Promise(function(resolve, reject) {
                                var params = 'token=' + Util.playerData.token;
                                params += '&game_id=' + Util.playerData.game_id;
                                params += '&speed=' + Util.playerData.inventory.speed;
                                params += '&laser=' + Util.playerData.inventory.laser;
                                params += '&friends=' + JSON.stringify(friends);

                                var update_inventory = false;
                                if (cc.sys.localStorage.getItem('start_game') && !cc.sys.localStorage.getItem('end_game')) update_inventory = true;
                                params += '&update_inventory=' + update_inventory;

                                cc.sys.localStorage.removeItem('start_game');
                                cc.sys.localStorage.removeItem('end_game');

                                var xmlhttp = new XMLHttpRequest();
                                xmlhttp.open("POST", self._config.server_url + '/initialize', true);
                                xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                                xmlhttp.setRequestHeader("Cache-Control", "no-cache");
                                xmlhttp.onreadystatechange = function() {
                                    if (xmlhttp.readyState === 4) {
                                        if (xmlhttp.status === 200) {
                                            var response = JSON.parse(xmlhttp.responseText);
                                            if (response.status === 'success') {
                                                Util.playerData.name = response.user.name;
                                                Util.playerData.photo = response.user.photo;
                                                Util.playerData.email = response.user.email;
                                                Util.playerData.language = response.user.language;


                                                Util.playerData.friends = response.friends;

                                                Util.playerData.scores = response.scores;

                                                if (!self._config.fbinstant) {
                                                    Util.playerData.inventory = response.inventory;
                                                    Util.playerData.inventory.speed = parseInt(Util.playerData.inventory.speed);
                                                    Util.playerData.inventory.laser = parseInt(Util.playerData.inventory.laser);
                                                }

                                                if (response.hasOwnProperty('reward')) {
                                                    Util.playerData.inventory.speed += parseInt(response.reward.speed);
                                                    Util.playerData.inventory.laser += parseInt(response.reward.laser);
                                                }

                                                Util.playerData.user_data = response.user_data;
                                                try {
                                                    Util.playerData.user_data.unlock = JSON.parse(Util.playerData.user_data.unlock);
                                                } catch (e) {

                                                }

                                                Util.savePlayerData();


                                                Util.playerChats = response.chats;
                                                Util.saveChats();

                                                Util.playerChatMessages = response.chat_messages;
                                                Util.saveChatMessages();


                                                Util.iap = response.iap;
                                                Util.saveIap();


                                                resolve();
                                            } else {
                                                reject('Failed to get game data');
                                            }
                                        }
                                    }
                                }
                                xmlhttp.onerror = function () {
                                    reject('Failed to get game data');
                                };
                                xmlhttp.send(params);
                            })
                            .then(function(value) {
                                main_resolve();
                            })
                            .catch((err) => {
                                main_reject(err);
                            });
                        })
                        .catch((err) => {
                            main_reject(err);
                        });
                    })
                    .catch((err) => {
                        main_reject(err);
                    });
                })
                .catch((err) => {
                    main_reject(err);
                });
            })
            .catch((err) => {
                main_reject(err);
            });
        }).then(function(value) {
            self.incrProgress(0.4);

            Util.saveConfig(self._config);

            Util.loadScene('home');
        })
        .catch((err) => {
            Util.error(err);

            if (maintenance || update) {
                Util.createAlert(err, function() {});
            }
            else {
                // **************************************************
                // just proceed
                // **************************************************
                Util.saveConfig(self._config);

                if (Util.playerData.account_new) {
                    if (!Util.playerData.game_id) Util.playerData.game_id = Util.chance.integer({ min: 10000000, max: 99999999 });
                    if (!Util.playerData.name) Util.playerData.name = 'p_' + Util.playerData.game_id;
                    if (!Util.playerData.token) Util.playerData.token = Util.playerData.uid
                }

                Util.savePlayerData();

                // do not allow purchases
                Util.iap = {};
                Util.saveIap();

                Util.loadScene('home');
            }
        });
    },

    setProgress(progress) {
        let progress_bar = this.node.getChildByName('progress').getComponent(cc.ProgressBar);
        progress_bar.progress = progress;
    },

    incrProgress(incr) {
        let progress_bar = this.node.getChildByName('progress').getComponent(cc.ProgressBar);
        progress_bar.progress += incr;
    },

    setStatus(status) {
        let label = this.node.getChildByName('status').getComponent(cc.Label);
        label.string = status;

        var outline = label.getComponent(cc.LabelOutline);
        if (!outline) {
            outline = label.addComponent(cc.LabelOutline);
        }
        outline.color = cc.Color.BLACK;
        outline.width = 1;
    },

    register(email_verified, phone_number, fbinstant_id, hms_id, callback) {
        var params = 'user_id=' + Util.playerData.uid;
        params += '&is_native=' + cc.sys.isNative;
        params += '&is_browser=' + cc.sys.isBrowser;
        params += '&platform=' + cc.sys.platform;
        params += '&language=' + cc.sys.language;
        params += '&os=' + cc.sys.os;
        params += '&os_version=' + cc.sys.osVersion;
        params += '&email=' + Util.playerData.email;
        params += '&email_verified=' + email_verified;
        params += '&phone=' + phone_number;
        params += '&is_anonymous=' + Util.playerData.is_anonymous;

        params += '&fbinstant_id=' + fbinstant_id;
        params += '&hms_id=' + hms_id;
        params += '&name=' + Util.playerData.name
        params += '&photo=' + Util.playerData.photo

        params += '&speed=' + Util.playerData.inventory.speed;
        params += '&laser=' + Util.playerData.inventory.laser;

        for (var i = 0; i < Util.playerData.scores.length; i++) {
            if (Util.playerData.scores[i].user_id == Util.playerData.uid) {
                params += '&tank=' + Util.playerData.scores[i].tank
                params += '&king=' + Util.playerData.scores[i].eagle;
                break;
            }
        }


        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", this._config.server_url + '/register', true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("Cache-Control", "no-cache");
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4) {
                if (xmlhttp.status === 200) {
                    var response = JSON.parse(xmlhttp.responseText);
                    if (response.status === 'success') {
                        Util.playerData.token = response.token;
                        Util.playerData.game_id = response.game_id;
                        Util.playerData.account_new = false;

                        Util.savePlayerData();

                        callback(true);
                    } else {
                        callback(false);
                    }
                }
            }
        };
        xmlhttp.onerror = function () {
            callback(false);
        };
        xmlhttp.send(params);
    }
});